const response = require('../../helpers/response')
const _ = require('lodash')
const auth = require('../../helpers/auth/')
const util = require('util')
const passport = require('../../../packages/passport')
const twilio = require('../../../packages/twilio')

const {User} = require('../../../models')
const uuid = require('uuid/v4')


exports.login = login
async function login(req, res, next) {
  try {
    const {mfaToken, rememberDevice, fingerprint} = req.body
    const user = await passport.authenticatePromise('local')(req, res, next)
    if (!user) {throw new Error('Invalid username or password')}
    console.log(`Login attempt for ${user.username} from device [${fingerprint}].`)
    console.log('rememberDevice', rememberDevice)
    console.log('fingerprint', fingerprint)
    console.log('user.allowedDevices', user.allowedDevices)
    const jwtToken = auth.jwt.createToken(process.env.JWT_SECRET, user._id)
    if (rememberDevice && _.includes(user.allowedDevices, fingerprint)) {
      console.log(`[${fingerprint}] is a recognized device. Bypassing MFA.`)
      return response.successResponse(res, jwtToken)
    }
    
    if (!mfaToken) {
      const generatedMfaToken = auth.mfa.createToken(user.mfaSecret)
      twilio.sendMessage(
        `Attempted login from ${user.username}. Your Zap Surgical login token is: ${generatedMfaToken}`,
        user.number
      )
      console.log(`Not a remembered device. Enabling MFA. Generated MFA token: ${generatedMfaToken}`)
      return response.successResponse(res)
    }
    const mfaVerified = auth.mfa.verifyToken(mfaToken, user.mfaSecret)

    if (mfaVerified && rememberDevice) {
      const currentUser = await User.findById(user._id)
      currentUser.allowedDevices = _.union(currentUser.toObject().allowedDevices, [fingerprint])
      currentUser.save()
    } else if (mfaVerified && !rememberDevice) {
      const currentUser = await User.findById(user._id)
      currentUser.allowedDevices = _.filter(currentUser.toObject().allowedDevices, device => device != fingerprint)
      currentUser.save()
    }

    if (mfaVerified) {
      return response.successResponse(res, jwtToken)
    }

    throw new Error('Invalid MFA token')    
  } catch (err) {
    console.log(err)
    return response.errorResponse(res, err)
  }
}

exports.mfaLogin = mfaLogin
async function mfaLogin(req, res, next) {
  const {jwtToken, mfaToken, rememberDevice} =  req.body
  try {
    const user = req.user

    if (mfaToken) {
      const verified = auth.mfa.verifyToken(mfaToken, secret)
    } else {
      
    }
  } catch (err) {
    return response.errorResponse(res, err)
  }
}


exports.logout = logout
function logout(req, res, next) {
  // This function should be useless at the moment since the idea is that a jwt token is returned and always used
  // It is here for completeness and in case we ever change the dataflow.
  req.logout()
  response.successResponse(res, undefined, 'Successfully logged out.')
}

exports.getUser = getUser
function getUser(req, res, next) {
  return response.successResponse(res, req.user)
}

exports.isMfaAuthenticated = (req, res, next) => {
  const {user: {allowedDevices=[], secret, number, _id}, body: {username, fingerprint, mfaToken, rememberDevice}} = req;

  // Device is a remembered device
  if (allowedDevices.indexOf(fingerprint) > -1 && rememberDevice) {
    console.log(`Device ${fingerprint} is a remembered device. Bypassing MFA.`);
    return next()
  }

  // Device is not a remembered device, but a token was submitted
  if (mfaToken) {
    console.log(`Submitted token: ${mfaToken}`);
    const totpOptions = {
      secret,
      encoding: 'base32',
      token: mfaToken,
      window: 10,
      // step: 60
    }
    const verified = speakeasy.totp.verify(totpOptions);
    console.log(`Verification of token: ${verified}`);
    // Add device to remembered devices if selected, otherwise remove
    if (verified) {modifyDevicesList(_id, fingerprint, !Boolean(rememberDevice))}

    return verified ? next() : res.json({error: "Invalid verification token!"})
  }

  // Device is not remembered and no token was submitted.
  // Prompt for a token

  // console.log(`User secret: ${secret}`);
  // console.log(`${speakeasy.totp({secret: secret, encoding: 'base32'})}`);
  console.log("This is not a known device");
  const token = speakeasy.totp({secret: secret, encoding: 'base32'});
  const message = `Attempted login from ${username}. Your Zap Surgical login token is: ${token}`;
  console.log(`Attempted login from ${username}`)
  console.log(message);
  twilio.sendMessage(message, number);
  res.json({mfaRequired: true})
}

// exports.UserUpdate = UserUpdate
// async function UserUpdate(req, res) {
//   const user = await User.findById(req.params.id)
//   console.log('user', user)
//   // await user.setPassword('benben')
//   // await user.save()
// }

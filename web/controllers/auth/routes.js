const router = require('express').Router()
const auth = require('./')
const middleware = require('../../middleware')

router.route('/login')
.post(auth.login)

router.route('/logout')
.get(auth.logout)
.post(auth.logout)

router.route('/user')
.get(middleware.isAuthenticated, auth.getUser)

module.exports = router
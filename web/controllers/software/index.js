const {Software} = require('../../../models')
const {response, file} = require('../../helpers')
const utils = require('../../utils')
const _ = require('lodash')


exports.SoftwareIndex = SoftwareIndex
async function SoftwareIndex(req, res) {
  let softwares = await Software.find()
  softwares = await file.addUrl(softwares)
  // const keys = {}
  const uniqueSoftware = {}

  for (const software of softwares) {
    const softwareEntry = uniqueSoftware[`${software.name}:${software.version}`]
    
    if (
        !softwareEntry ||
        (softwareEntry && (software.dateUploaded > softwareEntry.dateUploaded))
      ) {
      uniqueSoftware[`${software.name}:${software.version}`] = software
    }
  }

  // console.log(uniqueSoftware)

  return response.successResponse(res, Object.values(uniqueSoftware))
}

// async function run() {
//   const softwares = await Software.find()
//   // let versions = {'tps':[], 'tds':[], 'broker':[]}

//   for(let software of softwares) {
//   //   // software.file['s3ObjectName'] = software.toObject().s3ObjectName
//   //   // software.file['s3BucketName'] = software.toObject().s3BucketName
//   //   // console.log('branchName', software.build.fullBranchName)
//   //   // software.version = software.version || software.file['version'] || utils.parse.version(software.build.fullBranchName)
//   //   // software.save()
//   //   if (software.name.match(/tps/i)) { versions.tps = _.union(versions.tps, [software.version])  }
//   //   if (software.name.match(/treatment/i)) { versions.tds = _.union(versions.tds, [software.version])  }
//   //   if (software.name.match(/broker|sitemgmt/i)) { versions.broker = _.union(versions.broker, [software.version])  }
//   //   if (!software.version || (!software.file || _.isEmpty(software.file))) {
//   //     console.log(software._id)
//   //     await Software.findByIdAndRemove(software._id)
//   //   }
//     software.dateUploaded = new Date(software.build.buildEnd)
//     software.save()
//   }
//   // console.log(versions)
  
// }

// run()
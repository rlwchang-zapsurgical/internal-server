const router = require('express').Router()
const controller = require('./')

router.route('/')
.get(controller.SoftwareCompatabilityIndex)

router.route('/:id')
.get(controller.SoftwareCompatabilityShow)
// .put()
// .delete()

router.defaultIdKey = '_id'

module.exports = router
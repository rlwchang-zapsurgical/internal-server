const {SoftwareCompatability} = require('../../../models')
const {response, file} = require('../../helpers')
const _ = require('lodash')
// exports.SoftwareCompatabilityIndex = SoftwareCompatabilityIndex
// async function SoftwareCompatabilityIndex(req, res) {
//   const compatabilities = await 
// }

// async function run() {
//   const compats = await SoftwareCompatability.find()
//   for (const compat of compats) {
//     if (
//       _.includes([ '1.7.16', '1.7.17', '1.7.18', '1.7.19' ], compat.software.tps) &&
//       _.includes([ '1.7.13' ], compat.software.tds) &&
//       _.includes([ '1.7.4.4', '1.7.4.5', '1.7.4.3', '1.7.5.0' ], compat.software.broker) 
//     ) {
//       // console.log(compat._id)
//       continue
//     } else {
//       await SoftwareCompatability.findByIdAndRemove(compat._id)
//     }
//   }
// }

// run()

exports.SoftwareCompatabilityIndex = SoftwareCompatabilityIndex
async function SoftwareCompatabilityIndex(req, res, next) {
  let results = await SoftwareCompatability.find()
  results = await file.addUrl(results, 'documents')
  response.successResponse(res, results)
}

exports.SoftwareCompatabilityShow = SoftwareCompatabilityShow
async function SoftwareCompatabilityShow(req, res, next) {
  let results = await SoftwareCompatability.findOne({_id: req.params.id})
  results = await file.addUrl(results, 'documents')
  response.successResponse(res, results)
}

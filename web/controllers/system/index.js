const _ = require('lodash')
const {System} = require('../../../models')
const {response} = require('../../helpers')

const omissionList = [
  'index.js',
  'archive',
  'routes.js'
]
let controllers = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (!_.includes(omissionList, file)) {
     controllers = {...controllers, ...require(`./${file.replace('.js', '')}`)}
  }
});

controllers.SystemIndex = async (req, res, next) => {
  const systems = await System.find()
  .populate('masterDiskControl') 
  .populate('masterDiskOperator') 
  .populate('masterDiskDatabase')

  response.successResponse(res, systems)
}


controllers.SystemShow = async (req, res, next) => {
  const system = await System.findOne({systemId: req.params.id})
  .populate('masterDiskControl') 
  .populate('masterDiskOperator') 
  .populate('masterDiskDatabase')

  response.successResponse(res, system)
}



module.exports = controllers

// const {System} = require('../../../models/')

// async function run() {
//   const systems = await System.find()

//   for(let system of systems) {
// //     // system.owner = {
// //     //   name: system.toObject().name,
// //     //   city: system.toObject().city,
// //     //   state: system.toObject().state,
// //     //   country: system.toObject().country,
// //     //   lat: system.toObject().lat,
// //     //   lon: system.toObject().lon
// //     // }
// //     _.forEach(['state', 'city', 'country', 'lat', 'lon'], field => system[field] = undefined)
//     system.masterDiskControl = "5bc6b841fc9107963bd1b7fa"
//     system.masterDiskOperator = "5ba931c0a04e20300836db78"
//     system.masterDiskDatabase = "5ba931c0a04e20300836db76"
//     system.save()
//   }
  

// }

// run()
const salt = require('../../helpers/salt')
const response = require('../../helpers/response')

exports.getServices = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.cmd.run('service.status', tgt, ['*'], {cacheExp: 60})
  request
  .then(data => salt.response.successResponse(res, data))
  .catch(err => response.errorResponse(res, err))
}

exports.modifyService = (req, res) => {
  const {serviceName} = req.body
  //actions include: start, stop, restart
  const {id, pcType, action} = req.params
  let tgt
  if (id && pcType) {
    tgt = `*_${id}_*${pcType}*`
  } else if (id) {
    tgt = `*_${id}_*`
  } else {
    tgt = '*'
  }
  const getServicesKey = salt.cmd.getParams(
    'service.status',
    id ? `*_${id}_*` : '*',
    ['*']
  )
  const keyToClear = JSON.stringify(_.pick(getServicesKey, ['client', 'tgt', 'arg', 'fun']))
  const request = salt.cmd.run(`service.${action}`, tgt, serviceName, {clearOtherKeys: keyToClear})
  console.log(`targetting ${tgt}`)
  console.log(`trying to ${action}`, serviceName)
  request
  .then(data => salt.response.successResponse(res, data))
  .catch(err => response.errorResponse(res, err))
}
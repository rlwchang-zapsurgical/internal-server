const salt = require('../../helpers/salt')
const response = require('../../helpers/response')
const _ = require('lodash')

exports.getSystemInfo = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.cmd.run('system.get_system_info', tgt, {cacheExp: 60 * 60 * 24, failedCacheExp: 20})
  request
  .then(data => {
    return salt.response.successResponse(res, data)
  })
  .catch(err => response.errorResponse(res, err))
}

exports.getVitals = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request =  Promise.all([
    // salt.cmd.run('ps.cpu_percent', tgt),
    salt.cmd.run('status.cpuload', {tgt: `${tgt} and G@kernel:windows`, tgt_type: 'compound'}, {cacheExp: 300, failedCacheExp: 300}),
    salt.cmd.run('ps.virtual_memory', tgt, {cacheExp: 300, failedCacheExp: 300}),
    salt.cmd.run('ps.disk_usage', tgt, ['/'], {cacheExp: 300, failedCacheExp: 300}),
  ])

  request
  .then(data => {
    return salt.response.successResponse(res, data, ['cpu_load', 'memory', 'disk'])
  })
  .catch(err => response.errorResponse(res, err))
}

const salt = require('../../helpers/salt')
const response = require('../../helpers/response')

exports.pingSystems = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.cmd.run('test.ping', tgt, {cacheExp: 300, failedCacheExp: 300})
  request
  .then(data => {
    salt.response.successResponse(res, data)
  })
  .catch((err) => {
    console.log('An error occurred while pinging the systems')
    console.log(err)
    response.errorResponse(res, err)})
}
const router = require('express').Router()

const controller = require('./');



router.route('/ping')
  .get(controller.pingSystems)
router.route('/:id/ping')
  .get(controller.pingSystems)

router.route('/sysinfo')
  .get(controller.getSystemInfo)
router.route('/:id/sysinfo')
  .get(controller.getSystemInfo)

router.route('/vitals')
  .get(controller.getVitals)
router.route('/:id/vitals')
  .get(controller.getVitals)

router.route('/packages')
  .get(controller.getPackages)
  .post(controller.modifyPackage)
router.route('/:id/packages')
  .get(controller.getPackages)
  .post(controller.modifyPackage)

router.route('/services')
  .get(controller.getServices)
router.route('/:id/services')
  .get(controller.getServices)
router.route('/:id/:pcType/services/:action')
  .post(controller.modifyService)

router.route('/logs')
  .get(controller.getLogs)
router.route('/:id/logs')
  .get(controller.getLogs)

router.route('/logs/:filename')
.get(controller.showLogs)
router.route('/:id/logs/:filename')
  .get(controller.showLogs)
  
router.route('/logs/refresh')
  .post(controller.pushLogs)
router.route('/:id/logs/refresh')
  .post(controller.pushLogs)


router.route('/')
  .get(controller.SystemIndex)

router.route('/:id')
  .get(controller.SystemShow)

router.defaultIdKey = 'systemId'
module.exports = router

const router = require('express').Router()
const controller = require('./')

router.route('/')
.get(controller.DocumentIndex)

module.exports = router
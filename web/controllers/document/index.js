const {Document} = require('../../../models')
const {response, file} = require('../../helpers')

exports.DocumentIndex = DocumentIndex
async function DocumentIndex(req, res, next) {
  let documents = await Document.find().populate('template')

  documents = await file.addUrl(documents)
  documents = await file.addUrl(documents, 'template.file')
  
  response.successResponse(res, documents)
}
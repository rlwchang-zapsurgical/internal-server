const {Release} = require('../../../models')
const _ = require('lodash')
const {response, file} = require('../../helpers')

exports.ReleaseIndex = ReleaseIndex
async function ReleaseIndex(req, res, next) {
  let results = await Release.find()
  .populate('software.tps')
  .populate('software.tds')
  .populate('software.broker')
  results = await file.addUrl(results, 'documents')
  results = await file.addUrl(results, 'software.tps.file', formatBuildFile)
  results = await file.addUrl(results, 'software.tds.file', formatBuildFile)
  results = await file.addUrl(results, 'software.broker.file', formatBuildFile)
  results = results.sort((prev, curr) => prev.eco > curr.eco ? -1 : 1)
  response.successResponse(res, results)
}

exports.ReleaseShow = ReleaseShow
async function ReleaseShow(req, res, next) {
  let results = await Release.findOne({_id: req.params.id})
  .populate('software.tps')
  .populate('software.tds')
  .populate('software.broker')
  results = await file.addUrl(results, 'documents')
  results = await file.addUrl(results, 'software.tps.file', formatBuildFile)
  results = await file.addUrl(results, 'software.tds.file', formatBuildFile)
  results = await file.addUrl(results, 'software.broker.file', formatBuildFile)
  response.successResponse(res, results)
}

exports.ReleaseUpdate = ReleaseUpdate
async function ReleaseUpdate(req, res, next) {
  let release = await Release.findOne({_id: req.params.id})

  for (const field in req.body) {
    const value = req.body[field]
    // console.log(field, value)
    if (typeof value == 'object' && !Array.isArray(value)) {
      release[field] =  _.merge(req.body[field]) 
    } else {
      release[field] =  req.body[field] || undefined
    }
  }
  await release.save()
  // console.log('body', req.body)
  let results = await Release.populate(
    release,
    [
      {path: 'software.tps'},
      {path: 'software.tds'},
      {path: 'software.broker'},
    ]
  )
  response.successResponse(res, results)
}

module.exports = {...require('./sprint'), ...exports}

function formatBuildFile(build) {
  // console.log('build', build)
  // const fileName = 'test'
  try {
    let fileName = `[${build.build.buildId}]`
    fileName += ` ${build.name}`
    if (build.version) {fileName += ` (v${build.version})`}
    return {
      additionalParams: {
        ResponseContentDisposition: `attachment; filename="${fileName}.zip"`,
      }
    }
  } catch (e) {
    return
  }
}
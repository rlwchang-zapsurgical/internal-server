const router = require('express').Router()
const controller = require('./')

router.route('/sprint/summary')
.get(controller.sprintSummary)

router.route('/')
.get(controller.ReleaseIndex)

router.route('/:id')
.get(controller.ReleaseShow)
.put(controller.ReleaseUpdate)

module.exports = router
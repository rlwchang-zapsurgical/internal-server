const vsts = require('../../helpers/vsts')
const response = require('../../helpers/response')
const _ = require('lodash')
const {sendMail} = require('../../../packages/nodemailer')
const {UserGroup} = require('../../../models')
const parse = require('../../utils/parse')

// Note that if people on VSTS/Azure DevOps delete a team, this information will need to be updated
const projectReference = {
  'tps': {
    project: 'tps',
    team: '6fb57ee6-3f9c-4709-a6e3-ebeec834e39a'
  },
  'treatment': {
    project: 'treatment',
    team: '647c3274-070e-47b1-9061-400328e94c34'
  },
  'entMgmt': {
    project: 'entMgmt',
    team: 'e954713f-9137-4c85-9dac-cef5a7a270a3'
  },
  'siteMgmt': {
    project: 'siteMgmt',
    team: '7801f4cb-5dfb-412e-bb8f-1ab1c0856f81'
  }
}

const messageTemplates = {
  'bug': {
    subject: "🐛 It's a bug invasi🐞n! 🐜",
    message: "My 🕷 SPIDER senses are tingling. I think you got some bugs running loose here.",
    defaultIssue: "There is an open bug of severity 2 or greater.",
    signature: "From your lovely bug mashes,"  
  },
  'test': {
    subject: "👨‍🔬 Uhhhh Boss, you sure about these tests?",
    message: "So I was testing your life savings 💸 against fire 🔥. Your savings kind of lost... What was that? That's not what I was supposed to be testing? Hmm, you might want to specify some test steps in that case.",
    defaultIssue: "There are no test steps defined.",
    signature: "From your non-existent tester,"  
  },
  'srs': {
    subject: "🕵️ So I was investigating your SRS...",
    message: "Was I supposed to see a blank page. I'm looking at SRS Details...but there's not a whole lot of details there.",
    defaultIssue: "There are no SRS details defined.",
    signature: "From your not-so-SeRiouS lover"  
  },
  'version': {
    subject: "❓❓ This is Zap Bot, version #⁉️⁉️⁉️⁉️",
    message: "Wait what!? What version am I? That's what happens when you don't include a version number. Software like me get an identity crisis. 😕❓",
    defaultIssue: "The version number was not specified.",
    signature: " From your version-deprived buddy,"  
  },
  'acceptance': {
    subject: "😎 Your mission, should you choose to accept it...",
    message: "is to find ways to spend all of Zap's discretionary funds 💰💰💰. Oh wait, we can't accept you for this mission. We haven't come up with any acceptance criterias. You might want to work on those. Better luck next time.",
    defaultIssue: "No acceptance criteria specified.",
    signature: "From Mission Impossibot,"
  },
  'default': {
    subject: "🤖 Bzzzt Zap Bot Here",
    message: "Zap Bot here with another possibly important, arguable spammy message."
  },
  'goodJob': {
    subject: "👍 Good work!",
    message: "For this sprint I couldn't find any issues...and I'm a bot. Nice work! Give yourself a high-five ✋. I'd give you one, but that would mean you slapping your screen.",
    signature: "From your non-exclusive, non-personal cheerleader,"
  },
}

exports.sprintSummary = sprintSummary
async function sprintSummary(req, res, next) {
  let {version, project, complete} = req.query
  if (!version && !project) {
    version = req.body.version
    project = req.body.project
  }

  const workItems = await vsts.workItem.workItemsQuery({
    project,
    version
  })

  const result = {
    bugs: _.filter(filterWorkItems(workItems, complete), (workItem) => _.get(workItem, ['fields', 'System.WorkItemType']) == 'Bug' ),
    features: _.filter(filterWorkItems(workItems, complete), (workItem) => _.get(workItem, ['fields', 'System.WorkItemType']) == 'Product Backlog Item'),
    trace: _.filter(filterWorkItems(workItems, complete), (workItem) => _.get(workItem, ['fields', 'Custom.TestSteps']) && _.get(workItem, ['fields', 'Custom.SRSDetails'])),
    issues: findIssues(filterWorkItems(workItems, complete)),
    project,
    version
  }

  response.successResponse(res, result)
}

function filterWorkItems(workItems=[], complete) {
  return _.filter(workItems, (workItem) => {
    const type = _.get(workItem, ['fields', 'System.WorkItemType']) // This is actually unnecessary for now since it gets filtered out above.
    const state = _.get(workItem, ['fields', 'System.State'])
    if (type != 'Bug' && type != 'Product Backlog Item') { return false } // This is actually unnecessary for now since it gets filtered out above.
    if (complete && (state != 'Done' && state != 'Committed')) { return false }

    return true
  })
}

function findIssues(data=[]) {
  const results = []

  const allowedBugStates = ['Done', 'Approved', 'Committed', 'Removed']
  const allowedBugs = allowedBugStates.join(' ')

  
  for(let feature of data) {
      if (!feature.fields['Custom.TestSteps']) {
        results.push({
        id: feature.id,
        type: 'test',
        description: 'No test steps found.'
        })
      }
      
      if (
        feature.fields["System.WorkItemType"] == "Bug" &&
        _.includes(allowedBugStates, feature.fields['System.State']) &&
        (
          parse.severity(feature.fields["Custom.RMSeverity"]) > 2 ||
          parse.severity(feature.fields["Microsoft.VSTS.Common.Severity"] > 2)
        )
        )
      {
        results.push({
          id: feature.id,
          type: 'bug',
          description: `There is a bug of with a severity greater 2 that is not in one of the allowed states: ${allowedBugs}`
        })    
      }
  }
  
  return results  
}

exports.checkSprintItems = checkSprintItems
async function checkSprintItems(projectName, options={}) {
  const project = projectReference[projectName]
  const missingFields = {
    'bug': [],
    'srs': [],
    'test': [],
    'version': [],
    'acceptance': []
  }
  try {
    let items = await vsts.sprint.getLatestSprintItems(project.project, project.team, options)
    // Occasionally this fails, so this acts as a second chance.
    items = (!items || _.isEmpty(items)) ? await vsts.sprint.getLatestSprintItems(project.project, project.team, options) : items
    
    if (!items) {throw new Error('Failed to connect to Azure DevOps and retrieve work items.')}
    
    for(let item of items) {
      const {id, url, fields} = item
      if (fields['System.WorkItemType'] == 'Product Backlog Item' || fields['System.WorkItemType'] == 'Bug') {
        if (hasOpenBugIssue(item)) {missingFields.bug = _.unionBy(missingFields.bug, [item], 'id')}
        if (!fields['Custom.SRSDetails']) {missingFields.srs = _.unionBy(missingFields.srs, [item], 'id')}
        if (!fields['Custom.TestSteps']) {missingFields.test = _.unionBy(missingFields.test, [item], 'id')}
        if (fields['System.State'] == 'Committed' && !fields['zapsurgicalScrum.FixedoraddedinRelease']) {missingFields.version = _.unionBy(missingFields.version, [item], 'id')}
        if (!fields['Microsoft.VSTS.Common.AcceptanceCriteria']) {missingFields.acceptance = _.unionBy(missingFields.acceptance, [item], 'id')}
      }
    }
    // console.log(missingFields)
    
    if (options.sendNotification) {
      const issues = [] // e.g. ['bugs', 'srs']s
      let projectGroup = await UserGroup.findOne({name: `${projectName}`})
      let testingGroup = await UserGroup.findOne({name: `${projectName}-test`})
      let srsGroup = await UserGroup.findOne({name: 'srs'}) //As of 10-29-18, it's basically just Larry.
      let projectMembers = _.map(projectGroup.members, member => member.email)
      let projectManagers = _.map(projectGroup.managers, manager => manager.email)
      let testingMembers = _.map(testingGroup.members, member => member.email)
      let testingManagers = _.map(testingGroup.managers, manager => manager.email)
      let srsMembers = _.map(srsGroup.members, member => member.email)
      let acceptanceGroup = {} //notice how this is an object? We will push the entire list of items to send, keyed by email

      let initialContacts = [
        ...projectMembers,
        ...projectManagers,
        ...testingMembers,
        ...testingManagers,
        ...srsMembers
      ]
      
      let finalContacts = []
      
      // for (let i = 0; i < 10; i++) {
      //   sendMail(['RichardC@zapsurgical.com'], 'Test ' + i, 'i')
      // }
      // let testMembers = ['test1@randiouhnvf.com', 'test2@randiouhnvf.com', 'test3@randiouhnvf.com', 'test61@randiouhnvf.com', 'test1@randiouhnvf.com', 'test671@randiouhnvf.com', 'test14@randiouhnvf.com', 'test15@randiouhnvf.com']
      // sendMail(testMembers, 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: ${testMembers.join(', ')}`)
      // sendMail(['ZapBot@zapsurgical.com', 'RichardC@zapsurgical.com'], messageTemplates.bug.subject, createIssueMessage('bug', [{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}}], messageTemplates.bug.message))

      // sendMail(['richardlwchang@gmail.com'], 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: ${[...managerGroup, ...testingGroup].join(', ')}`)
      // sendMail(['ZapBot@zapsurgical.com', 'RichardC@zapsurgical.com', 'fictionalemail.@fictionalemail1231235.com'], 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: noone`)
      
      // Handle the acceptance criteria issue where emails are sent to each individual assigned to the issue
      _.forEach(missingFields.acceptance, issue => {
        const assignee = _.get(issue, ['fields', 'System.AssignedTo'])
        if (!assignee) {return}
        // console.log('assignee', assignee)
        const regexMatch = assignee.match(/<(.*)>/)
        const email = regexMatch && regexMatch[1]
        acceptanceGroup[email] = _.unionBy(acceptanceGroup[email] || [], [issue], 'id')
      })

      _.forEach(acceptanceGroup, (issueList, email) => {
        sendMail(
          options.test ? 'RichardC@zapsurgical.com' : email,
          messageTemplates.acceptance.subject,
          {
            message: messageTemplates.acceptance.message,
            addenum: createIssueMessage('acceptance', issueList, messageTemplates.acceptance.defaultIssue)
          },
          undefined,
          console.error
        )
      })
      
      _.forEach(missingFields, (field, type) => {
        if ((type) == 'acceptance') {return}
        if (!_.isEmpty(field)) { issues.push(type) }
      })

      // if (!options.test) {
        // console.log('test does not work!')
        if (!_.includes(issues, 'test')) {
          sendMail(
            options.test ? 'RichardC@zapsurgical.com' : options.sendNotification.to || [...testingMembers, ...testingManagers],
            messageTemplates.goodJob.subject,
            messageTemplates.goodJob.message
            )
        }
        
        if (!_.some(issues, issue => _.includes(['bug', 'srs', 'version'], issue))) {
          sendMail(
            options.test ? 'RichardC@zapsurgical.com' : options.sendNotification.to || [...projectMembers, ...projectManagers],
            messageTemplates.goodJob.subject,
            messageTemplates.goodJob.message
            )
        }
      // }

        _.forEach(issues, issue => {
          // console.log(createIssueMessage(issue, missingFields[issue], messageTemplates[issue].defaultIssue))
          let target
  
          switch(issue) {
            case 'test':
              target = [...testingManagers, ...testingMembers]
              break;
            case 'srs':
              target = [...srsMembers, ...projectMembers, ...projectManagers]
              break;
            default:
              target = [...projectMembers, ...projectManagers]
          }
          finalContacts = [...finalContacts, ...target, ...Object.keys(acceptanceGroup)]
          finalContacts = _.uniq(finalContacts)
          // console.log('target', target)
            // if (!options.test) {

            sendMail(
              options.test ? 'RichardC@zapsurgical.com' : options.sendNotification.to || target,
              messageTemplates[issue].subject,
              {
                message: messageTemplates[issue].message,
                addenum: createIssueMessage(issue, missingFields[issue], messageTemplates[issue].defaultIssue)
              },
              undefined,
              console.error
            )
          }
        // }
        )

       
      finalContacts = _.uniq(finalContacts)
      const uncontacted =_.difference(initialContacts, finalContacts).join(', ')
      sendMail(
        'RichardC@zapsurgical.com',
        'Email Summary',
        `Emails ${options.test ? '<strong>would be</strong>' : 'were'} sent to the following indiviudals: ${finalContacts.join(', ')}. ${ uncontacted ? 'The following individuals were not e-mailed this time: ' + uncontacted : ''}`
      )    
      }      
    return missingFields
  } catch (err) {
    console.log(err, err.stack)
  }
}

function createIssueMessage(key, issues=[], defaultMessage) {
  // console.log('defaultMessage', defaultMessage)
  const issuesHtml = `
      You might want to check out the ones below:
      <h3>${key.toUpperCase()} Issues</h3>
      <table>
        <tr>
          <th>Work Id</th>
          <th>Issue</th>
        </tr>
        ${_.map(issues, (issue) => {
          // console.log('issue', issue)
          const url = `https://zapsurgical.visualstudio.com/${issue.fields["System.TeamProject"]}/_workitems/edit/${issue.id}`
          return (
            "<tr>\
            <td><a href='" + url + "'>" + issue.id + "</a></td>\
            <td>" + (issue.message || defaultMessage) + "</td>\
            </tr>"
            )
        }).join('')}
      </table>
      <br/>
      <p>${messageTemplates[key].signature}</p>
      <p> 🤖 Zap Bot</p>
    `

  return issuesHtml
}

function hasOpenBugIssue(item) {
  const {fields} = item
  const allowedBugStates = ['Done', 'Approved', 'Committed', 'Removed']

  return (
    fields["System.WorkItemType"] == "Bug" &&
    _.includes(allowedBugStates, fields['System.State']) &&
    (
      parseSeverity(item, "Custom.RMSeverity") >= 2 ||
      parseSeverity(item, "Microsoft.VSTS.Common.Severity" >= 2)
    )
  )
}

function parseSeverity(item, field) {
  const {fields} = item
  return Number(_.get(fields, field, 0))
}
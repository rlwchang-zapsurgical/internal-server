const serviceHook = require('../../helpers/vsts/serviceHook')
const response = require('../../helpers/response')
const utils = require('../../utils')
const axios = require('axios')
const _ = require('lodash')

const API_URL = `http://localhost:${process.env.PORT}/api`
const bucket = 'zap-surgical-installers'
exports.handleVstsHook = handleVstsHook
async function handleVstsHook(req, res, next) {
  const build = serviceHook.formatBuildData(req.body)
  const Axios = axios.create({
    headers: _.omit(req.headers, 'content-length')
  })

  if (build.type == 'release')  {
    handleReleaseCandidate(build, {req})
  }

  try {
    await Axios.post(`${API_URL}/builds`, build)
    response.successResponse(res, build)
  } catch (err) {
    response.errorResponse(res, err)
  }
  // next()
}

function handleReleaseCandidate(baseData, context) {
  addSoftwareToDB(baseData, context)
  addSoftwareCompatabilityToDB(baseData, context)
}

function addSoftwareToDB(baseData, context={}) {
  const {build} = baseData
  const Axios = axios.create({
    headers: _.omit(_.get(context, 'req.headers', axios.default.headers), 'content-length')
  })

  Axios.post(`${API_URL}/softwares`, {...baseData, name: baseData.softwareName})
  
  let types = []
  let fileName = ''

  switch(baseData.projectName) {
    case 'Treatment':
      console.log('is treatment')
      types = ['Pendant', 'TDCS', 'View']
      for (const type of types) {
        fileName = `${baseData.softwareName}-${type}-${build.buildNumber}-${build.buildId}.zip`
        const data = {
          ...baseData,
          name: `${baseData.softwareName}-${type}`,
          file: {
            ...baseData.file,
            s3ObjectName: `${baseData.projectName}/${fileName}`
          }
        }

        Axios.post(`${API_URL}/softwares`, data)
      }
      break;
    case 'TPS':
      console.log('is tps')
       types = ['App', 'Service']
       for (const type of types) {
         fileName = `${baseData.projectName}-${type}-${build.buildNumber}-${build.buildId}.zip`
         const data = {
          ...baseData,
          name: `${baseData.softwareName}-${type}`,
          file: {
            ...baseData.file,
            s3ObjectName: `${baseData.projectName}/${fileName}`
          }
        }
         Axios.post(`${API_URL}/softwares`, data)
       } 
  }
}

async function addSoftwareCompatabilityToDB(softwareData, context={}) {
  console.log('softwareData', softwareData)
  const compatabilityData = {
    author: softwareData.build.author,
    software: {}
  }
  console.log('softwareData', softwareData)
  const Axios = axios.create({
    headers: _.omit(_.get(context, 'req.headers', axios.default.headers), 'content-length')
  })
  
  try {
    const {data: {data: softwares}} = await Axios.get(`${API_URL}/softwares`)
    const softwareName = softwareData.softwareName || _.get(softwareData, 'build.projectName')
    const version = softwareData.version || _.get(softwareData, 'file.version')
    console.log('softwareName', softwareName)
    switch(softwareName.toLowerCase()) {
      case 'tps':
        console.log('isTps')
        compatabilityData.software.tps = softwareData.version
        break;
      case 'treatment':
        console.log('isTreatment')
        compatabilityData.software.tds = softwareData.version
        break;
      case 'sitemgmt':
      case 'broker':
        console.log('isBroker')
        compatabilityData.software.broker = softwareData.version
        break;
    }
    
    for (const software of softwares) {
      if (compatabilityData.software.tps && compatabilityData.software.tds && compatabilityData.software.broker) {break}
      
      switch(utils.parse.softwareType(software.name)) {
        case 'tps':
          compatabilityData.software.tps = utils.version.getGreaterVersion(compatabilityData.software.tps, software.version || _.get(software, 'file.version'))
          break;       
        case 'tds':
          compatabilityData.software.tds = utils.version.getGreaterVersion(compatabilityData.software.tds, software.version || _.get(software, 'file.version'))
          break;       
        case 'broker':
          compatabilityData.software.broker = utils.version.getGreaterVersion(compatabilityData.software.broker, software.version || _.get(software, 'file.version'))
          break;               
      }
    }

    console.log('compatabilityData', compatabilityData)

    Axios.post(`${API_URL}/softwareCompatabilities`, compatabilityData)
  

  } catch (err) {
    console.error('Failed to add hook data to software comptability table.', err)
    throw err
  }
}

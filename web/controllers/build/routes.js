const router = require('express').Router()
const controller = require('./');
const axios = require('axios')

router.route('/hook')
.post(controller.handleVstsHook)

router.route('/')
.get(controller.BuildIndex)


router.route('/:id')
.get(controller.BuildShow)
.put(controller.BuildUpdate)
// router.route('/test')
// .post( async (req, res) => {
//   const Axios = axios.create({
//     headers: req.headers,
//     timeout: 1000 * 60,
//   })

  // console.log(req.headers)

//   const testHeader = {
//     'content-type': 'application/json',
//   authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDE0NTI3NzAzMjEsImV4cCI6MTU0NDA0NDc3MDMyMSwic3ViIjoiNWFlNzQ2ZDc3MWM4MDk3MjcwMjBlNmVjIn0.GCFx4__69By4TarJEP04jkdIlH_c17fmQnrrhfPsCdc',
//   'cache-control': 'no-cache',
//   'postman-token': '3f0f5ef3-e6eb-47e3-abf3-107b91090d9b',
//   'user-agent': 'PostmanRuntime/7.4.0',
//   accept: '*/*',
//   host: 'localhost:3000',
//   cookie: 'connect.sid=s%3AsFSiYxiRzgXw4_x6grlsm9njM9-PunNr.opNdUzVNDCEzHZzxfG9JHJlwGqplflh2Bs%2Bn7Kfwemo',
//   'accept-encoding': 'gzip, deflate',
//   // 'content-length': '4976',
//   connection: 'keep-alive'
// }

//   const instance = axios.create({
//     // headers: req.headers,
//     headers: testHeader,
//   })
  

//   // console.log('custom', Axios.get)
//   // console.log('native', axios.get)

//   try {
//     const results = await instance.get(`http://localhost:${process.env.PORT}/api/builds/test`)
//     // console.log('resukts', results)
//     console.log('results were achieved')
//     res.json(results)
//   } catch(err) {
//     console.log(err)
//     res.json(err.message)
//   }
// })
// .get(async (req, res) => {
//   res.json(req.headers)
// })

module.exports = router

const _ = require('lodash')
const {restful, file, response} = require('../../helpers')
const {Build} = require('../../../models')
const serviceHook = require('../../helpers/vsts/serviceHook')
// const omissionList = [
//   'index.js',
//   'archive',
//   'routes.js'
// ]
exports = restful.createRestfulMethods('Build')
// require("fs").readdirSync(__dirname).forEach(function (file) {
//   if (!_.includes(omissionList, file)) {
//      controllers = {...controllers, ...require(`./${file.replace('.js', '')}`)}
//   }
// });

// module.exports = controllers

exports.handleVstsHook = handleVstsHook
async function handleVstsHook(req, res, next) {
  const build = serviceHook.formatBuildData(req.body)
  req.body = build
  // console.log('req.body', req.body)
  return exports.BuildCreate(req, res)
}

// Check to see if you want to populate all the builds from the get go. It adds a lot of work that may or may not be necessary.
exports.BuildIndex = BuildIndex
async function BuildIndex(req, res) {
  let results = await Build.find()
  .populate('masterDisks.control')
  .populate('masterDisks.operator')
  .populate('masterDisks.database')

  results = await file.addUrl(results)
  results = await file.addUrl(results, 'masterDisks.control')
  results = await file.addUrl(results, 'masterDisks.operator')
  results = await file.addUrl(results, 'masterDisks.database')
  
  // results.forEach(element => {
  //   if (!element.name) {
  //     console.log(element)
  //   }
  // });

  results = _.sortBy(results, 'build.buildId').reverse()
  results = _.uniqBy(results, 'build.buildId')

  response.successResponse(res, results)
}

exports.BuildShow = BuildShow
async function BuildShow(req, res) {
  let results = await Build.findById(req.params.id)
  .populate('masterDisks.control')
  .populate('masterDisks.operator')
  .populate('masterDisks.database')

  results = await file.addUrl(results)
  results = await file.addUrl(results, 'masterDisks.control')
  results = await file.addUrl(results, 'masterDisks.operator')
  results = await file.addUrl(results, 'masterDisks.database')
  response.successResponse(res, results)
}

exports.BuildUpdate = BuildUpdate
async function BuildUpdate(req, res, next) {
  let build = await Build.findOne({_id: req.params.id})

  for (const field in req.body) {
    const value = req.body[field]
    // console.log(field, value)
    if (value && typeof value == 'object' && !Array.isArray(value)) {
      build[field] =  _.merge(build.toObject()[field], req.body[field]) 
    } else {
      build[field] =  req.body[field] || undefined
    }
  }

  // Handle erasing associated masterDisk
  for (const pcName in build.masterDisks) {
    const value = _.get(req.body, `masterDisks.${pcName}`)
    if (pcName.match(/operator|control|database/i) && value && _.isEmpty(value)) {
      build.masterDisks = _.omit(build.masterDisks.toObject(), pcName)
    }
  }

  await build.save()
  // console.log('body', req.body)
  let results = await Build.populate(
    build,
    [
      {path: 'masterDisk.operator'},
      {path: 'masterDisk.control'},
      {path: 'masterDisk.database'},
    ]
  )
  // console.log('results', results)
  response.successResponse(res, results)
}

// module.exports = {...require('./hook'), ...exports}
module.exports = {...exports}


// This is to forcibly overwrite all of the older data and include the 'file' attribute.
// const models = require('../../../models')

// async function run() {
//   const Build = models.Build

//   const builds = await Build.find()

//   for(let buildInstance of builds) {
//     const buildCopy = buildInstance.toObject()
//     if (!buildCopy.name) {buildInstance.name = buildCopy.projectName}
//     // const {build, version, projectName} = buildCopy
//     // buildInstance.file = {
//     //   name: `${projectName}-${build.buildNumber}-${build.buildId}.zip`,
//     //   version: version,
//     //   s3ObjectName: `${parseObjectName(projectName)}-${build.buildNumber}-${build.buildId}.zip`,
//     //   s3BucketName: 'zap-surgical-installers',      
//     // }
//     buildInstance.save()
//   }
  
// }

// function parseObjectName(projectName) {
//   switch(projectName) {
//     case 'SiteMgmt':
//     case 'SiteManagement':
//     case 'Broker':
//       return 'SiteMgmt/Broker'
//     default:
//       return `${projectName}/${projectName}`
//   }
// }

// run()
const router = require('express').Router()
const controller = require('./')

router.route('/url')
.post(controller.getUrls)

router.route('/update')
.post(controller.updateDB)

router.route('/delete')
.post(controller.deleteUpload)


module.exports = router
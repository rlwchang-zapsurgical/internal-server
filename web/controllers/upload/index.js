// TODO create upload url generation and affector using the helper methods
// exports.getUrl = getUrl

const {upload, response} = require('../../helpers')
const {func} = require('../../utils')
const mime = require('mime')
const _ = require('lodash')


const fileFormat = {
  name: String,
  type: String,
  mime: String,
}

const optionsFormat = {
  prefix: String,
  bucket: String,
  Model: String
}

exports.getUrls = getUrls
async function getUrls(req, res, next) {
  let {files, file, options} = req.body
  if (!files) {files = file}
  // files = formatFiles(options)
  // options = formatOptions(options)
  try {
    const results = await upload.getUrls(files, options)
    return response.successResponse(res, results)

  } catch(err) {
    return response.errorResponse(res, err)
  }
} 

exports.updateDB = updateDB
async function updateDB(req, res, next) {
  let {files, file, options} = req.body
  if (!files) {files = file}
  // files = formatFiles(options)
  // options = formatOptions(options)
  try {
    const results = await upload.addToDB(files, options)
    return response.successResponse(res, results)
    
  } catch(err) {
    console.log(err)
    return response.errorResponse(res, err)
  }
}

exports.deleteUpload = deleteUpload
async function deleteUpload(req, res, next) {
  let {files, file, options} = req.body
  if (!files) {files = file}
  // files = formatFiles(options)
  // options = formatOptions(options)
  try {
    const results = await upload.remove(files, options)
    return response.successResponse(res, results)
  } catch(err) {
    return response.errorResponse(res, err)
  }
}

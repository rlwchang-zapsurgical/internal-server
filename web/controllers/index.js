
const pluralize = require('pluralize')
const restful = require('../helpers/restful')
const _ = require('lodash')
const fs = require("fs")
const path = require('path')
const exceptionList = [
  'index.js',
  'archive',
  'auth',
  'README.md'
]

const isAuthenticated = require('../middleware/isAuthenticated')

module.exports = (app) => {
  fs.readdirSync(__dirname).forEach(function (file) {
    let customRoutes = true
    if (!_.includes(exceptionList, file)) {
      const routePath = `./${file}/routes`
      let routes
      
      if (fs.existsSync(path.resolve(__dirname, `${routePath}.js`))) {
        routes = require(`./${file}/routes`)
        if (_.isEmpty(routes)) {
          routes = require('express').Router()
          customRoutes = false
        } 
      } else {
        routes = require('express').Router()
        customRoutes = false
      }

     
      if (!customRoutes) {console.warn(`There are no routes specified for ${file.toUpperCase()}, so only default REST routes have been loaded. Did you mean for that to happen?`)}

      restful.addRestfulRoutes(routes, {modelName: file, defaultIdKey: routes.defaultIdKey})
      app.use(`/api/${pluralize(file)}`, isAuthenticated, routes)
    }
  });
  
  app.use(`/api/auth`, require('./auth/routes'))  
}

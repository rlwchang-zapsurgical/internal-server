const router = require('express').Router()
const controller = require('./')

router.route('/')
.post(controller.UserCreate)

router.route('/register')
.post(controller.UserCreate)

router.route('/:id')
.put(controller.UserUpdate)

module.exports = router
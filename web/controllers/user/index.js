const {User} = require('../../../models')
const _ = require('lodash')
const {response} = require('../../helpers')
const csv = require('csvtojson')

exports.UserCreate = UserCreate
async function UserCreate(req, res) {
  if (req.body.password != req.body.verifyPassword) {
    response.errorResponse(res, null, 'Please enter the same password.')
  }

  const results = await User.register(_.omit(req.body, ['password']), req.body.password)
  response.successResponse(res, results)
}

exports.UserUpdate = UserUpdate
async function UserUpdate(req, res) {
  const results = await User.findById(req.params.id)

  _.forEach(_.omit(req.body, ['password', 'oldPassword', 'newPassword', 'salt', 'hash']), (value, key) => {
    results[key] = value
  })

  if (req.body.oldPassword && req.body.newPassword) {
    try {
      await results.changePassword(req.body.oldPassword, req.body.newPassword)
    } catch (err) {
      response.errorResponse(res, err)
    }
  }

  await results.save()

  response.successResponse(res, results)
}

// async function run() {
//   const results = await csv({noheader:true}).fromFile('./UsersList.csv')
//   for (const employee of results) {
//     const firstName = employee.field1
//     const lastName = employee.field2
//     const number = employee.field3.replace(/[.-]/g, '')
//     const email = employee.field5

//     const username = email.match(/(.*)@.+\.com/) && email.match(/(.*)@.+\.com/)[1].toLowerCase()
//     try {
//       await User.register({
//         firstName,
//         lastName,
//         number,
//         email,
//         username
//       }, firstName.toLowerCase())
//     } catch(err) {
//       console.warn('Error for: ', firstName, lastName, err.message)
//     }
//   }
// }

// run()
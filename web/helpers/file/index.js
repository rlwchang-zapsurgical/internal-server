const _ = require('lodash')
const aws = require('../../../packages/aws')


exports.addUrl = addUrl
async function addUrl(modelInstance, path=['file'], options={}) {
  // let semiFinalPath
  
  // if (Array.isArray(path)) {
  //   semiFinalPath = path.slice(0, path.length - 1)
  // } else {
  //   semiFinalPath = path.replace('[', '.').replace(']', '').split('.')
  //   semiFinalPath = semiFinalPath.slice(0, semiFinalPath.length - 1)
  // }
  
  if (Array.isArray(modelInstance)) {
    const results = []
    for(let instance of modelInstance) {
      results.unshift(await formatInstanceUrl(instance, path, options))
    }
    return results
  } else {
    // console.log('modelInstance', {...modelInstance})
    return await formatInstanceUrl(modelInstance, path, options) 
  }
}

async function formatInstanceUrl(instance, path, options={}) {
  try {
    const instanceCopy = instance.toObject ? instance.toObject() : instance 
    // console.log('instanceCopy', instanceCopy)
    const fileObject = _.get(instanceCopy, path)
    // console.log('path', path)
    // The parentPath and parentObject are primarily for use in the options
    // If the user pases in a function for options,
    // then the function will be called with the parentObject of where the file url was to be added
    const parentPath = Array.isArray(path) ? path.slice(-1) : path.replace(/(\.?\[?\w*?\]?)$/, '')
    const parentObject = _.get(instanceCopy, parentPath, instanceCopy)
    
    
    switch (typeof options) {
      case 'function':
        options = options(parentObject)
    }
  
    if (Array.isArray(fileObject)) {
      // console.log('isArray')
      // This means it is an array of file objects
      for (const file of fileObject) {
        if (file.s3ObjectName) {file.url =  await aws.s3.getUrl(file.s3BucketName || instanceCopy.s3BucketName, file.s3ObjectName, options, options.additionalParams)}
        // console.log('file.url', file.url)
      }
  
    } else if (typeof fileObject == 'object') {
      // console.log('isObject')
      const file = fileObject
      if (file.s3ObjectName) {file.url =  await aws.s3.getUrl(file.s3BucketName || instanceCopy.s3BucketName, file.s3ObjectName, options, options.additionalParams)}    
      // console.log(file)
    } else {
      // console.log('isOther')
      // console.log(typeof fileObject)
    }
  
  
    return instanceCopy
  } catch (e) {
    console.warn(e)
  }
  // console.log(typeof instance)
}
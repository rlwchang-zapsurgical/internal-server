const speakeasy = require('speakeasy');
const _ = require('lodash')

exports.createToken = createToken
function createToken(secret, options={}) {
  return speakeasy.totp({
    secret: secret,
    encoding: options.encoding || 'base32'
  })
}

exports.verifyToken = verifyToken
function verifyToken(token, secret, options={}) {
  return speakeasy.totp.verify({
    encoding: options.encoding || 'base32',
    window: options.window || 6,
    secret,
    token
  })
}


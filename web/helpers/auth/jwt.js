const jwt = require('jwt-simple');

exports.createToken = createToken
function createToken(secret, sub, options={}) {
  const tokenParams = {
    iat: Date.now(),
    exp: Date.now() + (options.exp || 1000 * 60 * 60 * 24 * 30),
    sub
  }
  return jwt.encode(tokenParams, secret)
}

exports.verifyToken = verifyToken
function verifyToken(token, secret) {
  try {
    const payload = jwt.decode(token, secret)
    if (payload.exp < Date.now()) {
      return false
    }
    return payload.sub

  } catch(err) {
    return false
  }
}
let auth = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive' &&
      file !== 'README.md') {
      
    file = file.replace('.js', '')
    auth = {...auth, [file]: require(`./${file}`)}
  }
});

module.exports = auth
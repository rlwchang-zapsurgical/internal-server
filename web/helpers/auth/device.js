const _ = require('lodash')

exports.checkDevices = checkDevices
function checkDevices(fingerprint, rememberedDevices=[]) {
  return (_.includes(rememberedDevices, fingerprint))
}

// This can be modified to actually change the devices on the user rathen than returning a new value
exports.rememberDevice = rememberDevice
function rememberDevice(rememberedDevices=[], fingerprint, remove=false) {
  if (remove) {
    return _.filter(rememberedDevices, device => device == fingerprint)
  } else {
    return _.union(rememberedDevices, fingerprint)
  }
}
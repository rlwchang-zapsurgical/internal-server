const models = require('../../models')
const response = require('./response')
const _ = require('lodash')
const {addUrl} = require('./file')



function createRestfulMethods(modelName, options={}) {
  const modelNameProper = _.startCase(modelName).replace(/\W+/gi, '')
  const Model = models[modelNameProper]
  const methods = {}
  const {defaultIdKey='id'} = options
  methods[`${modelName}Index`] = async (req, res) => {
    try {
      let result = await Model.find()
      result = await addUrl(result)
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
      throw err
    }
  }

  methods[`${modelName}Show`] = async (req, res) => {
    const {id} = req.params
    try {
      let result = await Model.findOne({[defaultIdKey]: id})
      if (!result) {result = await Model.findById(id)}
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
      throw err
    }
   }

   methods[`${modelName}Create`] = async (req, res) => {
    // let {data} = req.body
    // if (!data) {data = req.body}
    let data = req.body
    // console.log('data', data)
    try {
      let result = await Model.create(data)
      // result = await addUrl(result)
      // console.log('result', result)
      response.successResponse(res, result)
    } catch(err) {
      // console.log('err', err)
      response.errorResponse(res, err)
      throw err
    }
   }

   methods[`${modelName}Update`] = async (req, res) => {
    const {id} = req.params
    // let {data} = req.body
    // if (!data) {data = req.body}
    let data = req.body

    try {
      let result = await Model.findOne({[defaultIdKey]: id})
      if (result) {
        result = await Model.findOneAndUpdate({[defaultIdKey]: id}, data, {new: true})
      } else {
        result = await Model.findByIdAndUpdate(id, data, {new: true})
      }
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
      throw err
    }
   }

   methods[`${modelName}Destroy`] = async (req, res) => {
    const {id} = req.params
    try {
      let result = await Model.findOne({[defaultIdKey]: id})

      if (result) {
        result = await Model.findOneAndRemove({[defaultIdKey]: id})
      } else {
        result = await Model.findByIdAndRemove(id)
      }
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
      throw err
    }
   }

   return methods
}

function addRestfulRoutes(router, options={}) {
  if (typeof options == 'string') {options = {modelName: options}}
  let {model, modelName, Model, include=['index', 'create', 'show', 'update', 'destroy']} = options
  if (!modelName) {
    if (model) {modelName = model.name || model}
    else if (Model) {modelName = Model.name || Model}
  }
  const restfulMethods = createRestfulMethods(modelName, options)
  if (_.includes(include, 'index')) { router.route('/').get(restfulMethods[`${modelName}Index`]) }
  if (_.includes(include, 'create')) { router.route('/').post(restfulMethods[`${modelName}Create`]) }
  if (_.includes(include, 'show')) { router.route('/:id').get(restfulMethods[`${modelName}Show`]) }
  if (_.includes(include, 'update')) { router.route('/:id').put(restfulMethods[`${modelName}Update`]) }
  if (_.includes(include, 'destroy')) { router.route('/:id').delete(restfulMethods[`${modelName}Destroy`]) }    
}

module.exports = {
  createRestfulMethods,
  addRestfulRoutes
}
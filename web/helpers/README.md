# Web Helpers

Here are where reusable "architectural" pieces of code are stored. For example, pieces that give out an error response to an HTTP request would be stored here.

## FAQ

### How is this different than the helpers section?
In the utils section, I include functions that perform simple computation and yield simple information, but are not responsible for any "architectural" tasks. They simply take an input piece of data and return an output. A version util might help me sort and return a list of version numbers for example. Helpers on the other hand may or may not return anything but they perform a certain function. They could send another HTTP request for example.
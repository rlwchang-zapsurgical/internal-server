const utils = require('../utils')
const aws = require('../../packages/aws')
const models = require('../../models')
const _ = require('lodash')
const pluralize = require('pluralize')
const Bottleneck = require('bottleneck')
const limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 333 // This is how fast each request is allowed to fire
})


exports.getUrl = getUrl
async function getUrl(file={}, options={}) {
  // console.log('file', file)
  // console.log('isValid', utils.file.isValidType(file))
  if (!utils.file.isValidType(file)) {return null}

  let key

  if (file.s3ObjectName) { key = file.s3ObjectName }
  else if (options.prefix) { key = `${options.prefix}/${file.name}` }
  else { key = file.name }

  const url = await aws.s3.getUrl(
    options.bucket,
    key,
    {action: 'putObject'},
    {
        ContentType: file.mime || file.type,
        ServerSideEncryption: "AES256",        
    }
  )
  return url
}

exports.getUrls = getUrls
async function getUrls(files=[], options={}) {
  const results = {}
  
  if (!Array.isArray(files)) {files = [files]}
  formatFiles(files)
  formatOptions(options)
  // console.log('files', files)
  
  for (const file of files) {
    const url = await getUrl(file, options)
    results[file.name] = {
      ...file,
      url
    }
  }
  
  return results
}

// This adds a file(s) to one model instance or document at a time
// exports.addToDB = addToDB
const addToDBWrapped = limiter.wrap(addToDB)
exports.addToDB = addToDBWrapped
async function addToDB(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}
  formatOptions(options)
  // formatFiles(files)
  let {
    Model,
    query,
    property,
    bucket,
    prefix,
    overwrite=true,
    replace=false
  } = options
  // console.log('options', options)
  // console.log('invoking findOne!!!!!!!!!')
  // console.log('models', models.SoftwareCompatability)
  if (typeof Model == 'string') {Model = models[_.startCase(Model).replace(/\W+/gi, '')]}
  if (typeof query == 'string') {query = {_id: query}}

  const modelInstance = await Model.findOne(query)

  // If there is only one file and property does not exist, add the file to the "file" property
  // This is meant for the "main" file for a model. For example, the software model would have a file property that would include its information
  if ((!property || _.isEmpty(property)) && files.length == 1) {
    let file = utils.file.formatFileData(files[0], options)
    
    modelInstance.file = file
    await modelInstance.save()

    return modelInstance.toObject()

  // Otherwise use the property field to drill down into the modelInstance and shove all the files there.
  } else {
    if (Array.isArray(property)) {property = property.join('.')}

    const secondToLastProperty = property.replace(/\[?\.?\w*\]?$/, '')
    const lastProperty = property.match(/(\w+)\]?$/)[1]

    if (!secondToLastProperty || _.has(modelInstance, secondToLastProperty)) {
      let pointer = _.get(modelInstance.toObject(), secondToLastProperty, modelInstance)
      let filesFormatted = utils.file.formatFilesData(files, options)

      filesFormatted = filesFormatted.filter((file) => utils.file.isValidType(file, options))
      
      if (replace) {
        pointer[lastProperty] = filesFormatted
      } else if (overwrite) {
        pointer[lastProperty] = _.unionWith(pointer[lastProperty] || [], filesFormatted, _.isEqual)
      } else {
        pointer[lastProperty] = pointer[lastProperty].concat(filesFormatted)
      }

      await pointer.save()
      return pointer.toObject()

    } else {
      console.error(`The property ${property} does not exist!`)
    }
  }
}

const removeWrapped = limiter.wrap(remove)
exports.remove = removeWrapped
// exports.remove = remove
async function remove(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}
  
  try {
    files = files.filter(file => utils.file.isValidType(file, options))
    const result = await aws.s3.deleteObjects(files, options)
    if(result) {return await removeFromDB(files, options)}

  } catch (err) {
    console.error(err)
    throw err
  }
}

exports.removeFromDB = removeFromDB
async function removeFromDB(files=[], options={}) {
  // TODO: This function currently does not remove singular files. Implement method to remove singular files
  formatOptions(options)
  // formatFiles(files)

  let {
    Model,
    query,
    property,
  } = options
  
  try {
    if (!Array.isArray(files)) {files = [files]}
    files = files.filter(file => utils.file.isValidType(file, options))
    if (typeof Model == 'string') {Model = models[_.startCase(Model).replace(/\W+/gi, '')]}
    if (typeof query == 'string') {query = {_id: query}}
    const modelInstance = await Model.findOne(query)
    let filesFormatted = utils.file.formatFilesData(files, options)
    let remainingFiles = _.get(modelInstance.toObject(), property)
    let secondToLastProperty
    let lastProperty
    if ((!property || _.isEmpty(property)) && files.length == 1) {
      modelInstance.file = {}
      await modelInstance.save()
  
      return modelInstance.toObject()
    } else {
      secondToLastProperty = property.replace(/\[?\.?\w*\]?$/, '') // This includes everything up to and including the second to last
      lastProperty = property.match(/(\w+)\]?$/)[1]
  
      if (!secondToLastProperty || _.has(modelInstance, secondToLastProperty)) {
        let pointer = _.get(modelInstance.toObject(), secondToLastProperty, modelInstance)
        
        pointer[lastProperty] = filterFiles(filesFormatted, remainingFiles)
        await pointer.save()
        return pointer.toObject()
  
      } else {
        console.error(`The property ${property} does not exist!`)
      }
    }
  
    
  } catch (err) {
    console.error(err)
    throw err
  }

  function filterFiles(filesToRemove, initialFiles) {
    let remainingFiles = initialFiles
    for(let file of filesToRemove) {
      // Try filtering first.
      const filteredUploads = _.filter(remainingFiles, (upload) => upload.name != file.name)
      if (filteredUploads.length == (remainingFiles.length - 1)) {
        remainingFiles = filteredUploads
        continue
      } else{
        // If there is more than one element removed, check using deep comparison
        const filteredUploads = _.filter(remainingFiles, (upload) => !_.isEqual(upload, file))
        
        if (filteredUploads.length == (remainingFiles.length - 1)) {
          remainingFiles = filteredUploads
          continue
        } else {
          // If there is no match with deep comparison, delete first instance
          const targetObject = _.find(remainingFiles, (upload) => upload.name == file.name)
          remainingFiles = _.filter(remainingFiles, (upload) => !_.isEqual(upload, targetObject))
        }
      }
    }

    return remainingFiles
  }
    
  pointer[secondToLastProperty] = remainingFiles

  await pointer.save()
  return pointer.toObject()
}

function formatOptions(options) {
  // const options = _.cloneDeep(originalOptions)
  options.Model = options.Model && _.startCase(pluralize.singular(options.Model)).replace(' ', '')
}

function formatFiles(files) {
  
}
let serviceHook = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive' &&
      file !== 'README.md') {
      
    file = file.replace('.js', '')
    serviceHook = {...serviceHook, ...require(`./${file}`)}
  }
});

module.exports = serviceHook
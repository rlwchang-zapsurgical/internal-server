const utils = require('../../../utils/')

exports.formatVstsData = formatVstsData
function formatVstsData(data) {
  // Extract out useful data from the webhook
  const {
    resource: {
      id: buildId,
      url: buildJsonUrl,
      buildNumber,
      startTime: buildStart,
      finishTime: buildEnd,
      project: { name: projectName = `Unnamed Project - ${data.resource.definition.name} | ${data.resource.buildNumber}` } = {},
      definition: { name: definition = `No definition name` } = {},
      // log: {url: logUrl},
      requestedBy: { displayName: authorBy } = {},
      requestedFor: { displayName: authorFor } = {},
      sourceBranch,
      sourceVersion: commitHash,
      reason
    },
  } = data;

  const buildUrl = buildJsonUrl.replace('_apis/build/Builds/', '_build/index?buildId=')
  const buildDuration = new Date(buildEnd) - new Date(buildStart)
  const fullBranchName = sourceBranch;
  const branchName = fullBranchName ? utils.parse.branch(fullBranchName) : 'Branch and Version not defined';

  const buildData = {
    projectName,
    buildId,
    buildStart,
    buildEnd,
    buildDuration,
    buildNumber,
    buildUrl,
    buildJsonUrl,
    branchName,
    fullBranchName,
    commitHash,
    definition,
    reason,
    author: authorFor || authorBy
  }

  return buildData
}

exports.formatBuildData = formatBuildData
function formatBuildData(data) {
  const bucket = 'zap-surgical-installers'
  const buildData = formatVstsData(data)

  const softwareName = parseSoftwareName(buildData.projectName)
  
  let version
  
  if (buildData.fullBranchName.match(/release/i)) { version = utils.parse.version(buildData.branchName) }
  else if (buildData.reason == 'manual') { version = 'manual' } // Added to differentiate from non-release and pull request builds.

  const build = {
    name: buildData.projectName,
    projectName: buildData.projectName,
    softwareName: softwareName,
    version: version,
    type: utils.parse.release(buildData.branchName),
    dateUploaded: new Date(buildData.buildEnd),
    file: {
      name:  softwareName,
      version: version,
      s3ObjectName: `${buildData.projectName}/${softwareName}-${buildData.buildNumber}-${buildData.buildId}.zip`,
      s3BucketName: bucket,
    },
    build: buildData,
    releaseDocument: {
      name: `${softwareName} Release Document`,
      s3ObjectName: `${buildData.projectName}-${buildData.buildNumber}-${buildData.buildId}-ReleaseNotes.txt`,
      s3BucketName: bucket
    }
  }

  return build;
}

function parseSoftwareName(softwareName) {
  switch (softwareName.toLowerCase()) {
    case 'tds':
      return 'Treatment'
    case 'sitemgmt':
      return 'Broker'
    default:
      return softwareName
  }
}
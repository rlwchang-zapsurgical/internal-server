let vsts = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive') {
      vsts = {...vsts, [file]: require(`./${file}`)}

  }
});

module.exports = vsts
const _ = require('lodash')
const redis = require('../../../packages/redis')

exports.checkKey = checkKey
async function checkKey(cacheKey, options={}) {
  let {
    clearCache = false,
    clearOtherKeys = [],
  } = options

  let data = null;

  if (process.env.REDIS) {
    if (!Array.isArray(clearOtherKeys)) { clearOtherKeys = [clearOtherKeys] }
    _.forEach(clearOtherKeys, (key) => redis.del(key))
    
    if (clearCache) { redis.del(cacheKey) }
    data = await redis.get(cacheKey)
  }
  
  try {
    const parsedData = JSON.parse(data)
    return parsedData
  } catch (err) {
    return data // This will be null, a jid, or the result object
  }
}
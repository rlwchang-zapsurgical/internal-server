const _ = require('lodash')
const cache = require('./cache')
const axios = require('axios')
const redis = require('../../../packages/redis')
const response = require('./response')

// TODO better separate out the caching logic into the cache file.

const defaultOptionsForSaltCmd = {
  async: Boolean,
  pollingInterval: Number, //number in seconds to check
  timeOut: Number, //number in seconds to stop listening for results. Note that the request will still.
  cacheExp: Number,//number of seconds to expire the cache after,
  failedCacheExp: Number, //number of seconds to expire the cache after if the request fails,
  clearCache: Boolean, //whether or not to clear the cache of the running cmd
  clearOtherKeys: String, //name of key to clear
}

exports.run = run
async function run(cmd, tgt, arg, options={}) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }

  const params = getParams(cmd, tgt, arg, options)
  const cacheKey = JSON.stringify(_.pick(params, ['client', 'tgt', 'arg', 'fun']))
  // let result = options.clearCache ? null : await cache.checkKey(cacheKey)
  let result = null
  if (!result) { 
    result = await execute(cmd, tgt, arg, options, params)
    const failed = checkRetrievedDataForFailure(result)
    let expiration
    if (failed && options.failedCacheExp) { expiration = options.failedCacheExp }
    else if (!failed && options.cacheExp) { expiration = options.cacheExp }

    // console.log('failed', failed)
    // console.log('expiration', expiration)
    // console.log('options.cacheExp', options.cacheExp)

    if (process.env.REDIS && expiration) {
      redis.set(cacheKey, JSON.stringify(result), 'EX', expiration)
    }
   }
  return result
}


exports.execute = execute
async function execute(cmd, tgt, arg, options={}, saltParams) {
  const params = saltParams || getParams(cmd, tgt, arg, options)

  if (options.isAsync) {
    // If they specify to run the command asynchronously, it will return a job id or jid.
      try {
        const jidRequest = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/run`, params)
        const jid = jidRequest.return[0].jid
        return jid
      } catch(err) {
        console.log(err)
      }
    // }
  } else {
    // This is the synchronous version which is the default. It will return the actual results instead of a job id.
    try {
      let result = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/run`, params)
      return result
    } catch (err) {
      console.log('error from execute', err)
      throw (err)
    }
  }

}

exports.getParams = getParams
function getParams(cmd, tgt='*', arg, options={}) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }
  // Allow you to pass in both strings and objects with more details
  // By default, tgt_type is a glob. Possible types include 'grains' and 'compound'
  let target = typeof tgt == 'string' ? {tgt} : tgt
  let client = options.client || (options.isAsync ? 'local_async' : 'local')

  const params = {
    username: process.env.SALT_USERNAME,
    password: process.env.SALT_PASSWORD,
    eauth: process.env.SALT_EAUTH,
    client: client,
    tgt: target.tgt,
    tgt_type: target.tgt_type,
    arg,
    kwarg: {
      pillar: options.pillar,
      test: options.test
    },
    fun: cmd
  }

  return params
}

// ====================
// HELPERS
// ====================


// This will likely not work since executeSaltCmd doesn't exist
exports.applyStates = applyStates
async function applyStates(states=[], tgt, options={}, saltParams) {
  const stateString = states.join(",")
  const result = await executeSaltCmd(
    'state.apply',
    tgt,
    stateString,
    options,
    saltParams
    )

  return result
}

async function getSaltJobResults(jid, options={}, res) {
  const {timeOut = 60, cacheExp} = options
  const job = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/jobs/${jid}`)
  const info = job.info[0]
  const result = job.return[0]
  const startTime = new Date(info.StartTime + ' GMT+0').getTime()
  if (!_.isEmpty(result) && info.Target == "unknown-target") {
    return res ? response.successResponse(res, job) : result
  }
  // The job took too long and timed out
  if (Date.now() - startTime > timeOut * 1000) {
    const timeoutError = new Error('Action failed to complete in the designated time.')
    const error = {
      error: timeoutError,
      message: `Failed to complete in ${timeOut} seconds` 
    }
    
    return res ? 
      response.errorResponse(res, error.error, error.message) :
      error
  }
  // The job is still pending
  return res ? response.pendingResponse(res, info) : {status: 'pending', timeOut: startTime + timeOut * 1000}
}

async function pollSaltJobResults(jid, handler, options={}) {
  const results = await pollingPromise(jid, handler, options)
  return results
}

function pollingPromise(jid, handler, options) {
  // handler is a user-defined function.
  // it is passed the job results, as well as a done function.
  // The user should call the done function when they want the job check to be considered done
  // This will cause the endPoll function to run with whatever result the user specified in done()
  // done() is an arbitrary name and can be called anything,
  // but it should be a function that takes in one param, the result
  // e.g.
  // function handler(result, done) {
  // do stuff here...
  // like processing result and storing it in someResult
  // then use done() with the result you want to pass back.
  // done(someResult)
  // }
  const {timeOut=(60 * 10), timeInterval=(1.5)} = options
  const stopTime = Date.now() + (timeOut * 1000)
  let pollInterval

  return new Promise((resolve, reject) => {
    pollInterval = setInterval(async () => {
      console.log('running the polling for jid', jid)
      const jobResult = await getSaltJobResults(jid)
      handler(jobResult, (error, result) => endPoll(error, result, resolve, reject))

      if (Date.now() > stopTime) {
        reject('Job failed to complete in time. If this is a longer job, please specify a different timeout duration')
        clearInterval(pollInterval)
      }
    }, (timeInterval * 1000))
  })

  function endPoll(error, result, resolve, reject) {
    console.log('request to end poll')
    if (error) console.log('error', error)
    if (result) console.log('result', result)
    clearInterval(pollInterval)
    error ? reject(error) : resolve(result)
  }
}

function checkRetrievedDataForFailure(data) {
  let failed = false
  const results = data.return[0]

  _.forEach(results, (result, key) => {
    if (!result && key.match(/control|operator|database/i)) {failed = true}
  })

  return failed
}



async function authenticatedSaltRequest(url, data, options={}) {
  const token = await saltAuth()
  const method = options.method || (data ? 'post' : 'get')
  try {
    const result = (await axios.request({
      url,
      method,
      headers: {
        'X-Auth-Token': token
      },
      data
    })).data
    return result
  } catch(err) {
    console.log('Salt request failed:', err.message)
  }
}

async function saltAuth(authOptions={}) {
  const cachedToken = await cache.checkKey('salt-auth-token')
  // console.log('cachedtoken', cachedToken)
  if (cachedToken) return cachedToken

  const {username, password, eauth} = authOptions
  const result = (await axios.post(`${process.env.SALT_MASTER_URL}/login`, {
    username: process.env.SALT_USERNAME || username,
    password: process.env.SALT_PASSWORD || password,
    eauth: eauth || 'auto'
  })).data
  const perms = result.return[0].perms
  const token = result.return[0].token
  // console.log(token)
  // console.log(perms)
  if (_.isEmpty(perms)) {
    return null
  } else {
    if (process.env.REDIS) {
      redis.set('salt-auth-token', token, 'EX', 60 * 60 * 6) //This time is determined by what the cherryPy server currently allows. If you want to change this, change both of them.
    }
    
    return token
  }
}
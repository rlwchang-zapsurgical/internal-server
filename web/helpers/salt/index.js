let salt = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive' &&
      file !== 'README.md') {
      
    file = file.replace('.js', '')
    salt = {...salt, [file]: require(`./${file}`)}
  }
});

module.exports = salt
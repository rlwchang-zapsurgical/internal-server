const minion = require('./minion')
const response = require('../response')

exports.successResponse = successResponse
function successResponse(res, data, names, message) {
  // console.log('running salt success reponse')
  if (Array.isArray(data)) {
    response.successResponse(res, minion.formatDataset(data, names), message)
  } else {
    response.successResponse(res, minion.formatData(data.return[0]))
  }
}
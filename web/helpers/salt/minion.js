const _ = require('lodash');

exports.getPcType = getPcType
function getPcType(name) {
  if (name.toLowerCase().match(/broker|database|DB/i)) {
    return 'database'
  } else if (name.toLowerCase().match(/operator|console|OPS/i)) {
    return 'operator'
  } else if (name.toLowerCase().match(/system|control|CTRL/i)) {
    return 'control'
  } else {
    return null
  }
}

exports.formatData = formatData
function formatData(data, config={methods: []}) {
  // console.log('data', data)
  const systemMinions = _.pickBy(data, (value, key) => key.toLowerCase().includes('sys'))

  let result = {}
  _.forEach(systemMinions, (value, key) => {
    let id = key.match(/_\d+_/)[0] || 'no-id'
    id =  _.trim(id, '_')
    const type = getPcType(key)

    result[id] = result[id] || {}
    result[id][type] = value
  })

  return result
}

exports.formatDataset = formatDataset
function formatDataset(dataArray, names) {
  if (!names || names.length < 1) {console.warn('formatMinionMultipleData requires a name array equal to the length of the different kinds of data')}
  let result = {}
  // console.log('dataArray', dataArray)

  _.forEach(dataArray, (raw_data, index) => {
    const data = raw_data.return[0]
    const name = names[index]
    const systemMinions = _.pickBy(data, (value, key) => key.includes('sys'))
  
    _.forEach(systemMinions, (value, key) => {
      let id = key.match(/_\d+_/)[0] || 'no-id'
      id =  _.trim(id, '_')
      const type = getPcType(key)
  
      result[id] = result[id] || {}
      result[id][type] = result[id][type] || {}
      result[id][type][name] = value
    })
    
  })
  return result
}
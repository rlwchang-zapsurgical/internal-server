let utils = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive' &&
      file !== 'README.md') {
        const fileName = file.replace('.js', '')
     utils = {...utils, [fileName]: require(`./${fileName}`)}
  }
});

module.exports = utils
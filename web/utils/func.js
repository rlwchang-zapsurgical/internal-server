exports.checkArgs = checkArgs
function checkArgs(required=[], preferred=[]) {
  required.forEach((element, index) => {
    if (element === undefined) {
      console.error(`A required argument is missing! Check element ${index}`)
      throw new Error(`Missing required argument: Element ${index} of the 'required' Array.`)
    }

    if (element === null) {
      console.warn(`One of the required arguments is null. Are you sure you meant to have element ${index} be null?`)
    }
  });

  preferred.forEach((element, index) => {
    if (element === undefined) {
      console.log(`You are missing a preferred argument. Element ${index} is not required, but strongly preferred.`)
    }

    if (element === null) {
      console.log(`Element ${index} is null. Did you mean for that to happen? It is not required, but strongly preferred.`)
    }
  })
}
const _ = require('lodash')

exports.getGreaterVersion = getGreaterVersion
function getGreaterVersion(versionOne, versionTwo) {  
    if (versionOne == versionTwo) {return versionOne}
    if (!versionOne) { return versionTwo }
    if (!versionTwo) { return versionOne }
  
    const paddedVersionOne = padVersionNumber(versionOne)
    const paddedVersionTwo = padVersionNumber(versionTwo)

    return paddedVersionOne > paddedVersionTwo ? versionOne : versionTwo
  }
  
exports.sort = sort
function sort(collection=[], asc=false, sortFunc) {
    // sortFunc is a function to apply to each element to get the version numbers
    let result = sortFunc ?
        collection.sort((prev, curr) => getGreaterVersion(sortFunc(prev), sortFunc(curr)) == prev ? 1 : -1) :
        collection.sort((prev, curr) => getGreaterVersion(prev, curr) == prev ? 1 : -1)

    return asc ?
        result :
        _.reverse(result)
}

exports.padVersionNumber = padVersionNumber
function padVersionNumber(version, padding=5) {
  const parsedVersion = parseGreek(version)
    const versionArray = _.map(parsedVersion.split('.'), value => _.padStart(value, padding, '0'))
    return versionArray.join('.')
}

exports.parseGreek = parseGreek
function parseGreek(version) {
    let parsedVersion = version.replace(/[.-_]*alpha()[.-_]*/i, '.1.')
        parsedVersion = parsedVersion.replace(/[.-_]*delta()[.-_]*/i, '.2.')
        parsedVersion = parsedVersion.replace(/[.-_]*beta()[.-_]*/i, '.3.')
        return parsedVersion
}
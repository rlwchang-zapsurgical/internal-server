const _ = require('lodash')

exports.version = version
function version(versionString) {
    if (!versionString) {return null}
    const versionRegex = new RegExp('([0-9].){2,3}([0-9])+', 'gi');
    return _.get(versionString.match(versionRegex), '[0]', undefined)
}

exports.pcType = pcType
function pcType(pcString) {
  if (pcString.toLowerCase().match(/broker|database/)) {
    return 'database'
  } else if (pcString.toLowerCase().match(/operator|console/)) {
    return 'operator'
  } else if (pcString.toLowerCase().match(/system|control/)) {
    return 'control'
  } else {
    return null
  }
}

exports.softwareType = softwareType
function softwareType(softwareName) {
  if (softwareName.toLowerCase().match(/treatment|tds/i)) {
    return 'tds'
  } else if (softwareName.toLowerCase().match(/tps/i)) {
    return 'tps'
  } else if (softwareName.toLowerCase().match(/broker|sitemgmt/i)) {
    return 'broker'
  } else {
    return null
  }
}

exports.branch = branch
function branch(branchName) {
  const nameRegex = new RegExp('[a-zA-Z._-]+', 'gi');
  return _.get(branchName.match(branchName), '[0]', null)
}

exports.release = release
function release(branchName) {
  return branchName.toLowerCase().includes('release') ? 'release' : null
}

exports.severity = severity
function severity(severityString) {
  return severityString && _.get(severityString.match(/\d+/), '[0]', null)
}
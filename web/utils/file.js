const mime = require('mime')
const _ = require('lodash')

exports.isValidType = isValidType
function isValidType(file={}, options={}) {
  if(!options.acceptedMimes && !options.acceptedExts) {
    // console.log('Is valid type.')
    return true
  }
  const {acceptedMimes=[], acceptedExts=[]} = options
  const types = [...acceptedMimes, ...acceptedExts]
  if(types.length == 0) {return true}

  let fileMime = file.mime || mime.getType(file.name)
  let ext = file.ext || mime.getExtension(fileMime)

  return _.includes(types, fileMime) || _.includes(types, ext)

}
exports.formatFilesData = formatFilesData
function formatFilesData(files=[], options={}) {
  if (!Array.isArray(files)) { files = [files]}
  return _.map(files, file => formatFileData(file, options))
}

exports.formatFileData = formatFileData
function formatFileData(file={}, options={}) {
  if(typeof file == 'string') {file = {name: file}}

  let fileMime = file.mime || mime.getType(file.name)
  let ext = file.ext || mime.getExtension(fileMime)

  return {
    ...file,
    mime: fileMime,
    ext,
    bucket: file.bucket || options.bucket,
    prefix: file.prefix || options.prefix,
    s3ObjectName: file.prefix || options.prefix ? `${_.trim(file.prefix || options.prefix, '/')}/${_.trim(file.name, '/')}` : file.name,
    s3BucketName: file.bucket || options.bucket,
  }
}
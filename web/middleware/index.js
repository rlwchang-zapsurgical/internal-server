require("fs").readdirSync(__dirname).forEach(function (file) {
    if (file != 'index.js') {
        const middlewareName = file.replace('.js', '')
        exports[middlewareName] = require(`./${file.replace('.js', '')}`);
    }
});

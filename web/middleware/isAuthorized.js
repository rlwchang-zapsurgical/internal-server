const _ = require('lodash')
const response = require('../helpers/response')
module.exports = (authorizedRoles=[], options={}) => {
    return (req, res, next) => {
        let authorized = false
        
        // Extract roles from user info
        const {roles=[]} = req.user

        // Normalize the authorizedRoles
        if (typeof authorizedRoles == 'string')
            authorizedRoles = [authorizedRoles]

        // If user is considered a boss or an admin, and the resource is not admin restricted, give access
        if (_.includes(roles, 'boss') || (_.includes(roles, 'admin') && !options.adminRestricted)) {
           authorized = true
        } else {
            // Otherwise, go through each of the allowed roles and check if the user has them
            authorized = _.some(authorizedRoles, authorizedRole => _.includes(roles, authorizedRole))
        }
    
        // If the user is authorized, proceed.
        if (authorized) {return next()}

        // Otherwise, complain
       return response.unAuthorizedResponse(res)
    }
}
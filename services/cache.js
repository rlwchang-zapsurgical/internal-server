const mongoose = require('mongoose')
const client = require('../packages/redis')
const exec = mongoose.Query.prototype.exec

mongoose.Query.prototype.cache = function(options={}) {
    // Options can have the following values:
    // exp: number of seconds before expiring
    // key: which key to cache with (and delete when expired)

    const {key, exp} = options

    this.useCache = true
    this.cacheHashKey = key || 'default'
    this.cacheExp = exp

}

mongoose.Query.prototype.exec = async function() {
    const result = await exec.apply(this, arguments)
    
    // If there is no useCache flag, immediately return query promise.
    if (!this.useCache)
        return result
    
    // If there is a flag, return value if available, otherwise set cache.
    const key = JSON.stringify({
        ...this.getQuery(),
        collection: this.mongooseCollection.name
    })

    if (this.useCache) {
        const docs = JSON.parse(cacheValue)

        if (Array.isArray(docs))
            return docs.map(doc => new this.model(JSON.parse(doc)))
        else
            return new this.model(docs)
    }

    if (this.cacheExp)
        client.hset(this.cacheHashKey, key, JSON.stringify(result), 'EX', this.cacheExp)
    else
        client.hset(this.cacheHashKey, key, JSON.stringify(result))
    
}
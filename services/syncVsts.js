const fs = require('fs')
const CronJob = require('cron').CronJob;
const vstsConnection = require('../packages/vsts')
const {Build} = require('../models')

// ====================
// Builds
// ====================
const {formatVstsData, formatBuildData} = require('../web/helpers/vsts/serviceHook/format') // Bad practice. Files may need to be restructured at some point
const restful = require('../web/helpers/restful') // Bad practice. Files may need to be restructured at some point

// synchronize build information
async function synchronizeBuildInformation(projectName) {
  console.log(`Checking build synchronization for ${projectName}...`)
  projectName = projectName.toLowerCase()
  const buildApi = await vstsConnection.getBuildApi()
  const currentBuilds = await buildApi.getBuilds(projectName)
  const targetDefinition = {
    tps: 'TPS',
    sitemgmt: 'Broker Official Build',
    treatment: 'Treatment App and Delivery Installer'
  }

  // fs.writeFile('./builds', JSON.stringify(currentBuilds))
  currentBuilds.forEach(async build => {
    if (build.definition.name != targetDefinition[projectName]) {return}

    const foundBuild = await Build.findOne({"build.buildId": build.id})
    
    try {
      if (!foundBuild && build.result == 2) {
        // If the build does not already exist, and it was completed successfully, create an entry in the DB.
        const formattedBuild = formatBuildData({resource: build})
        console.log(`Identified a missing build for project: ${projectName.toUpperCase()}`)
        console.log(`Creating a new database entry for build ${build.id}...`)
        await Build.create(formattedBuild)
      }
    } catch (e) {
      console.log(`Failed to create new build entry: ${e.message}`);
    }
  })

}

if (process.env.NODE_ENV == 'production') {
  const syncBuildJob = new CronJob(
    '0 */15 * * * *', // sync every 30 minutes
    () => ['tps', 'treatment', 'sitemgmt'].forEach((project) => synchronizeBuildInformation(project)),
    null,
    true,
    'America/Los_Angeles'
    );
}

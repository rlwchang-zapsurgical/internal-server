const CronJob = require('cron').CronJob;
const {checkSprintItems} = require('../web/controllers/release/sprint')

if (process.env.NODE_ENV == 'production') {
  const job = new CronJob(
    '0 0 0 * * 1',
    () => ['tps', 'treatment'].forEach((project) => checkSprintItems(project, {sendNotification: true})),
    null,
    true,
    'America/Los_Angeles'
    );
}

  // checkSprintItems('tps', {sendNotification: true, test: false})
  // checkSprintItems('treatment', {sendNotification: true, test: false})

  // checkSprintItems('tps', {sendNotification: true, test: true})
  // checkSprintItems('treatment', {sendNotification: true, test: true})
  // checkSprintItems('tps', {sendNotification: true, test: true})
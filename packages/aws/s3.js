const AWS = require('aws-sdk');
const redis = require('../redis')
const _ = require('lodash')

const awsParams = {
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  apiVersion: 'latest',
};

const s3 = new AWS.S3(awsParams)
exports.native = s3

exports.getUrl = getUrl
exports.generateDownloadUrl = async (bucketName, prefix='', fileName) => getUrl(bucketName, prefix ? `${prefix}/${fileName}` : fileName) //Legacy support
async function getUrl(bucketName, key, options={}, additionalParams={}) {
  const params = {
    Bucket: bucketName,
    Key: key,
    Expires: 60 * 60 * 24 * 2,
    ...additionalParams
  }

  const {action='getObject'} = options

  try {
    let url
    if (process.env.REDIS && action == 'getObject') {
      url = await redis.get(`aws:s3:${JSON.stringify(params)}`)
      if (url) {return url}

      url = await s3.getSignedUrl(action, params)
      redis.set(`aws:s3:${JSON.stringify(params)}`, url, 'EX', (params.Expires/2))

    } else {
      url = await s3.getSignedUrl(action, params)
    }

    return url

  } catch (e) {
    console.log(e)
    throw e
  }
}

exports.listObjects = listObjects
exports.listS3Objects = listObjects //This is for legacy support
async function listObjects(bucketName, prefix=null) {
  const params = {
    Bucket: bucketName
  }

  if (prefix) params.Prefix = prefix;
  

  const data = await requestAllObjects(params);
  return data;
}

exports.deleteObjects = deleteObjects
exports.deleteS3Objects = deleteObjects // Legacy support
async function deleteObjects(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}
  const keys = _.map(files, file => {
    const key = (file.s3ObjectName) || (options.prefix ? `${options.prefix}/${file.name}` : file.name)
    return {Key: key}
  })

  try {
    const result = await s3.deleteObjects({
      Bucket: files[0].bucket || options.bucket,
      Delete: {
        Objects: keys
      }
    }).promise()
    return result
  } catch(e) {
    console.log(e)
    throw e
  }
}

exports.deleteObject = deleteObject
exports.deleteS3Object = deleteObject // Legacy support
async function deleteObject (file, options={}) {
  deleteObjects(file, options)
 }

 // ====================
 // HELPER FUNCTIONS
 // ====================
 async function requestAllObjects (options, continuationToken, objects=[]) {
  const requestOptions = continuationToken ? {...options, ContinuationToken: continuationToken} : options

  try {
    const data = await s3.listObjectsV2(requestOptions).promise()
    // console.log('data', data)
    if (data["IsTruncated"] && data["NextContinuationToken"]) {
      return await requestAllObjects(options, data["NextContinuationToken"], [...objects, ...data["Contents"]])
    } else {
      return [...objects, ...data["Contents"]]
    }

  } catch(err) {
    console.log(`An error occured with fetching objects: ${err}`)
    return objects;
  }
}
const AWS = require('aws-sdk');
const redis = require('../redis')
const _ = require('lodash')

////////////////////////
// GENERAL AWS CONFIG //
////////////////////////

/////////////////////////////////////////////////////////////////////////
// Import keys from a config/keys.js file,                             //
// otherwise use the ones in environment variables (ie for deployment) //
/////////////////////////////////////////////////////////////////////////

const {AWS_ACCESS_KEY, AWS_SECRET_KEY} = process.env

// Use latest API and promise features for the AWS SDK.
AWS.config.setPromisesDependency(null);

// Create a reloadable params object with keys
const awsParams = {
  accessKeyId: AWS_ACCESS_KEY,
  secretAccessKey: AWS_SECRET_KEY,
  apiVersion: 'latest',
  // region: 'us-west-1',
  // signatureVersion: 'v4'
};

module.exports = {
  s3: require('./s3')
}
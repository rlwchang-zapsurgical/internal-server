const nodemailer = require('nodemailer')
const stripHtml = require('string-strip-html')
const Bottleneck = require('bottleneck')

const limiter = new Bottleneck({
  minTime: 1000,
  maxConcurrent: 1,
})

const transporter = nodemailer.createTransport({
  host: 'smtp.office365.com',
  port: 587,
  // secureConnection: false,
  secure: false, // true for 465, false for other ports
  // tls: {
  //   ciphers:'SSLv3'
  // },
  auth: {
    user: process.env.EMAIL_USER, // generated ethereal user
    pass: process.env.EMAIL_PASSWORD // generated ethereal password
  }
});

transporter.verify().then(
  success => console.log('A new SMTP connection has been established and is ready to receive messages.')
).catch(
  error => console.log('There was an error establishing a connection.', error)
)

// const transporter = nodemailer.createTransport("SMTP", {
//   service: "hotmail",
//   auth: {
//     user: process.env.EMAIL_USER, // generated ethereal user
//     pass: process.env.EMAIL_PASSWORD // generated ethereal password
//   }
// })
exports.mailer = transporter
  


// setup email data with unicode symbols
exports.sendMail = limiter.wrap(sendMail)
function sendMail(to, subject, content) {
// function sendMail(to, subject, content, cb=() => {}, eb=() => {}) {
  if (content.addenum) {
    content = content.message + '\n\n' + content.addenum
  } else {
    content = content.message || content
  }

  const mailOptions = {
      from: '"Zap Bot" <ZapBot@zapsurgical.com>', // sender address
      to: Array.isArray(to) ? to.join(', ') : to, // list of receivers
      subject: subject, // Subject line
      text: stripHtml(content), // plain text body
      html: content // html body
  };

  console.log(`Sending mail to ${mailOptions.to}`)

  // console.log('mailoptions', mailOptions.to)
  
  // return transporter.sendMail(mailOptions, (error, info) => {
  //     if (error) {
  //         eb(error);
  //     } else {
  //       console.log(`Successfully sent a message (id: ${info.messageId }) to ${mailOptions.to}: `)
  //       console.log(`The message received a response of ${info.response}`)
  //       cb(info)
  //     }
  // });

  return transporter.sendMail(mailOptions)
}


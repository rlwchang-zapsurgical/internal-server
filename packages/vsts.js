const azdev = require('azure-devops-node-api')

const collectionUrl = "https://dev.azure.com/zapsurgical"

const token = process.env.VSTS_SECRET_TOKEN
const authHandler = azdev.getPersonalAccessTokenHandler(token)
const connection = new azdev.WebApi(collectionUrl, authHandler)

module.exports = connection
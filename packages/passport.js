const passport = require('passport');
const util = require('util')

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

const {User} = require('../models');

// const nativePassportAuthenticate = passport.authenticate

passport.authenticatePromise = (strategy, options) => {
  return (req, res, next) => {
    return new Promise((resolve, reject) => {
      passport.authenticate(strategy, (err, user , info) => {
        if (err) {
          return reject(err)
        }

        if (!user) {
          return reject(info)
        }

        if (user) {
          return resolve(user)
        }
      })(req, res, next)
    })
  }
}

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: process.env.JWT_SECRET
}

const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  User.findById(payload.sub, (err, user) => {
    return done(err || null, user || Boolean(user))
  })
})

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
passport.use(new LocalStrategy(User.authenticate()));
passport.use(jwtLogin);


module.exports = passport
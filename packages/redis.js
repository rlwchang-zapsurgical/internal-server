if (process.env.REDIS) {
    const redis = require('redis')
    const util = require('util')
    
    const REDIS_URL = process.env.REDIS_URL || 'redis://127.0.0.1:6379'
    const client = redis.createClient(REDIS_URL)
    
    client.on('ready', () => console.log(`Successfully connected to Redis server at ${REDIS_URL}`))
    client.on('reconnecting', (delay, attempt) => console.log(`Redis server was disconnected. Attempting to reconnect. Attempts number: ${attempt}`))
    client.on('error', err => console.log(`Failed to connect to Redis server at ${REDIS_URL} : ${err}`))
    client.on('end', () => console.log('Connection with Redis server terminated.'))
    
    // client.get = (key, cb) => client.get(key, (err, value) => cb(JSON.parse(value)))
    client.get = util.promisify(client.get)
    client.hget = util.promisify(client.hget)
    client.hgetall = util.promisify(client.hgetall)
    
    // client.set = (key, value, cb) => client.set(key, JSON.stringify(value), cb)
    client.set = util.promisify(client.set)
    client.setnx = util.promisify(client.setnx)
    client.hset = util.promisify(client.hset)
    // client.hsetnx = util.promisify(client.hsetnx)
    client.hkeys = util.promisify(client.hkeys)
    
    client.del = util.promisify(client.del)
    client.hdel = util.promisify(client.hdel)

    module.exports = client
}
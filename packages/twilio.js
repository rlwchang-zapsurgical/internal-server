const twilio = require('twilio');
///////////////////
// TWILIO CONFIG //
///////////////////

const {TWILIO_ACCESS_KEY, TWILIO_SECRET_KEY, TWILIO_PHONE_NUMBER} = process.env
const client = new twilio(TWILIO_ACCESS_KEY, TWILIO_SECRET_KEY);

const sendMessage = (message, to, from='+14157921388') => {
  client.messages.create({
      body: message,
      to,  // Text this number
      from // From a valid Twilio number
  })
  .then((message) => console.log(message.sid))
  .catch(err => console.log(err))
}

module.exports = {
  sendMessage
}

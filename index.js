require('./init')

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const server = require('http').Server(app); //Create an HTTP server for socket.io using Express
const passport = require('passport');

const cors = require('cors');
const mime = require('mime')

// Monkey patch to use the new version of express
express.static.mime.charsets = express.static.mime.charsets || {}
express.static.mime.charsets.lookup=mime.getType



const pluralize = require('pluralize')

// process.env.MONGODB_URI = 'mongodb://richard:Zapsurgical@ds133271.mlab.com:33271/zap-surgical-dev'

const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://richard:Zapsurgical@ds245337.mlab.com:45337/zapenterprise-internal';
mongoose.connect(MONGODB_URI)
  .then()
  .catch(err => console.log(`Failed to connect to the mongoDB database: ${err}`));

// app.use(cors(corsOptions))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// app.use(express.static());

// ============
// AUTH SECTION
// ============
// const speakeasy = require('speakeasy');
// const secret = speakeasy.generateSecret({length: 30})
// const secretToken = speakeasy.totp({secret: secret.base32, ecnoding: 'base32'})
// console.log(secret);
// console.log(secretToken);
// const SESSION_SECRET = process.env.SESSION_SECRET || 'this is the secret for local dev';
//
// app.use(session({
//   maxAge: 1000 * 60 * 60 * 24 * 7,
//   keys: [SESSION_SECRET]
// }));
// app.use(session({
//   secret: SESSION_SECRET,
//   resave: false,
//   saveUninitialized: false
// }))




// ===============
// ROUTES SECTION
// ===============
// Copied from https://github.com/expressjs/express/issues/3308
// List out all of the routes.
app.get('/', (req, res) => {
  const routes = []
  // console.log()
app._router.stack.forEach(route => {
    print([], route)
  })

  res.json(routes)

  function print (path, layer) {
    if (layer.route) {
      layer.route.stack.forEach(print.bind(null, path.concat(split(layer.route.path))))
    } else if (layer.name === 'router' && layer.handle.stack) {
      layer.handle.stack.forEach(print.bind(null, path.concat(split(layer.regexp))))
    } else if (layer.method) {
      routes.push({
        method: layer.method.toUpperCase(),
        path: `/${path.concat(split(layer.regexp)).filter(Boolean).join('/')}`
      })
    }
  }
  
  function split (thing) {
    if (typeof thing === 'string') {
      return thing.split('/')
    } else if (thing.fast_slash) {
      return ''
    } else {
      var match = thing.toString()
        .replace('\\/?', '')
        .replace('(?=\\/|$)', '$')
        .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
      return match
        ? match[1].replace(/\\(.)/g, '$1').split('/')
        : '<complex:' + thing.toString() + '>'
    }
  }
})

require('./web/controllers')(app);
// ====================
// SERVICES
// ====================
require('./services')
app.use(passport.initialize());
app.use(passport.session());
// ============
// MISC SECTION
// ============

// ===========
// WEB SOCKET
// ===========
app.locals.connections = {}; //Create a global variable to keep track of all active connections
// console.log(app.locals);
// require('./services/socketIO')(io, app.locals.connections);


////////////////
// PLAYGROUND //
////////////////
require('./playground')

const PORT = process.env.PORT || 3000
server.listen(PORT, err => console.log(`Server has started on ${PORT}`, err || "\nNo errors detected"));

// ====================
// PLAYGROUND
// ====================
const redis = require('./packages/redis')

// redis.hset('test', 'yummy-pkg', JSON.stringify({'value': 'value', expire: Date.now() + 10000}))

// redis.hgetall("test", (error, data) => {
//   console.log('error', error)
//   console.log('data', data)
// }
// )
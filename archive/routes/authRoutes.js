const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


const passport = require('passport');


router.route('/login')
  .post(helpers.localAuthentification, helpers.isAuthenticated, helpers.isMfaAuthenticated, helpers.login)

router.route('/logout')
  .post(helpers.logout);

router.route('/jwt-auth')
  .get(middleware.isJwtAuthenticated, helpers.jwtLogin)

module.exports = router;

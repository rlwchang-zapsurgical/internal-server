const express = require('express');
const router = express.Router();

const helpers = require('../helpers');

const {
  getUploadLinksRes,
  addUploadsToDBRes,
  deleteUploadsRes
} = helpers



router.route('/:modelName/:id/link')
.post(getUploadLinksRes)

router.route('/:modelName/:id/register')
.post(addUploadsToDBRes)
.put(addUploadsToDBRes)

router.route('/:modelName/:id/delete')
.post(deleteUploadsRes)

router.route('/:modelName/:id/:property/link')
.post(getUploadLinksRes)

router.route('/:modelName/:id/:property/register')
.post(addUploadsToDBRes)
.put(addUploadsToDBRes)

router.route('/:modelName/:id/:property/delete')
.post(deleteUploadsRes)


module.exports = router;
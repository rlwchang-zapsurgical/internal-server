const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
// const middleware = require('../middleware')

router.route('/')
  .get(helpers.getInstructions)
  .post(helpers.createInstructions)

router.route('/:id')
  .get(helpers.showInstructions)
  .put(helpers.editInstructions)
  .delete(helpers.removeInstructions)



module.exports = router;

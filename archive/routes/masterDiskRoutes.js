const express = require('express');
const router = express.Router();
const restful = require('../helpers/restful')
const helpers = require('../helpers/')

restful.addRestfulRoutes(router, {modelName: 'MasterDisk', include: ['create', 'update', 'destroy']})

router.route('/').get(helpers.masterDiskIndex)
router.route('/:id').get(helpers.masterDiskShow)

module.exports = router;

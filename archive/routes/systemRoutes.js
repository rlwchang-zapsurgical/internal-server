const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')

router.route('/')
  // .get(helpers.getLogs)
  .get(helpers.getSystems)
  .post(helpers.createSystem)

router.route('/ping')
  .get(helpers.pingSystems)
router.route('/:id/ping')
  .get(helpers.pingSystems)
// router.route('/login')
//   .get(helpers.loginSystems)

router.route('/sysinfo')
  .get(helpers.getSystemInfo)

router.route('/:id/sysinfo')
  .get(helpers.getSystemInfo)

router.route('/vitals')
  .get(helpers.getVitals)

router.route('/:id/vitals')
  .get(helpers.getVitals)

router.route('/packages')
  .get(helpers.getPackages)
  .post(helpers.modifyPackage)
router.route('/:id/packages')
  .get(helpers.getPackages)
  .post(helpers.modifyPackage)

router.route('/services')
  .get(helpers.getServices)
router.route('/:id/services')
  .get(helpers.getServices)
router.route('/:id/:pcType/services/:action')
  .post(helpers.modifyService)

router.route('/logs')
  .get(helpers.getLogs)
router.route('/:id/logs')
  .get(helpers.getLogs)
router.route('/logs/:filename')
  .get(helpers.showLogs)
router.route('/logs/refresh')
  .post(helpers.pushLogs)
router.route('/:id/logs/refresh')
  .post(helpers.pushLogs)
router.route('/:id/logs/:filename')
  .get(helpers.showLogs)



router.route('/:id')
  .get(helpers.showSystem)
  .put(helpers.editSystem)
  .delete()


module.exports = router;

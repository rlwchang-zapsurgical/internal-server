const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


// router.route('/:id/software/:softwareType')
//   .get((req, res) => helpers.getSoftwareParams(req, res, req.params.id, req.params.softwareType))
//
// router.route('/:id/software/:softwareType/:dataType')
//   .get((req, res) => helpers.getSoftwareData(req, res, req.params.id, req.params.softwareType, req.params.dataType))

router.route('/')
    .get(helpers.getSoftware)
    .post(helpers.addSoftware)

router.route('/:id')
    .get(helpers.showSoftware)
    .put(helpers.editSoftware)
    .delete(helpers.removeSoftware)

router.route('/:id/upload')
    .post(helpers.getSoftwareDocumentsUploadLinks)
    .put(helpers.addSoftwareDocuments)

// router.route('/:id/instructions')
//     .put(helpers.addSoftwareDocuments)

module.exports = router;

const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')

const passport = require('passport');

router.route('/')
  .get(middleware.isJwtAuthenticated, middleware.isAuthorized('admin'), helpers.getUsers)


router.route('/:id')
  .get(middleware.isJwtAuthenticated, middleware.isAuthorized('admin'), helpers.showUser)
  .put(middleware.isJwtAuthenticated, middleware.isAuthorized('admin'), helpers.editUser)
  .delete(middleware.isJwtAuthenticated, middleware.isAuthorized('admin'), helpers.removeUser)

router.route('/register')
  .post(middleware.isJwtAuthenticated, middleware.isAuthorized('admin'), helpers.register)


  

module.exports = router;
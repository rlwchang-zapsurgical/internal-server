const express = require('express');
const router = express.Router();
const restful = require('../helpers/restful')
const helpers = require('../helpers/')

restful.addRestfulRoutes(router, {modelName: 'Group'})

module.exports = router;

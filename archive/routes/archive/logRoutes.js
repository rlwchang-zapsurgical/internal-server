const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


// router.route('/test/logtestpush')
// .get(() => {
//   const moment = require('moment');
//   console.log("route logtestpush accessed");
//   const now = moment("08:00", "hh:mm");
//   const currentHour = Number(now.format('HH')); //24 Hour scheme
//   const minutes = now.format('mm') //Either 00, 15, or 30 to account for x:30 and x:45 timezones
//   const upperTimeZoneHr = (currentHour >= 10) ? 24 - currentHour : null
//   const lowerTimeZoneHr = (currentHour <= 12) ? currentHour * -1 : null
//   const upperTimeZone = upperTimeZoneHr && (moment(upperTimeZoneHr, "HH").format("HH") + ":" + minutes)
//   const lowerTimeZone = lowerTimeZoneHr && (moment(lowerTimeZoneHr * -1, "HH").format("HH") + ":" + minutes)
//
//   // console.log(`currentHour: ${currentHour}`);
//   // console.log(currentHour == 0);
//   // console.log(`upperTimeZone: ${upperTimeZone}`);
//   // console.log(`lowerTimeZone: ${lowerTimeZone}`);
//
//   if (currentHour == 0) {
//     const targetDate = now.format("YYYY-MM-DD");
//     const pushLogsParams = {tgt: `timezone:*(UTC+00:00)*`, expr_form: "grain"}
//     helpers.pushLogs(undefined, targetDate, undefined, pushLogsParams);
//     pushLogsParams["tgt"] = `timezone:*(UTC)*`
//     helpers.pushLogs(undefined, targetDate, undefined, pushLogsParams);
//   }
//
//   if (upperTimeZone) {
//     const targetDate = now.add(1, 'days').format("YYYY-MM-DD");
//     const pushLogsParams = {tgt: `timezone:*(UTC+${upperTimeZone})*`, expr_form: "grain"}
//     helpers.pushLogs(undefined, targetDate, undefined, pushLogsParams)
//   }
//   if (lowerTimeZone && currentHour != 0) {
//     const targetDate = now.format("YYYY-MM-DD");
//     const pushLogsParams = {tgt: `timezone:*(UTC-${lowerTimeZone})*`, expr_form: "grain"}
//     helpers.pushLogs(undefined, targetDate, undefined, pushLogsParams)
//   }
// })
router.route('/:systemId/:pcType/logs')
  // .get((req, res) => helpers.getLogs)
  .get(helpers.getLogs)
  // .post(helpers.createLog(req, res))

router.route('/:systemId/logs/refresh')
  .post(helpers.refreshLogs)

router.route('/:systemId/:pcType/logs/refresh')
  .post(helpers.refreshLogs)

router.route('/:systemId/:pcType/logs/:filename')
  .get(helpers.showLog)


module.exports = router;

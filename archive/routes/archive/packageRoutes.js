const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


router.route('/:systemId/packages')
  .get(helpers.getPackages)

router.route('/:systemId/:pcType/packages')
  .get(helpers.getPackages)
  .post((req, res) => helpers.modifyPackage(req, res))


module.exports = router;

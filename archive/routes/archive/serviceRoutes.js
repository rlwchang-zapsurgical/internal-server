const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


router.route('/:systemId/:pcType/services')
  // .get((req, res) => helpers.getLogs)
  .get(helpers.getServices)
  .post(helpers.modifyService)


module.exports = router;

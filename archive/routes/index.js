let routes = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file != 'index.js' &&
      file != 'archive') {
     routes = {
       ...routes,
       [file.replace('.js', '')]: require(`./${file}`)
      }
  }
});


module.exports = routes
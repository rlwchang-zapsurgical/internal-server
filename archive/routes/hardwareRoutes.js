const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')


router.route('/:id/hardware/:hardwareType')
  .get((req, res) => helpers.getHardwareParams(req, res, req.params.id, req.params.hardwareType))

  router.route('/:id/hardware/:hardwareType/:dataType')
    .get((req, res) => helpers.getHardwareData(req, res, req.params.id, req.params.hardwareType, req.params.dataType))


module.exports = router;

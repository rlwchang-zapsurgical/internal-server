const express = require('express');
const router = express.Router();

const helpers = require('../helpers');

router.route('/jobs/:id')
  .get((req, res) => helpers.getSaltJobResults(req.params.id, undefined, res))



module.exports = router;

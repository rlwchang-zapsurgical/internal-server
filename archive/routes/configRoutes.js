const express = require("express");
const router = express.Router();

const helpers = require("../helpers");
const middleware = require('../middleware')


router.route("/")
  .get(middleware.isJwtAuthenticated, middleware.isAuthorized(['eng', 'qa', 'prod']), helpers.getConfig)
  .post(helpers.addConfig);

// router.route("/docs/upload")
//   .post(helpers.uploadConfigDocs)

// router.route("/docs/:id/")
//   .get(helpers.listConfigDocs)
//   .post(helpers.addConfig)

// router.route("/docs/:id/")
//   .get(helpers.listConfigDocs)
//   .post(helpers.addConfig)
  
router.route("/:id")
  .get(middleware.isJwtAuthenticated, middleware.isAuthorized(['eng', 'qa', 'prod']), helpers.showConfig)
  .put(middleware.isJwtAuthenticated, middleware.isAuthorized(['eng', 'qa']), helpers.editConfig)
  .delete(middleware.isJwtAuthenticated, middleware.isAuthorized(['eng', 'qa']), helpers.removeConfig);

router.route("/:id/docs")
  .get(helpers.listConfigDocs)
  .delete(helpers.removeConfigDocs)

router.route("/:id/docs/upload")
  .post(helpers.getConfigDocsUploadLink)
  .put(helpers.addConfigDocsToDatabase)

module.exports = router;

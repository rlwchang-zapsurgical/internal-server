const express = require('express');
const router = express.Router();

const helpers = require('../helpers');
const middleware = require('../middleware')

// console.log(middleware);

router.route('/')
  .get(middleware.isJwtAuthenticated, middleware.isAuthorized('eng'), helpers.getReleases)
  .post(middleware.isJwtAuthenticated, middleware.isAuthorized('eng'),helpers.addRelease)
  
router.route('/hook')
  .post(helpers.hookRelease, helpers.addRelease)

  router.route('/:id')
    .put(middleware.isJwtAuthenticated, middleware.isAuthorized('eng'), helpers.editRelease)


router.route('/documents/generate')
  .get(helpers.getReleaseDocuments)

module.exports = router;


// {
//   subscriptionId: '09b44797-2423-47a0-a2ed-5d5b84156a7f',
//     notificationId: 67,
//       id: 'da1976a7-8cba-4b0c-b00c-2b6a08849fb3',
//         eventType: 'build.complete',
//           publisherId: 'tfs',
//             message:
//   {
//     text: 'Build 20180610.6 succeeded',
//       html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464">20180610.6</a> succeeded',
//         markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464) succeeded'
//   },
//   detailedMessage:
//   {
//     text: 'Build 20180610.6 succeeded',
//       html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464">20180610.6</a> succeeded',
//         markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464) succeeded'
//   },
//   resource:
//   {
//     _links:
//     {
//       self: [Object],
//         web: [Object],
//           sourceVersionDisplayUri: [Object],
//             timeline: [Object]
//     },
//     properties: { },
//     tags: [],
//       validationResults: [],
//         plans: [[Object]],
//           triggerInfo: { },
//     id: 4464,
//       buildNumber: '20180610.6',
//         status: 'completed',
//           result: 'succeeded',
//             queueTime: '2018-06-11T02:04:02.33863Z',
//               startTime: '2018-06-11T02:04:39.7147951Z',
//                 finishTime: '2018-06-11T02:05:29.4135955Z',
//                   url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Builds/4464',
//                     definition:
//     {
//       drafts: [],
//         id: 42,
//           name: 'Treatment-.NET Desktop-PendantApp',
//             url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Definitions/42?revision=7',
//               uri: 'vstfs:///Build/Definition/42',
//                 path: '\\',
//                   type: 'build',
//                     queueStatus: 'enabled',
//                       revision: 7,
//                         project: [Object]
//     },
//     buildNumberRevision: 6,
//       project:
//     {
//       id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         name: 'Treatment',
//           description: 'Treatment delivery system',
//             url: 'https://zapsurgical.visualstudio.com/_apis/projects/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//               state: 'wellFormed',
//                 revision: 63110880,
//                   visibility: 'private'
//     },
//     uri: 'vstfs:///Build/Build/4464',
//       sourceBranch: 'refs/heads/develop',
//         sourceVersion: '2e03961ee29ab6df5c5489664f2ae8498c190d4b',
//           queue:
//     {
//       id: 1,
//         name: 'Default',
//           url: 'https://zapsurgical.visualstudio.com/_apis/build/Queues/1',
//             pool: [Object]
//     },
//     priority: 'normal',
//       reason: 'individualCI',
//         requestedFor:
//     {
//       displayName: 'Ben Chen',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/8fad799a-5598-63f6-9cde-fff4cc53806c',
//           _links: [Object],
//             id: '8fad799a-5598-63f6-9cde-fff4cc53806c',
//               uniqueName: 'Ben@zapsurgical.com',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=8fad799a-5598-63f6-9cde-fff4cc53806c',
//                   descriptor: 'aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj'
//     },
//     requestedBy:
//     {
//       displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//           _links: [Object],
//             id: '00000002-0000-8888-8000-000000000000',
//               uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//                   descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA'
//     },
//     lastChangedDate: '2018-06-11T02:05:29.777Z',
//       lastChangedBy:
//     {
//       displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//           _links: [Object],
//             id: '00000002-0000-8888-8000-000000000000',
//               uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//                   descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA'
//     },
//     orchestrationPlan: { planId: '479fbd3f-360c-4037-8a10-d1a53abacecb' },
//     logs:
//     {
//       id: 0,
//         type: 'Container',
//           url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/builds/4464/logs'
//     },
//     repository:
//     {
//       id: '1b1ccb62-d3cf-4683-a499-55c317ab08c6',
//         type: 'TfsGit',
//           name: 'Treatment',
//             url: 'https://zapsurgical.visualstudio.com/Treatment/_git/Treatment',
//               clean: null,
//                 checkoutSubmodules: false
//     },
//     keepForever: false,
//       retainedByRelease: false,
//         triggeredByBuild: null
//   },
//   resourceVersion: '2.0',
//     resourceContainers:
//   {
//     collection:
//     {
//       id: 'de1f328d-b12a-4368-afec-1208abbcce65',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     },
//     account:
//     {
//       id: '62e5a3ed-0139-4177-a9d0-6af10375baee',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     },
//     project:
//     {
//       id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     }
//   },
//   createdDate: '2018-06-11T02:05:31.2479373Z'
// }
// Treatment
// Treatment -.NET Desktop - PendantApp
// refs / heads / develop
// develop
// {
//   subscriptionId: '09b44797-2423-47a0-a2ed-5d5b84156a7f',
//     notificationId: 68,
//       id: 'd0098f78-0795-42dc-8461-54527dfce490',
//         eventType: 'build.complete',
//           publisherId: 'tfs',
//             message:
//   {
//     text: 'Build 20180610.6 succeeded',
//       html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465">20180610.6</a> succeeded',
//         markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465) succeeded'
//   },
//   detailedMessage:
//   {
//     text: 'Build 20180610.6 succeeded',
//       html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465">20180610.6</a> succeeded',
//         markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465) succeeded'
//   },
//   resource:
//   {
//     _links:
//     {
//       self: [Object],
//         web: [Object],
//           sourceVersionDisplayUri: [Object],
//             timeline: [Object]
//     },
//     properties: { },
//     tags: [],
//       validationResults: [],
//         plans: [[Object]],
//           triggerInfo: { },
//     id: 4465,
//       buildNumber: '20180610.6',
//         status: 'completed',
//           result: 'succeeded',
//             queueTime: '2018-06-11T02:04:02.4948914Z',
//               startTime: '2018-06-11T02:04:53.5534731Z',
//                 finishTime: '2018-06-11T02:05:30.1834767Z',
//                   url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Builds/4465',
//                     definition:
//     {
//       drafts: [],
//         id: 56,
//           name: 'Treatment-.NET Desktop-PendantApp-RichardsPlayground',
//             url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Definitions/56?revision=1',
//               uri: 'vstfs:///Build/Definition/56',
//                 path: '\\',
//                   type: 'build',
//                     queueStatus: 'enabled',
//                       revision: 1,
//                         project: [Object]
//     },
//     buildNumberRevision: 6,
//       project:
//     {
//       id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         name: 'Treatment',
//           description: 'Treatment delivery system',
//             url: 'https://zapsurgical.visualstudio.com/_apis/projects/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//               state: 'wellFormed',
//                 revision: 63110880,
//                   visibility: 'private'
//     },
//     uri: 'vstfs:///Build/Build/4465',
//       sourceBranch: 'refs/heads/develop',
//         sourceVersion: '2e03961ee29ab6df5c5489664f2ae8498c190d4b',
//           queue:
//     {
//       id: 1,
//         name: 'Default',
//           url: 'https://zapsurgical.visualstudio.com/_apis/build/Queues/1',
//             pool: [Object]
//     },
//     priority: 'normal',
//       reason: 'individualCI',
//         requestedFor:
//     {
//       displayName: 'Ben Chen',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/8fad799a-5598-63f6-9cde-fff4cc53806c',
//           _links: [Object],
//             id: '8fad799a-5598-63f6-9cde-fff4cc53806c',
//               uniqueName: 'Ben@zapsurgical.com',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=8fad799a-5598-63f6-9cde-fff4cc53806c',
//                   descriptor: 'aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj'
//     },
//     requestedBy:
//     {
//       displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//           _links: [Object],
//             id: '00000002-0000-8888-8000-000000000000',
//               uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//                   descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA'
//     },
//     lastChangedDate: '2018-06-11T02:05:30.543Z',
//       lastChangedBy:
//     {
//       displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//           _links: [Object],
//             id: '00000002-0000-8888-8000-000000000000',
//               uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//                 imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//                   descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA'
//     },
//     orchestrationPlan: { planId: '569d44c1-3af4-4335-ac13-1364a224199a' },
//     logs:
//     {
//       id: 0,
//         type: 'Container',
//           url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/builds/4465/logs'
//     },
//     repository:
//     {
//       id: '1b1ccb62-d3cf-4683-a499-55c317ab08c6',
//         type: 'TfsGit',
//           name: 'Treatment',
//             url: 'https://zapsurgical.visualstudio.com/Treatment/_git/Treatment',
//               clean: null,
//                 checkoutSubmodules: false
//     },
//     keepForever: false,
//       retainedByRelease: false,
//         triggeredByBuild: null
//   },
//   resourceVersion: '2.0',
//     resourceContainers:
//   {
//     collection:
//     {
//       id: 'de1f328d-b12a-4368-afec-1208abbcce65',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     },
//     account:
//     {
//       id: '62e5a3ed-0139-4177-a9d0-6af10375baee',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     },
//     project:
//     {
//       id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         baseUrl: 'https://zapsurgical.visualstudio.com/'
//     }
//   },
//   createdDate: '2018-06-11T02:05:31.372934Z'
// }
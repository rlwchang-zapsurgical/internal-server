const passport = require('passport');
// Standalone middleware function to verify authenticated requests
module.exports = (req, res, next) => {
  if (process.env.NODE_ENV == "development") {
    req.user = {
      "roles": ['admin'],
      "permissions": [],
      "hashingAlg": "SHA-256",
      "_id": "5ae746d771c809727020e6ec",
      "username": "development",
      "email": "development@zapsurgical.com",
      "number": "11111111111111111111",
      "authLevel": -2,
      "fingerprint": "1e1f51894bf2f9ea89c244804d8554b9",
      "secret": "PI2SY3LZMQ2DGNZ2OBBHSMJOGA3DO52UEE6EOQ2MI5NCC3DR",
      "firstName": "Testing",
      "lastName": "Developer"
    };
    return next();
  }
  else {
    return passport.authenticate("jwt", { session: false })(req, res, next);
  }
};
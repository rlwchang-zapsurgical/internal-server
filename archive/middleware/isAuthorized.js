const _ = require('lodash')

module.exports = (authorizedRoles=[], options={}) => {
    return (req, res, next) => {
        let authorized = false
        
        // Extract roles from user info
        const {roles=[]} = req.user

        // Normalize the authorizedRoles
        if (typeof authorizedRoles == 'string')
            authorizedRoles = [authorizedRoles]

        // If user is considered a boss or an admin, and the resource is not admin restricted, give access
        if (_.includes(roles, 'boss') || (_.includes(roles, 'admin') && !options.adminRestricted)) {
           authorized = true
        } else {
            // Otherwise, go through each of the allowed roles and check if the user has them
            authorizedRoles.forEach(role => {
                if (_.includes(roles, role)) {
                    authorized = true;
                }
            })
            // console.log('authorized', authorized)
        }
    
    
        // If the user is authorized, proceed.
        if (authorized) return next()

        // Otherwise, complain
        res.status = 401
        res.json({
            status: 'error',
            message: 'You are not authorized to access this resource.'
        })
    }
}
const mongoose = require('mongoose');

const hardwareSchema = new mongoose.Schema({
  package: String,
  TwinCATSYS: String,
  TwinCATSOFT: String,
  AcsSYS: String,
  AcsSoft: String,
  GunDriver: String,
  DosimeterPrimary: String,
  DosimeterSecondary: String,
  Afc: String,
  MvBoard: String,
})

const Hardware = mongoose.model('Hardware', hardwareSchema)

module.exports = {
  hardwareSchema,
  Hardware
}

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const instructionsSchema = new Schema({
    name: String,
    type: String,
    url: String,
    description: String
})

const Instruction = mongoose.model('Instruction', instructionsSchema)

module.exports = Instruction
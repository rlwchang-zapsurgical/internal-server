const mongoose = require('mongoose');

const releaseSchema = new mongoose.Schema({
  name: {type: String},
  version: {type: String},
  description: String,
  type: [String],
  downloadUrl: String,
  instructionsUrl: String,
  notes: [String],
  dateUploaded: Date,
  build: Object,
  // {
  //   buildId: String,
  //   buildStart: Date,
  //   buildEnd Date,
  //   buildDuration: Number,
  //   buildNumber: String,
  //   branchName: String,
  //   fullBranchName: String,
  //   commitHash: String,
  //   definition: String,
  //   author: String
  // },
  projectName: String,
  qaApproved: Boolean,
  qaApprovalRequested: {type: Boolean, default: false},
  rejected: Boolean,
  testingStatus: String
});

const Release = mongoose.model("Release", releaseSchema);


module.exports = Release;

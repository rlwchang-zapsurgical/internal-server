const mongoose = require('mongoose');
const Schema = mongoose.Schema
const uuid = require('uuid/v4')

var licenseSchema = new Schema({
    codes: [new Schema({
    dateIssued: {type: Date, default: Date.now},
    uuid: {type: String, default: uuid}, // A _id will automatically be assigned as well, which can be used in conjunction
    used: {type: Boolean, default: false}
  })]
}, { _id: false, minimize: false, toJSON: {virtuals: true} })


licenseSchema.virtual('remaining').get(
  function() {
    let remaining = 0
    this.codes.forEach(code => {
      if (!code.used) {remaining++}
    })
    return remaining
  }
)

module.exports = licenseSchema
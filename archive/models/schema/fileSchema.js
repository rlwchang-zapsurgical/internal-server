const mongoose = require('mongoose')

const createSchema = (params={}, options={}) => {
    return new mongoose.Schema({
    uuid: String, // This is more for client side use
    name: {type: String, required: true},
    version: String,
    s3ObjectName: String,
    s3BucketName: String,
    notes: String,
    requirements: Array,
    ext: String,
    mime: String,
    ...params
  }, {minimize: false, _id: false, ...options})
}

module.exports = createSchema()
module.exports.createSchema = createSchema
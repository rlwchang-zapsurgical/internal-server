const mongoose = require('mongoose');
const Schema = mongoose.Schema

var documentSchema = new Schema({
  name: String,
  prefix: String, //S3 prefix if any
  bucket: String, //S3 bucket if being stored there
  // type: String,
  mime: String,
  ext: String,
  description: String
}, { _id: false })

module.exports = documentSchema
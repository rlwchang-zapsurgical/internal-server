const mongoose = require('mongoose')
const documentSchema = require('./documentSchema')

const createSchema = (params={}, options={}) => {
  // if (!(arguments.length > 0)) {
    //   return
    // }
    return new mongoose.Schema({
    uuid: String, // This is more for client side use
    name: String,
    version: String,
    s3ObjectName: String,
    s3BucketName: String,
    instructions: [documentSchema],
    documents: [documentSchema],
    notes: String,
    requirements: Array,
    ...params
  }, {minimize: false, _id: false, ...options})
}

module.exports = createSchema()
module.exports.createSchema = createSchema
const mongoose = require('mongoose');
const licenseSchema = require('./schema/licenseSchema')

const systemSchema = new mongoose.Schema({
  // status: "active",
  systemId: String,
  date: {type: Date, default: new Date()},
  name: String,
  owner: String,
  city: String,
  state: String,
  country: String,
  lat: Number, //Latitude
  lon: Number, //Longitude
  treatments: licenseSchema,
  masterDiskControl:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'MasterDisk'
  },
  masterDiskOperator:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MasterDisk'
  },
  masterDiskDatabase:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MasterDisk'
  },
  notes: [String]
}, {minimize: false})

module.exports = mongoose.model('System', systemSchema);

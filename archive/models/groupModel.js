const mongoose = require('mongoose');
const userSchema = require('./schema/userSchema')


const groupSchema = mongoose.Schema({
  name: String,
  managers: {type: [userSchema], default: []},
  members: {type: [userSchema], default: []}
}, {minimize: false})


const Group = mongoose.model('Group', groupSchema);

module.exports = Group
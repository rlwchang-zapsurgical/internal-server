const mongoose = require('mongoose');
const fileSchema = require('./schema/fileSchema')
const softwareSchema = require('./schema/softwareSchema')
const documentSchema = require('./schema/documentSchema')
const _ = require('lodash')

const masterDiskSchema = new mongoose.Schema({
  name: String,
  version: String,
  type: {type: String, required: true}, //this will be 'operator', 'control', or 'database'
  // file: {type: fileSchema, default: {}},
  file: {type: fileSchema},
  software: [softwareSchema],
  documents: [fileSchema],
  s3ObjectName: String,
  s3BucketName: String
}, {
  minimize: false,
  toJSON: {virtuals: true, minimize: false},
  toObject: {virtuals: true, minimize: false}
});

masterDiskSchema.virtual('softwareObjects').get(function() {
  const result = {}
  _.forEach(this.software, (softwareItem) => {
   result[softwareItem.name] = softwareItem
  });

  return result
})

const MasterDisk = mongoose.model("MasterDisk", masterDiskSchema);


module.exports = MasterDisk;

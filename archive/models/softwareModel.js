const mongoose = require('mongoose');
const {createSchema} = require('./schema/softwareSchema')

const softwareSchema = createSchema({
  qaApproved: {type: Boolean, default: false},
  releaseTime: {type: Date, default: Date.now},
  s3ReleaseDocument: {type: String},
  masterDisk: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MasterDisk'
  },
  build: {type: Object, default: {}}
}, {
  _id: true,
  minimize: false
})

const Software = mongoose.model('Software', softwareSchema)

module.exports = Software
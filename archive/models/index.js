const _ = require('lodash')
const omissionList = [
  'index.js',
  'archive',
  'schema'
]

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (!_.includes(omissionList, file)) {
      // const modelName = _.startCase(file.replace('Model.js', ''))
      let modelName = file.replace('Model.js', '')
      modelName = modelName[0].toUpperCase() + modelName.substring(1)
      const startCaseName = _.startCase(modelName.toLowerCase())
      exports[modelName] = require(`./${file.replace('.js', '')}`);
      exports[startCaseName] = require(`./${file.replace('.js', '')}`);
  }
});

const mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
  systemId: {type: String, required: true},
  dateCreated: {type: Date, default: new Date()},
  locationUri: String,
  type: {type: String, default: "diagnostics"},
  coverage: {type: String}, //e.g. hardware, software, or complete
  metaData: String
})

const Log = mongoose.model('Log', logSchema);

module.exports = Log

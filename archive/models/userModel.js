const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = new mongoose.Schema({
  image: String,//base64 encoded data url
  firstName: String,
  lastName: String,
  username: String,
  password: String,
  email: String,
  number: String,
  allowedDevices: [String],
  mfaSecret: String,
  authLevel: {type :Number, default: 99},
  roles: [String], //eng, prod, qa, admin, or boss
  permissions: [String],
  hashingAlg: {type: String, default: 'SHA-256'}
});

userSchema.plugin(passportLocalMongoose);

const User = mongoose.model("User", userSchema);

module.exports = User;

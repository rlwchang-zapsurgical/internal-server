const models = require('../models')
const response = require('./response')
const _ = require('lodash')

function createRestfulMethods(modelName, options={}) {
  const Model = models[modelName]
  // console.log('models', models)
  // console.log('modelName', modelName)
  // console.log('Model', Model)
  const methods = {}

  methods[`${modelName}Index`] = async (req, res) => {
    try {
      const result = await Model.find()
     response.successResponse(res, result)
   } catch(err) {
    response.errorResponse(res, err)
   }
  }

  methods[`${modelName}Show`] = async (req, res) => {
    const {id} = req.params
    try {
      const result = await Model.findById(id)
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
    }
   }

   methods[`${modelName}Create`] = async (req, res) => {
    let {data} = req.body
    if (!data) {data = req.body}
    try {
      const result = await Model.create(data)
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
    }
   }

   methods[`${modelName}Update`] = async (req, res) => {
    const {id} = req.params
    let {data} = req.body
    if (!data) {data = req.body}
    try {
      const result = await Model.findByIdAndUpdate(id, data, {new: true})
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
    }
   }

   methods[`${modelName}Destroy`] = async (req, res) => {
    const {id} = req.params
    try {
      const result = await Model.findByIdAndRemove(id)
      response.successResponse(res, result)
    } catch(err) {
      response.errorResponse(res, err)
    }
   }

   return methods
}

function addRestfulRoutes(router, options={}) {
  if (typeof options == 'string') {options = {modelName: options}}
  let {model, modelName, Model, include=['index', 'create', 'show', 'update', 'destroy']} = options
  if (!modelName) {
    if (model) {modelName = model.name || model}
    else if (Model) {modelName = Model.name || Model}
  }
  const restfulMethods = createRestfulMethods(modelName, options)
  if (_.includes(include, 'index')) { router.route('/').get(restfulMethods[`${modelName}Index`]) }
  if (_.includes(include, 'create')) { router.route('/').post(restfulMethods[`${modelName}Create`]) }
  if (_.includes(include, 'show')) { router.route('/:id').get(restfulMethods[`${modelName}Show`]) }
  if (_.includes(include, 'update')) { router.route('/:id').put(restfulMethods[`${modelName}Update`]) }
  if (_.includes(include, 'destroy')) { router.route('/:id').delete(restfulMethods[`${modelName}Destroy`]) }
    

  
    
    
    
}

module.exports = {
  createRestfulMethods,
  addRestfulRoutes
}
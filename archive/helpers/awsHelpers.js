/*************************************************************
 * Contains a bunch of wrapper functions around the aws sdk. *
 *
 * @author Richard Chang <RichardC@zapsurgical.com>          *
 *************************************************************/
 const AWS = require('aws-sdk');
 const redis = require('../packages/redis')
 const _ = require('lodash')

////////////////////////
// GENERAL AWS CONFIG //
////////////////////////

/////////////////////////////////////////////////////////////////////////
// Import keys from a config/keys.js file,                             //
// otherwise use the ones in environment variables (ie for deployment) //
/////////////////////////////////////////////////////////////////////////

const {AWS_ACCESS_KEY, AWS_SECRET_KEY} = process.env

// Use latest API and promise features for the AWS SDK.
AWS.config.setPromisesDependency(null);

// Create a reloadable params object with keys
const awsParams = {
  accessKeyId: AWS_ACCESS_KEY,
  secretAccessKey: AWS_SECRET_KEY,
  apiVersion: 'latest',
  // region: 'us-west-1',
  // signatureVersion: 'v4'
  };

/////////////////////
////////////////// //
// S3 FUNCTIONS // //
////////////////// //
/////////////////////
const s3 = new AWS.S3(awsParams);

////////////////////
// Main functions //
////////////////////

/**
 * List all objects in a bucket by specifying the bucketName. Can limit by prefix.
 * @param  {String} bucketName    Name of the bucket that you want to list objects from.
 * @param  {String} [prefix=null] Limit the type of objects to just those with the corresponding prefix
 * @return {Promise}               Returns a promise that resolves with the data.
 */
async function listS3Objects(bucketName, prefix=null) {
  const params = {
    Bucket: bucketName
  }

  if (prefix) params.Prefix = prefix;

  const data = await requestAllObjects(params);
  return data;
}

async function getS3ObjectUrl(bucketName, key, options={}, additionalParams={}) {
  const params = {
    Bucket: bucketName,
    Key: key,
    Expires: 60 * 60 * 24 * 2,
    ...additionalParams
  }

  const {action='getObject'} = options

  // if there is a cached version, retrieve the cached url
  
  // otherwise get the url and then cache it
  // console.log(`Checking URL for ${key}`)

  try {
    let url
    
    if (process.env.REDIS && action == 'getObject') {
      try {
        url = await redis.hget('aws-s3', JSON.stringify(params))
          // console.log(`The first URL: ${url}`)
        
        if (url) {
          // console.log('Using old url');
          // console.log(`The second A URL: ${url}`)
          return url
        } else {
          // console.log(`The second B URL: ${url}`)
          // console.log(`The third URL: ${url}`)
          // console.log('Fetching a new url')
          url = await s3.getSignedUrl(action, params)
          redis.hset('aws-s3', JSON.stringify(params), url)
          redis.expire('aws-s3', (params.Expires / 2))
        }
      } catch (err) {
        console.log(err)
        url = await s3.getSignedUrl(action, params)
        return null
      }
    } else {
      url = await s3.getSignedUrl(action, params)
    }
    
    return url
  } catch(err) {
    // console.log('An error occured while trying to get a signed url.');
    return null;
  }
}

async function generateDownloadUrl(bucketName, prefix = '', fileName) {
  // console.log('generateDownloadUrl')
  // console.log(prefix);
  // console.log(fileName);
  let fullFileName
  if (prefix && !fileName) {fullFileName = prefix}
  else { fullFileName = prefix ? `${prefix}/${fileName}` : fileName }
  // console.log(fullFileName);
  try {
    const url = await getS3ObjectUrl(bucketName, fullFileName);
    // console.log(url);
    return url;
  } catch (err) {
    console.log(`${err} Failed to generate url for ${fullFileName}`);
    return null;
  }
}


//////////////////////
// Helper Functions //
//////////////////////

/**
 * Helper function for the listS3Objects function.
 * @param  {Object} options           List of parameters to pass to native S3 function.
 * @param  {String} continuationToken Auto-generated token used for recursion.
 * @param  {Array}  [objects=[]]      List of all retrieved objects.
 * @return {Array}                   Returns the list of all objects.
 */
async function requestAllObjects (options, continuationToken, objects=[]) {
  const requestOptions = continuationToken ? {...options, ContinuationToken: continuationToken} : options

  try {
    const data = await s3.listObjectsV2(requestOptions).promise()
    // console.log('data', data)
    if (data["IsTruncated"] && data["NextContinuationToken"]) {
      return await requestAllObjects(options, data["NextContinuationToken"], [...objects, ...data["Contents"]])
    } else {
      return [...objects, ...data["Contents"]]
    }

  } catch(err) {
    console.log(`An error occured with fetching objects: ${err}`)
    return objects;
  }
}

async function deleteS3Object (file, options={}) {
 deleteS3Objects(file, options)
}

async function deleteS3Objects(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}
  const keys = _.map(files, file => {
    const key = file.s3ObjectName || options.prefix ? `${options.prefix}/${file.name}` : file.name
    return {
      Key: key
    }
  })
  // console.log('bucket', files[0].bucket || options.bucket)
  // console.log('keys', keys)
  // console.log('files', files)
  try {
    const result = await s3.deleteObjects({
      Bucket: files[0].bucket || options.bucket,
      Delete: {
        Objects: keys
      }
    }).promise()
    return result
  } catch(e) {
    console.log(e)
    throw e
  }
}

module.exports = {
  listS3Objects,
  getS3ObjectUrl,
  generateDownloadUrl,
  deleteS3Object,
  deleteS3Objects,
  s3
}


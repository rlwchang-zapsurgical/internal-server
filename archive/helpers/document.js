const aws = require('./awsHelpers')
const models = require('../models')
const mime = require('mime')
const _ = require('lodash')

const file = {
  mime: 'String | list of mime types',
  ext: 'String | list of extensions',
  name: 'name of the files',
}

const options = {
  bucket: 'Bucket to upload documents to',
  action: 'Action to take. It should be putObject by default',
  prefix: 'Any prefix to attach to the front of the file name',
  mimes: 'Array of mime types that are accepts', //not fully implementd yet
  exts: 'Array of extensions that are accepts.', //not fully implementd yet
  // These have to do with the DB parts
  Model: '',
  query: '',
  property: '',
  overwrite: '',
  replace: '' 
}

function isValidFileType(file={}, options={}) {
  const {mimes=[], exts=[]} = options
  const types = [...mimes, ...exts]
  let fileMime = file.mime || mime.getType(file.name)
  let ext = file.ext || mime.getExtension(fileMime)
  if(types.length == 0) {return true}
  // console.log('file', file)
  // console.log('types', types)
  // console.log('types length', types.length)
  // console.log('mime contains right type', _.includes(types, fileMime))
  // console.log('exts contains right type', _.includes(types, exts))
  // console.log('filemime', fileMime)
  // console.log('ext', ext)
  return _.includes(types, fileMime) || _.includes(types, ext)

}

const getDocUploadLink = async(file={}, options={}) => {
  if (!isValidFileType(file)) {return null}
  const key = options.prefix ? `${options.prefix}/${file.name}` : file.name
  const url = await aws.getS3ObjectUrl(
    options.bucket,
    key,
    {action: 'putObject'},
    {
        ContentType: file.mime || file.type,
        ServerSideEncryption: "AES256",        
    }
  )

  return url
}

const getDocsUploadLinks = async(files=[], options={}) => {
  const results = {}

  if (!Array.isArray(files)) {files = [files]}

  for (const file of files) {
    const url = await getDocUploadLink(file, options)
    results[file.name] = {
      ...file,
      url
    }
  }

  return results
}
exports.getDocsUploadLinks = getDocsUploadLinks

// getDocsUploadLinks([
//   {
//     name: 'gibberish.docx',
//     mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
//   }
// ], {
//   bucket: 'zap-surgical-installers',
//   prefix: 'ConfigDocuments',

// }).then(console.log)
exports.addDocToDB = async(file={}, options={}) => {
  addDocsToDB([file], options)
}

async function addDocsToDB(files=[], options={}) {
  let {
    Model,
    query,
    property,
    overwrite=true,
    replace=false
  } = options
  if (typeof Model == 'string') {Model = models[_.startCase(Model)]}
  if (typeof query == 'string') {query = {_id: query}}

  const modelInstance = await Model.findOne(query)

  if (typeof property == 'string') {property = [property]}

  let pointer = modelInstance
  let pointerCopy = modelInstance.toObject()
  let finalProp = property.slice(-1)[0]
  // {
  //   l1: {
  //     l2: {
  //       docs: []
  //     }
  //   }
  // }
  for (let i = 0; i < property.length; i++) {
    const prop = property[i]
    
    if(i == property.length - 1) {
      pointer[prop] = pointer[prop] || []
      pointerCopy[prop] = pointerCopy[prop] || []
    } else {
      pointer = pointer[prop]
      pointerCopy = pointerCopy[prop]
    }
  }

  let filesFormatted = files.map(file => {
    const {bucket, prefix} = options
    if(typeof file == 'string') {file = {name: file}}
    let fileMime = file.mime || mime.getType(file.name)
    let ext = file.ext || mime.getExtension(fileMime)
    return {
      ...file,
      mime: fileMime,
      ext,
      bucket,
      prefix
    }
  })
  // console.log('filesFormatted', filesFormatted)

  filesFormatted = filesFormatted.filter((file) => isValidFileType(file, options))

  if (replace) {
    pointer[finalProp] = filesFormatted
  } else if (overwrite) {
    pointer[finalProp] = _.unionWith(pointerCopy[finalProp] || [], filesFormatted, _.isEqual)
  } else {
    pointer[finalProp] = pointer[finalProp].concat(filesFormatted)
  }
  // console.log('files', files)
  // console.log('filesFormatted', filesFormatted)
  // console.log('finalProp', finalProp)
  // console.log('pointerCopy', pointerCopy)
  // console.log('pointer[finalProp]', pointer[finalProp])
  await pointer.save()
  return pointer.toObject()
}
exports.addDocsToDB = addDocsToDB

// addDocsToDB([{
//   name: 'test.docx',
// }], {
//   Model: 'software',
//   query: {_id: '5b4fcb124cfc323a87d71ef9'},
//   property: 'instructions'
// }).then((item) => console.log('adding Doc', item))

const deleteDoc = async(file={}, options={}) => {
  deleteDocs([file], options)
}
exports.deleteDoc = deleteDoc

async function deleteDocs(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}

  files = files.filter(file => isValidFileType(file, options))
  const result = await aws.deleteS3Objects(files, options)
  if(result) {return await deleteDocsFromDB(files, options)}
}
exports.deleteDocs = deleteDocs

exports.deleteDocFromDB = async(file={}, options={}) => {
  deleteDocsFromDB([file], options)
}

async function deleteDocsFromDB(files=[], options={}) {
  if (!Array.isArray(files)) {files = [files]}
  files = files.filter(file => isValidFileType(file, options))
  let {
    Model,
    query,
    property,
    overwrite=true,
    replace=false
  } = options
  if (typeof Model == 'string') {Model = models[_.startCase(Model)]}
  if (typeof query == 'string') {query = {_id: query}}

  const modelInstance = await Model.findOne(query)
  
  if (typeof property == 'string') {property = [property]}

  let pointer = modelInstance
  let pointerCopy = modelInstance.toObject()
  let finalProp = property.slice(-1)[0]
  // {
  //   l1: {
  //     l2: {
  //       docs: []
  //     }
  //   }
  // }
  for (let i = 0; i < property.length; i++) {
    const prop = property[i]
    
    if(i == property.length - 1) {
      pointer[prop] = pointer[prop] || []
      pointerCopy[prop] = pointerCopy[prop] || []
    } else {
      pointer = pointer[prop]
      pointerCopy = pointerCopy[prop]
    }
  }
  
  let remainingDocs = [...pointerCopy[finalProp]]

  for(let file of files) {
    if (typeof file == 'string') { file = {name: file} }
    // Try filtering first.
    const filteredDocs = _.filter(remainingDocs, (doc) => doc.name != file.name)
    if (filteredDocs.length == (remainingDocs.length - 1)) {
      remainingDocs = filteredDocs
      continue
    } else{
      // If there is more than one element removed, check using deep comparison
      const filteredDocs = _.filter(remainingDocs, (doc) => !_.isEqual(doc, file))
      
      if (filteredDocs.length == (remainingDocs.length - 1)) {
        remainingDocs = filteredDocs
        continue
      } else {
        // If there is no match with deep comparison, delete first instance
        const targetObject = _.find(remainingDocs, (doc) => doc.name == file.name)
        remainingDocs = _.filter(remainingDocs, (doc) => !_.isEqual(doc, targetObject))
      }
    }
  }
  pointer[finalProp] = remainingDocs
  await pointer.save()
  return pointer.toObject()
}

exports.deleteDocsFromDB = deleteDocsFromDB
// deleteDocsFromDB([
//   'test.docx'
// ], {
//   Model: 'software',
//   query: {_id: '5b4fcb124cfc323a87d71ef9'},
//   property: 'instructions'
// }).then(console.log)
const vsts = require('../vsts')
const _ = require('lodash')
const {sendMail} = require('../../packages/nodemailer')
const {Group} = require('../../models')

// Note that if people on VSTS/Azure DevOps delete a team, this information will need to be updated
const projectReference = {
  'tps': {
    project: 'tps',
    team: '6fb57ee6-3f9c-4709-a6e3-ebeec834e39a'
  },
  'treatment': {
    project: 'treatment',
    team: '647c3274-070e-47b1-9061-400328e94c34'
  },
  'entMgmt': {
    project: 'entMgmt',
    team: 'e954713f-9137-4c85-9dac-cef5a7a270a3'
  },
  'siteMgmt': {
    project: 'siteMgmt',
    team: '7801f4cb-5dfb-412e-bb8f-1ab1c0856f81'
  }
}

const messageTemplates = {
  'bug': {
    subject: "🐛 It's a bug invasi🐞n! 🐜",
    message: "My 🕷 SPIDER senses are tingling. I think you got some bugs running loose here.",
    defaultIssue: "There is an open bug of severity 2 or greater.",
    signature: "From your lovely bug mashes,"  
  },
  'test': {
    subject: "👨‍🔬 Uhhhh Boss, you sure about these tests?",
    message: "So I was testing your life savings 💸 against fire 🔥. Your savings kind of lost... What was that? That's not what I was supposed to be testing? Hmm, you might want to specify some test steps in that case.",
    defaultIssue: "There are no test steps defined.",
    signature: "From your non-existent tester,"  
  },
  'srs': {
    subject: "🕵️ So I was investigating your SRS...",
    message: "Was I supposed to see a blank page. I'm looking at SRS Details...but there's not a whole lot of details there.",
    defaultIssue: "There are no SRS details defined.",
    signature: "From your not-so-SeRiouS lover"  
  },
  'version': {
    subject: "❓❓ This is Zap Bot, version #⁉️⁉️⁉️⁉️",
    message: "Wait what!? What version am I? That's what happens when you don't include a version number. Software like me get an identity crisis. 😕❓",
    defaultIssue: "The version number was not specified.",
    signature: " From your version-deprived buddy,"  
  },
  'default': {
    subject: "🤖 Bzzzt Zap Bot Here",
    message: "Zap Bot here with another possibly important, arguable spammy message."
  },
  'goodJob': {
    subject: "👍 Good work!",
    message: "For this sprint I couldn't find any issues...and I'm a bot. Nice work! Give yourself a high-five ✋. I'd give you one, but that would mean you slapping your screen.",
    signature: "From your non-exclusive, non-personal cheerleader,"
  }
}


exports.checkSprintItems = checkSprintItems
async function checkSprintItems(projectName, options={}) {
  const project = projectReference[projectName]
  const missingFields = {
    'bug': [],
    'srs': [],
    'test': [],
    'version': []
  }
  try {
    let items = await vsts.getLatestSprintItems(project.project, project.team, options)
    // Occasionally this fails, so this acts as a second chance.
    items = _.isEmpty(items) ? await vsts.getLatestSprintItems(project.project, project.team, options) : items

    for(let item of items) {
      const {id, url, fields} = item
  
      if (hasOpenBugIssue(item)) {missingFields.bug.push(item)}
      if (!fields['Custom.SRSDetails']) {missingFields.srs.push(item)}
      if (!fields['Custom.TestSteps']) {missingFields.test.push(item)}
      if (fields['System.State'] == 'Committed' && !fields['zapsurgicalScrum.FixedoraddedinRelease']) {missingFields.version.push(item)}
    }
    // console.log(missingFields)

    if (options.sendNotification) {
      const issues = [] // e.g. ['bugs', 'srs']
      let projectGroup = await Group.findOne({name: `${projectName}`})
      let testingGroup = await Group.findOne({name: `${projectName}-test`})
      let srsGroup = await Group.findOne({name: 'srs'}) //As of 10-29-18, it's basically just Larry.
      let projectMembers = _.map(projectGroup.members, member => member.email)
      let projectManagers = _.map(projectGroup.managers, manager => manager.email)
      let testingMembers = _.map(testingGroup.members, member => member.email)
      let testingManagers = _.map(testingGroup.managers, manager => manager.email)
      let srsMembers = _.map(srsGroup.members, member => member.email)
      
      let initialContacts = [
        ...projectMembers,
        ...projectManagers,
        ...testingMembers,
        ...testingManagers,
        ...srsMembers
      ]
      
      let finalContacts = []
      
      // for (let i = 0; i < 10; i++) {
      //   sendMail(['RichardC@zapsurgical.com'], 'Test ' + i, 'i')
      // }
      // let testMembers = ['test1@randiouhnvf.com', 'test2@randiouhnvf.com', 'test3@randiouhnvf.com', 'test61@randiouhnvf.com', 'test1@randiouhnvf.com', 'test671@randiouhnvf.com', 'test14@randiouhnvf.com', 'test15@randiouhnvf.com']
      // sendMail(testMembers, 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: ${testMembers.join(', ')}`)
      // sendMail(['ZapBot@zapsurgical.com', 'RichardC@zapsurgical.com'], messageTemplates.bug.subject, createIssueMessage('bug', [{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}},{id: 'test', url: 'test', fields: {}}], messageTemplates.bug.message))

      // sendMail(['richardlwchang@gmail.com'], 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: ${[...managerGroup, ...testingGroup].join(', ')}`)
      // sendMail(['ZapBot@zapsurgical.com', 'RichardC@zapsurgical.com', 'fictionalemail.@fictionalemail1231235.com'], 'Email Summary', `Emails ${options.test ? 'would be' : 'were'} sent to the following indiviudals: noone`)
      _.forEach(missingFields, (field, type) => {

        if (!_.isEmpty(field)) { issues.push(type) }
      })

      if (!options.test) {
        // console.log('test does not work!')
        if (!_.includes(issues, 'test')) {
          sendMail(
            options.sendNotification.to || [...testingMembers, ...testingManagers],
            messageTemplates.goodJob.subject,
            messageTemplates.goodJob.message
            )
        }
        
        if (!_.some(issues, issue => _.includes(['bug', 'srs', 'version'], issue))) {
          sendMail(
            options.sendNotification.to || [...projectMembers, ...projectManagers],
            messageTemplates.goodJob.subject,
            messageTemplates.goodJob.message
            )
        }
      }

        _.forEach(issues, issue => {
          // console.log(createIssueMessage(issue, missingFields[issue], messageTemplates[issue].defaultIssue))
          let target
  
          switch(issue) {
            case 'test':
              target = [...testingManagers, ...testingMembers]
              break;
            case 'srs':
              target = [...srsMembers, ...projectMembers, ...projectManagers]
              break;
            default:
              target = [...projectMembers, ...projectManagers]
          }
          finalContacts = [...finalContacts, ...target]
          // console.log('target', target)
            if (!options.test) {

            sendMail(
              options.sendNotification.to || target,
              messageTemplates[issue].subject,
              {
                message: messageTemplates[issue].message,
                addenum: createIssueMessage(issue, missingFields[issue], messageTemplates[issue].defaultIssue)
              },
              undefined,
              console.error
            )
          }
        })
      finalContacts = _.uniq(finalContacts)
      const uncontacted =_.difference(initialContacts, finalContacts).join(', ')
      sendMail(
        'RichardC@zapsurgical.com',
        'Email Summary',
        `Emails ${options.test ? '<strong>would be</strong>' : 'were'} sent to the following indiviudals: ${finalContacts.join(', ')}. ${ uncontacted ? 'The following individuals were not e-mailed this time: ' + uncontacted : ''}`
      )    
      }      
    return missingFields
  } catch (err) {
    console.log(err)
  }
}

function createIssueMessage(key, issues=[], defaultMessage) {
  // console.log('defaultMessage', defaultMessage)
  const issuesHtml = `
      You might want to check out the ones below:
      <h3>${key.toUpperCase()} Issues</h3>
      <table>
        <tr>
          <th>Work Id</th>
          <th>Issue</th>
        </tr>
        ${_.map(issues, (issue) => {
          // console.log('issue', issue)
          const url = `https://zapsurgical.visualstudio.com/${issue.fields["System.TeamProject"]}/_workitems/edit/${issue.id}`
          return (
            "<tr>\
            <td><a href='" + url + "'>" + issue.id + "</a></td>\
            <td>" + (issue.message || defaultMessage) + "</td>\
            </tr>"
            )
        }).join('')}
      </table>
      <br/>
      <p>${messageTemplates[key].signature}</p>
      <p> 🤖 Zap Bot</p>
    `

  return issuesHtml
}

function hasOpenBugIssue(item) {
  const {fields} = item
  const allowedBugStates = ['Done', 'Approved', 'Committed', 'Removed']

  return (
    fields["System.WorkItemType"] == "Bug" &&
    _.includes(allowedBugStates, fields['System.State']) &&
    (
      parseSeverity(item, "Custom.RMSeverity") >= 2 ||
      parseSeverity(item, "Microsoft.VSTS.Common.Severity" >= 2)
    )
  )
}

function parseSeverity(item, field) {
  const {fields} = item
  return Number(_.get(fields, field, 0))
}
let helpers = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive' &&
      file.toLowerCase() != 'data.js'
      ) {
     helpers = {...helpers, ...require(`./${file.replace('.js', '')}`)}
  }
});

module.exports = helpers
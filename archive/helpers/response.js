exports.successResponse = (res, data, message, code = 200) => {
    // console.log('successData', data)
    res.status(code)
    return res.json({
        status: 'success',
        message: message || 'Operation completed successfully.',
        data
    })
}

exports.pendingResponse = (res, data, message, code = 200) => {
    // console.log('successData', data)
    res.status(code)
    return res.json({
        status: 'pending',
        message: message || 'Operation is a long-running process and still running. Try checking again later.',
        data
    })
}

exports.errorResponse = (res, error, message, code = 400) => {
    // console.log('error:', error)
    res.status(code)
    // try {
        return res.json({
            status: 'error',
            message: message || 'Operation failed to complete.',
            error
        })
    // } catch(err) {
        // const {response: {data}} = error
        // res.status(500)
        // return res.json(data)
    // }
}

exports.unauthenticatedResponse = (res, error, message, code = 401) => {
    res.status(code)
    return res.json({
        status: 'error',
        message: message || 'You need to be authenticated to access this resource.',
        error
    })
}

exports.unauthorizedResponse = (res, error, message, code = 403) => {
    res.status(code)
    return res.json({
        status: 'error',
        message: message || 'You are not authorized to access this resource.',
        error
    })
}
const sampleServiceHookData = {
  "subscriptionId": "9314cfa3-2214-4d1d-a3ec-ba6198b98ec8",
  "notificationId": 363,
  "id": "facebc5e-ae2b-41e7-9f57-aeec8e177d72",
  "eventType": "build.complete",
  "publisherId": "tfs",
  "message": {
    "text": "Build 20181030.3 succeeded",
    "html": "Build <a href=\"https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f6821\">20181030.3</a> succeeded",
    "markdown": "Build [20181030.3](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f6821) succeeded"
  },
  "detailedMessage": {
    "text": "Build 20181030.3 succeeded",
    "html": "Build <a href=\"https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f6821\">20181030.3</a> succeeded",
    "markdown": "Build [20181030.3](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f6821) succeeded"
  },
  "resource": {
    "_links": {
      "self": {
        "href": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/Builds/6821"
      },
      "web": {
        "href": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_build/results?buildId=6821"
      },
      "sourceVersionDisplayUri": {
        "href": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/builds/6821/sources"
      },
      "timeline": {
        "href": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/builds/6821/Timeline"
      },
      "badge": {
        "href": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/status/29"
      }
    },
    "properties": {},
    "tags": [],
    "validationResults": [],
    "plans": [
      {
        "planId": "a94f01cb-0547-4e9c-bb17-03439d4cfb81"
      }
    ],
    "triggerInfo": {
      "pr.number": "651"
    },
    "id": 6821,
    "buildNumber": "20181030.3",
    "status": "completed",
    "result": "succeeded",
    "queueTime": "2018-10-30T22:30:04.3761463Z",
    "startTime": "2018-10-30T22:30:07.1907323Z",
    "finishTime": "2018-10-30T22:47:17.1213509Z",
    "url": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/Builds/6821",
    "definition": {
      "drafts": [],
      "id": 29,
      "name": "TPS",
      "url": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/Definitions/29?revision=52",
      "uri": "vstfs:///Build/Definition/29",
      "path": "\\official",
      "type": "build",
      "queueStatus": "enabled",
      "revision": 52,
      "project": {
        "id": "d7fe02ce-33ab-465e-91e7-e277d7483321",
        "name": "TPS",
        "description": "Treatment planning system",
        "url": "https://zapsurgical.visualstudio.com/_apis/projects/d7fe02ce-33ab-465e-91e7-e277d7483321",
        "state": "wellFormed",
        "revision": 63110924,
        "visibility": "private"
      }
    },
    "buildNumberRevision": 3,
    "project": {
      "id": "d7fe02ce-33ab-465e-91e7-e277d7483321",
      "name": "TPS",
      "description": "Treatment planning system",
      "url": "https://zapsurgical.visualstudio.com/_apis/projects/d7fe02ce-33ab-465e-91e7-e277d7483321",
      "state": "wellFormed",
      "revision": 63110924,
      "visibility": "private"
    },
    "uri": "vstfs:///Build/Build/6821",
    "sourceBranch": "refs/pull/651/merge",
    "sourceVersion": "e926d0f9a57a7e670010accb5f960617b34cb3b3",
    "queue": {
      "id": 1,
      "name": "Default",
      "pool": {
        "id": 1,
        "name": "Default"
      }
    },
    "priority": "normal",
    "reason": "pullRequest",
    "requestedFor": {
      "displayName": "Ben Chen",
      "url": "https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/8fad799a-5598-63f6-9cde-fff4cc53806c",
      "_links": {
        "avatar": {
          "href": "https://zapsurgical.visualstudio.com/_apis/GraphProfile/MemberAvatars/aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj"
        }
      },
      "id": "8fad799a-5598-63f6-9cde-fff4cc53806c",
      "uniqueName": "Ben@zapsurgical.com",
      "imageUrl": "https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=8fad799a-5598-63f6-9cde-fff4cc53806c",
      "descriptor": "aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj"
    },
    "requestedBy": {
      "displayName": "Microsoft.VisualStudio.Services.TFS",
      "url": "https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000",
      "_links": {
        "avatar": {
          "href": "https://zapsurgical.visualstudio.com/_apis/GraphProfile/MemberAvatars/s2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA"
        }
      },
      "id": "00000002-0000-8888-8000-000000000000",
      "uniqueName": "00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288",
      "imageUrl": "https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000",
      "descriptor": "s2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA"
    },
    "lastChangedDate": "2018-10-30T22:47:17.65Z",
    "lastChangedBy": {
      "displayName": "Microsoft.VisualStudio.Services.TFS",
      "url": "https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000",
      "_links": {
        "avatar": {
          "href": "https://zapsurgical.visualstudio.com/_apis/GraphProfile/MemberAvatars/s2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA"
        }
      },
      "id": "00000002-0000-8888-8000-000000000000",
      "uniqueName": "00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288",
      "imageUrl": "https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000",
      "descriptor": "s2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA"
    },
    "parameters": "{\"system.pullRequest.pullRequestId\":\"651\",\"system.pullRequest.sourceBranch\":\"refs/heads/features/feature3078\",\"system.pullRequest.targetBranch\":\"refs/heads/releases/v1.7.16\",\"system.pullRequest.sourceCommitId\":\"077866200c7eaceedec6db79fa304330436f5c92\",\"system.pullRequest.sourceRepositoryUri\":\"https://zapsurgical.visualstudio.com/TPS/_git/TPS\"}",
    "orchestrationPlan": {
      "planId": "a94f01cb-0547-4e9c-bb17-03439d4cfb81"
    },
    "logs": {
      "id": 0,
      "type": "Container",
      "url": "https://zapsurgical.visualstudio.com/d7fe02ce-33ab-465e-91e7-e277d7483321/_apis/build/builds/6821/logs"
    },
    "repository": {
      "id": "e092e93f-511e-407d-9271-a10aeb13db77",
      "type": "TfsGit",
      "name": "TPS",
      "url": "https://zapsurgical.visualstudio.com/TPS/_git/TPS",
      "clean": null,
      "checkoutSubmodules": false
    },
    "keepForever": false,
    "retainedByRelease": false,
    "triggeredByBuild": null
  },
  "resourceVersion": "2.0",
  "resourceContainers": {
    "collection": {
      "id": "de1f328d-b12a-4368-afec-1208abbcce65",
      "baseUrl": "https://zapsurgical.visualstudio.com/"
    },
    "account": {
      "id": "62e5a3ed-0139-4177-a9d0-6af10375baee",
      "baseUrl": "https://zapsurgical.visualstudio.com/"
    },
    "project": {
      "id": "d7fe02ce-33ab-465e-91e7-e277d7483321",
      "baseUrl": "https://zapsurgical.visualstudio.com/"
    }
  },
  "createdDate": "2018-10-30T22:47:24.904304Z"
}
const helpers = require('../helpers')
const restful = require('./restful')
const response = require('./response')
const s3 = require('./awsHelpers')
const {MasterDisk} = require('../models')
const baseOptions = {
  bucket: 'zap-surgical-masterdisks',
  // These have to do with the DB parts
  Model: 'MasterDisk',
}

const documentOptions = {
  ...baseOptions,
  prefix: 'MasterDiskDocuments/',
  exts: ['doc', 'docx'],
  property: ['documents'],
}

const diskOptions = {
  ...baseOptions,
  exts: ['iso'], //not fully implementd yet
}


exports.masterDiskIndex = masterDiskIndex
async function masterDiskIndex(req, res) {
  let result = []
  let disks = await MasterDisk.find()
  // console.log(result)
  for(let disk of disks) {
    const diskCopy = disk.toObject()
    const file = diskCopy.file || {}
    const documents = diskCopy.documents
    if (file.s3BucketName && file.s3ObjectName) {
      diskCopy.file.url = await s3.generateDownloadUrl(file.s3BucketName, file.s3ObjectName)
    }

    for (let doc of documents) {
      if (doc.s3BucketName && doc.s3ObjectName) {
        doc.url = await s3.generateDownloadUrl(doc.s3BucketName, doc.s3ObjectName)
      }
    }

    result.push(diskCopy)
  }
  
  response.successResponse(res, result)
}

exports.masterDiskShow = masterDiskShow
async function masterDiskShow(req, res) {
  const {id} = req.params
  let disk = await MasterDisk.findOne({_id: id})
  const diskCopy = disk.toObject()
  const file = diskCopy.file || {}
  const documents = diskCopy.documents

  if (file.s3BucketName && file.s3ObjectName) {

    diskCopy.file.url = await s3.generateDownloadUrl(file.s3BucketName, file.s3ObjectName)
  }

  for (let doc of documents) {
    if (doc.s3BucketName && doc.s3ObjectName) {
      doc.url = await s3.generateDownloadUrl(doc.s3BucketName, doc.s3ObjectName)
    }
  }
  const result = diskCopy
  
  response.successResponse(res, diskCopy)
}
const _ = require('lodash')

function getGreaterVersion(versionOne, versionTwo) {  
    if (versionOne == versionTwo) {return versionOne}
  
    const versionOneValues = formatVersionNumbersArray(versionOne)
    const versionTwoValues = formatVersionNumbersArray(versionTwo)

    if (versionOneValues.length > versionTwoValues.length) {
      base = versionOneValues
    } else {
      base = versionTwoValues
    }
  
    for (let i; i < base.length; i++) {
      if (versionOneValues[i] > versionTwoValues[i]) {
        return versionOne
      } else if (versionOneValues[i] < versionTwoValues[i]) {
        return versionTwo
      }
    }
  }

function sortByVersion(collection=[], asc=false, getVersionNumber) {
    // getVersionNumber is a function to apply to each element to get the version numbers
    let result

    if (getVersionNumber) {
        result = collection.sort((prev, curr) => getGreaterVersion(getVersionNumber(prev), getVersionNumber(curr)) == prev ? 1 : -1)
    } else {
        result = collection.sort((prev, curr) => getGreaterVersion(prev, curr) == prev ? 1 : -1)
    }

    if (asc) {
        return result;
    } else {
        return _.reverse(result);
    }
}

function formatVersionNumbers(version) {
    return formatVersionNumbersArray(version).join('.')
}

function formatVersionNumbersArray(version) {
    function parseGreekVersions(version) {
        let parsedVersion = version.replace(/[.]?alpha()[.]?/i, '.1.')
        parsedVersion = parsedVersion.replace(/[.]?delta()[.]?/i, '.2.')
        parsedVersion = parsedVersion.replace(/[.]?beta()[.]?/i, '.3.')
        return parsedVersion
    }
    return _.map(parseGreekVersions(version).split(/[.]/), value => _.padStart(value, 5, '0'))
}

module.exports = {
    getGreaterVersion,
    sortByVersion,
    formatVersionNumbers,
    formatVersionNumbersArray
}
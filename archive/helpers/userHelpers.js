const {User} = require('../models'); 
const speakeasy = require('speakeasy');
const _ = require('lodash');

exports.getUsers = (req, res) => {
    User.find()
        .then(users => res.json({status: 'success', message: 'Users successfully retrieved', users}))
        .catch(err => res.json({message: 'Request for all users failed!', err}))
}

exports.register = (req, res) => {
    const currentUser = req.user;
    
    if (currentUser) {
        const userBody = {
            ..._.pickBy(req.body, (value, key) => (key !== 'password' && key !== 'verifyPassword')),
            secret: speakeasy.generateSecret({
                length: 30
            })
        }

        // Cap auth level to match the requesting users.
        // if (userBody.authLevel < currentUser.authLevel) userBody.authLevel = currentUser.authLevel

        User.register(
            userBody,
            req.body.password,
            (err, user) => {
                if (user) res.json({
                        status: "success",
                        message: "User successfully created!",
                        user: filterUserData(user)
                        // token
                    })
                else res.json(err)
            }
        )
    } else {
        res.json({status: 'error', message: 'You are unauthorized to create such a user.'})
    }
}

exports.showUser = (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            const userInfo = filterUserData(user)
            res.json({message: 'You have successfully retrieved the user data', user: userInfo})
        })
        .catch(err => console.log(err))
}

exports.editUser = (req, res) => {
    const {authLevel: adminAuthLevel} = req.user;
    // console.log(req.user);
    // Username will be static and cannot be changed
    // console.log(req.body)
    const userBody = filterUserData(req.body);
    const {oldPassword, newPassword} = req.body;
    // console.log(userBody)

    // This is a quick and generic method of updating a record, but it does not provide any security and checking.
    // User.findByIdAndUpdate(req.params.id, userBody, {new: true})
    //     .then(user => res.json(user))
    //     .catch(err => console.log(err))
    // console.log('Invoking editUser')
    User.findById(req.params.id)
        .then(user => {
            // const {authLevel: userAuthLevel} = user;

            // if (isAuthorized(req.user, user)) {
            //     // console.log('Action permitted!')
                _.forEach(userBody, (value, key) => {
                    user[key] = value;
                    // Cap the authLevel that a given user can modify (modify up to own authLevel)
                    // if (key == 'authLevel' && value < adminAuthLevel) {
                    //     user[key] = adminAuthLevel
                    // }
                })
            //     // console.log(user)
                let result = { 
                    message: "You have successfully modified the user.",
                    user: filterUserData(user)
                };

                if (oldPassword && newPassword) {
                    // console.log('There was an old and new password')
                    user.changePassword(oldPassword, newPassword, (err) => {
                        // console.log(err)
                        if (err) result = {status: 'error', message: err}
                        res.json(result)
                    })
                    // .then(() => console.log(`Password successfully changed to: ${newPassword}`))
                    // .catch(console.log)
                } else {
                    res.json(result)                                
                }
                user.save();
            // } else {
            //     // console.log('Not permitted! Throwing an error now!')
            //     throw 'Your authorization level is not high enough to modify this user.'
            // }            
        })
        .catch(error => res.json({message: 'You are unable to modify this user.', error}));
 }

 exports.removeUser = (req,res) => {
    const {authLevel: adminAuthLevel} = req.user;
    // console.log(req.user);
    // Username will be static and cannot be changed
    // console.log(req.body)
    // console.log(userBody)
    // Naive Approach
    // User.findByIdAndRemove(req.params.id)
    //     .then(user => res.json({message: 'Successfully removed the user!', username: user.username}))
    //     .catch(error => res.json({message: 'Failed to remove the user', error}))

    User.findById(req.params.id)
    .then(user => {
        // console.log('User found!')
        const {authLevel: userAuthLevel} = user;

        // if (isAuthorized(req.user, user)) {
            user.remove();
            res.json({message: 'User has been successfully deleted.', user: filterUserData(user)})
        // } else {
        //     // console.log('Not permitted! Throwing an error now!')
        //     throw 'Your authorization level is not high enough to delete this user.'
        // }            
    })
    .catch(error => res.json({message: 'You are unable to delete this user.', error}));
 }

//  editUser()
// Takes userData in as an object and filters out sensitive data

function filterUserData(userData) {
    const result = _.pickBy(userData, (value, key) => (
        key !== 'password' &&
        key !== 'verifyPassword' &&
        key !== 'oldPassword' &&
        key !== 'newPassword' &&        
        key !== 'hashingAlg' &&
        key !== 'secret' &&
        key !== 'allowedDevices' &&
        key !== 'hash' &&
        key !== 'salt'
    ));

    return result;
}

function isCurrentUser(userOne, userTwo) {
    return (userOne._id == userTwo._id)
}

function isAuthorized(currentUser, targetUser, same=false, self=true) {
    let authorization = false;
    if (currentUser.authLevel < targetUser.authLevel) authorization = true;
    // console.log('authlevel higher', authorization)
    if (same && (currentUser.authLevel <= targetUser.authLevel)) authorization = true;
    // console.log('authlevel same', authorization)    
    if (self && currentUser._id.toString() == targetUser._id.toString()) authorization = true;
    return authorization
}

const salt = require('./salt')
const response = require('../response')
const minion = require('./minionHelpers')
const redis = require('../../packages/redis')
const _ = require('lodash')

exports.getPackages = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.saltCmd('pkg.list_pkgs', tgt, {cacheExp: 60 * 60 * 24})
  request
  .then(data => salt.saltSuccessResponse(res, data))
  .catch(err => response.errorResponse(res, err))
}

exports.reinitializePackages = async (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.saltCmd('pkg.refresh_db', tgt)
}

exports.modifyPackage = async (req, res) => {
  const {id} = req.params
  const {
    tgt='*_id_*',
    action,
    name,
    path,
    ext='msi',
    options: inputOptions={}
  } = req.body

  const {refresh, quiet, passive, pillar={}, ...otherOptions} = inputOptions

  const options = {
    isAsync: true,
    pillar: {
      path,
      name: _.kebabCase(name),
      refresh,
      quiet,
      passive,
      ...pillar
    },
    ...otherOptions
  }

  const states = []
  switch (action.toLowerCase()) {
    case 'install':
      states.push('installer_install')
      break
    case 'uninstall': // Does not work yet
      states.push('installer_uninstall')
      break
    default:
      break
  }
  // let tries = 1 // This is changed by the handler function
  // This is a code smell, and should probably be changed at some point
  // The goal is to serve as an error handler, in case the salt request
  // doesn't get through, it will keep running until the timeout time.
  // With tries, we can limit it.
  const jid = await salt.applyStates(states, tgt, options)
  response.successResponse(res, jid)

  // console.log('jid', jid)
  try {
    salt.pollSaltJobResults(jid, handler, {timeOut: 300, timeInterval: 30})
  } catch (err) {
    console.log('err from polling', err)
  }

  function handler(result, done) {
    const data = minion.formatMinionData(result)
    console.log('data from handler', data)
    console.log('data is empty', _.isEmpty(data))
    let resultPending = false || _.isEmpty(data)
    _.forEach(data, (system, systemId) => {
      console.log('looping through systems!', systemId)
      // system is in the form of a number
      // "1": {
      //   state1: {...},
      //   state2: {...},
      //   state3: {...},
      //   ...
      // }
      _.forEach(system, (states, pcType) => {
        console.log('looping through pcTypes!', pcType)
        // if (!_.includes([
          //   'operator',
          //   'control',
          //   'database'
          // ], pcType.toLowerCase())) {
            //   return
            // }
        const cacheKey = {
          id: systemId,
          type: pcType
        }
        _.forEach(states, (stateResult, stateKey) => {
          console.log(`looping through states: ${stateKey}`)
          if (stateKey.match(/-install_package/)) {
            let status = checkStateStatus(stateResult)
            let error = null
            const options = {
              message: stateResult.comment
            }
            console.log('status', status)

            switch(status) {
              case 'success':
                break
              case 'error':
                error = stateResult.comment
                break
              case 'pending':
                resultPending = true
                break
              default:
                break
            }
            cachePkgInstallStatus(cacheKey, name, status, options)
          }
        })
      })
    })
    
    console.log('result pending', resultPending)
    if (!resultPending) {return done(null, data)}
    if (tries >= 10) {return done(`Failed to fetch results and quit after ${tries}.`)}
    // tries++
  }
}

function checkStateStatus(stateResult) {
  if (stateResult.result) {
    return 'success'
  }

  if (!stateResult.result && !_.isEmpty(stateResult.changes)) {
    return 'error'
  }
  
  if (!stateResult.result) {
    return 'pending'
  }
}

async function cachePkgInstallStatus(system={}, pkgName, status, inputOptions={}) {
  const {id, type} = system
  const options = {
    expire: (1000 * 60 * 60 * 6),
    message: undefined,
    ...inputOptions
  }
  const key = `system-${id}-${type}:pkg-install-status`
  // Cache the value with a default expiration time of 6 hours.
  await clearPkgInstallCache(key)
  
  const value = {
    status,
    expire: options.expire,
    message: options.message
  }

  console.log(`caching status for ${key}`, status)
  return await redis.hset(key, pkgName, JSON.stringify(value))
}

async function clearPkgInstallCache(key) {
  const currentTime = Date.now()
  const currentStatuses = await redis.hgetall(key)
  for(let pkgKey in currentStatuses) {
    let status = currentStatuses[pkgKey]
    try {
      status = JSON.parse(currentStatuses[pkgKey])
    } catch (err) {
      console.log(err)
    }

    if (currentTime > status.expire) {
      redis.hdel(key, pkgKey)
    }
  }
}
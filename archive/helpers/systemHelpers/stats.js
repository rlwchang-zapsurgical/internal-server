const salt = require('./salt')
const response = require('../response')
const _ = require('lodash')

exports.getSystemInfo = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.saltCmd('system.get_system_info', tgt, {cacheExp: 60 * 60 * 24, failedCacheExp: 20})
  request
  .then(data => {
    // console.log('data', data)
    // const sampleData = _.cloneDeep(data.return[0])
    // console.log('sampleData', sampleData)
    // console.log('sampleData checked', sampleData.sys_1_database_minion);
    // sampleData['sys_1_database_minion'] = false
    // console.log('sampleData', sampleData)
    // return salt.saltSuccessResponse(res, {return: [sampleData]})
    return salt.saltSuccessResponse(res, data)
  })
  .catch(err => response.errorResponse(res, err))
}

exports.getVitals = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request =  Promise.all([
    // salt.saltCmd('ps.cpu_percent', tgt),
    salt.saltCmd('status.cpuload', {tgt: `${tgt} and G@kernel:windows`, tgt_type: 'compound'}, {cacheExp: 300}),
    salt.saltCmd('ps.virtual_memory', tgt, {cacheExp: 300}),
    salt.saltCmd('ps.disk_usage', tgt, ['/'], {cacheExp: 300}),
  ])

  console.log(request);

  request
  .then(data => {
    console.log('data', data);
    return salt.saltSuccessResponse(res, data, ['cpu_load', 'memory', 'disk'])
  })
  .catch(err => response.errorResponse(res, err))
}

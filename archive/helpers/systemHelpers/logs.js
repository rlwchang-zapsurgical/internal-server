const salt = require('./salt')
const response = require('../response')
const aws = require('../awsHelpers')


const logsBucket = 'zap-surgical-system-logs';
exports.getLogs = async (req, res) => {  
  // This route currently runs relatively synchronously when fetching the signed Url for the logs.
  // Use a more asynchronous approach in the future when there are more logs.
  // Current time for 20,000 logs is at best 6.5 seconds to respond time with cache.
  // It can easily reach half a minute when the cache is not running.
  const {id} = req.params
  const prefix = id

  let results = {}
  try {
    const logObjects = await aws.listS3Objects(logsBucket, prefix)
    
    for (const log of logObjects) {
      const name = log["Key"]
      const pcType = getPcTypeFromLog(name)
      const systemId = id || getIdFromLog(name)
      const url = await aws.getS3ObjectUrl(logsBucket, name)
      results[systemId] = results[systemId] || {}
      results[systemId][pcType] = results[systemId][pcType] || []
      results[systemId][pcType].push({
        name,
        url
      })
    }
    response.successResponse(res, results)
  } catch (err) {
    response.errorResponse(res, err)
  }
}
exports.showLogs = (req, res) => {
  // This is accessing the native aws s3 sdk
  const fullFilename = decodeURIComponent(req.params.filename);
  const showParams = {
    Bucket: logsBucket,
    Key: fullFilename
  }

  let logContents = ''
  const logReadStream = aws.s3.getObject(showParams).createReadStream();

  logReadStream.on('data', data => logContents += data.toString());
  logReadStream.on('end', () => res.json(logContents))
}

exports.pushLogs = (req, res) => {
  // pcType queries are currently disabled. They can be re-enabled by modifying the tgt parameter
  // const tgt `sys_${systemId}_*${pcType}*`,
  // const {days, pcType='[database|control|operator]', fullPush} = req.body
  const {id} = req.params
  let {days=1, fullPush, pcType} = req.body
  let remainingDays = days;
  let tgt = id ? `*_${id}_*` : '*'
  if (pcType) tgt += pcType.toLowerCase()
  let arg = [`/ZapSurgical/Logs/`]
  let request

  if (fullPush) {
    days = 0;
    request = salt.saltCmd(`cp.push_dir`, tgt, arg)
  } else {
    for (remainingDays; remainingDays >= 0; remainingDays--) {
      let date = moment().subtract(remainingDays, 'days').format("YYYY-MM-DD");
      arg = [`/ZapSurgical/Logs/${date}`]
      request = salt.saltCmd(`cp.push_dir`, tgt, arg)
    }
  }
  
  const successMessage = `Request has been sent to push logs ${days ? 'for the past ' + days +' day(s)' : 'with a full push'}. Please note that it may take several minutes to a few hours depending on the load of the request.`
  // const request = Promise.all(pushRequests)
  // pushRequests[0].then(console.log).catch(console.log)
  // pushRequests[1].then(console.log).catch(console.log)
  // request.then(console.log).catch(console.log)
  request
  .then(({data}) => response.successResponse(res, null, successMessage))
  .catch(err => response.errorResponse(res, 'Failed to push logs'))
}

// exports.refreshLogs = (req, res) => {

// }

function getIdFromLog(filePath) {
  // Given a file path like "1/SystemLogs/15/PendantApp-Linac-15.log"
  // Get the first set of numbers, up to 4 digits long
  // The number set cannot have a / preceding it.
  // In the above file path, it was designed so the system Id is the first number
  // This returns null if there is no match or the first match
  return filePath.match(/^\d{1,4}/) && filePath.match(/^\d{1,4}/)[0]
}

function getPcTypeFromLog(filePath) {
  const fragments = filePath.split('/')
  
  for (const fragment of fragments) {
    if (fragment.toLowerCase().match(/broker|database/)) {
      return 'database'
    } else if (fragment.toLowerCase().match(/operator|console/)) {
      return 'operator'
    } else if (fragment.toLowerCase().match(/system|control/)) {
      return 'control'
    }
  }
}
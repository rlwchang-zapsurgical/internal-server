const DB = require('../../models');
const _ = require('lodash')
const uuid = require('uuid/v4')

const omissionList = [
  'index.js',
  'archive',
  'saltSync.js'
]
let helpers = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  // console.log(file)
  if (!_.includes(omissionList, file)) {
     helpers = {...helpers, ...require(`./${file.replace('.js', '')}`)}
  }
});

// DB.System.find().then(systems => {
//   systems.map((system) => {
//     const codes = []
//     for (let i = 0; i < 10; i++) {
//       codes.push({
//         dateIssued: new Date(),
//         uuid: uuid()
//       })
//     }
//     system.treatments = {
//       codes
//     }
//     system.save()
//   })
// })

// ====================
// RESTFUL Methods for Systems
// ====================

// async function test() {
//   const system = await DB.System.findOne({systemId: '1'})
//   .populate({
//     path: 'masterDisk',
//     populate: {
//       path: 'control'
//     }
//   }).populate({
//     path: 'masterDisk',
//     populate: {
//       path: 'control'
//     }
//   })
//   .populate({
//     path: 'masterDisk',
//     populate: {
//       path: 'control'
//     }
//   })
//   console.log('system', system)
//   const sameSystem = await DB.System.findOne({systemId: '1'})
//   console.log('sameSystem', sameSystem)
// }
// test()

helpers.getSystems = (req, res) => {
  DB.System.find().populate('masterDisk')
  .then(systems => res.json(systems))
  .catch(err => console.log(err))
}


helpers.showSystem = (req, res) => {
  const {id} = req.params;
  DB.System.findOne({systemId: id})
  .then(system => res.json(system))
  .catch(err => console.log(err))
}

helpers.createSystem = (req, res) => {
  const systemBody = req.body;
  // console.log(systemBody);
  DB.System.create(systemBody)
  .then(system => res.json(system))
  .catch(err => console.log(err))
}

helpers.editSystem = (req, res) => {
  const {id} = req.params;
  const systemBody = req.body;
  console.log(systemBody);
  DB.System.findOneAndUpdate(id, systemBody, {new: true})
  .then(system => res.json(system))
  .catch(err => console.log(err))
}

module.exports = helpers
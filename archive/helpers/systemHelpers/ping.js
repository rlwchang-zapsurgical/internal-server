const salt = require('./salt')
const response = require('../response')

exports.pingSystems = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = salt.saltCmd('test.ping', tgt, {cacheExp: 300})
  request
  .then(data => {
    salt.saltSuccessResponse(res, data)
  })
  .catch((err) => {
    console.log('An error occurred while pinging the systems')
    response.errorResponse(res, err)})
}
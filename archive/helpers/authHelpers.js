const passport = require('passport');
const jwt = require('jwt-simple');
const speakeasy = require('speakeasy');
const {User} = require('../models');
const _ = require('lodash');

const twilio = require('../packages/twilio');

//////////////////////////
// Start auth functions //
//////////////////////////
exports.jwtLogin = (req, res) => {
  // const {authLevel, roles, permissions} = req.user
  // res.json({authLevel, roles, permissions})
  res.json(req.user)
}

exports.login = (req, res) => {
  // res.json(req.user)
  const token = createToken(req.user)
  // console.log(token)
  // console.log(process.env.NODE_ENV);
  const { authLevel, roles, permissions } = req.user
  // res.json({ token, authLevel, roles, permissions })
  res.json({token, ...req.user})
}

exports.logout = (req, res) => {
  req.logout();
  res.json({
    "message": "Logout successful",
    "currentUser": req.user
  })
}

exports.localAuthentification = (req, res, next) => {
  if (process.env.NODE_ENV == "development") {
    next();
  } else {
    // console.log(typeof passport.authenticate("local"));
    return passport.authenticate("local", { session: false })(req, res, next);
  }
}

exports.jwtAuthentification = (req, res, next) => {
  if (process.env.NODE_ENV == "development") {
    next();
  } else {
    return passport.authenticate("jwt", { session: false })(req, res, next);
  }
}

// All purpose authentification and should be paired with one of the authentification middlware
exports.isAuthenticated = (req, res, next) => {
  if (req.user) {
    return next();
  } else if (process.env.NODE_ENV == "development") {
    req.user = {
      "roles": ['boss', 'admin'],
      "permissions": [],
      "hashingAlg": "SHA-256",
      "_id": "5ae746d771c809727020e6ec",
      "username": "development",
      "email": "development@zapsurgical.com",
      "number": "11111111111111111111",
      "authLevel": 0,
      "fingerprint": "1e1f51894bf2f9ea89c244804d8554b9",
      "secret": "PI2SY3LZMQ2DGNZ2OBBHSMJOGA3DO52UEE6EOQ2MI5NCC3DR",
      "firstName": "Testing",
      "lastName": "Developer"
    }
    return next();
  } else {
    console.log('Unauthorized user! You must be logged in to continue!')
    res.json({error: "You must be logged in to continue!"})
  }
}

// exports.isAuthorized = (req, res, next) => {
//
// }
exports.verifyLogin = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else if (process.env.NODE_ENV == "development") {
    return next();
  } else {
    res.json({error: "Your username or password is incorrect!"})
  }
}

exports.isMfaAuthenticated = (req, res, next) => {
  const {user: {allowedDevices=[], secret, number, _id}, body: {username, fingerprint, mfaToken, rememberDevice}} = req;

  // Device is a remembered device
  if (allowedDevices.indexOf(fingerprint) > -1 && rememberDevice) {
    console.log(`Device ${fingerprint} is a remembered device. Bypassing MFA.`);
    return next()
  }

  // Device is not a remembered device, but a token was submitted
  if (mfaToken) {
    console.log(`Submitted token: ${mfaToken}`);
    const verified = speakeasy.totp.verify({
      secret,
      encoding: 'base32',
      token: mfaToken,
      window: 6
    });
    console.log(`Verification of token: ${verified}`);
    // Add device to remembered devices if selected, otherwise remove
    if (verified) {modifyDevicesList(_id, fingerprint, !Boolean(rememberDevice))}

    return verified ? next() : res.json({error: "Invalid verification token!"})
  }

  // Device is not remembered and no token was submitted.
  // Prompt for a token

  // console.log(`User secret: ${secret}`);
  // console.log(`${speakeasy.totp({secret: secret, encoding: 'base32'})}`);
  console.log("This is not a known device");
  const token = speakeasy.totp({secret: secret, encoding: 'base32'});
  const message = `Attempted login from ${username}. Your Zap Surgical login token is: ${token}`;
  console.log(`Attempted login from ${username}`)
  console.log(message);
  twilio.sendMessage(message, number);
  res.json({mfaRequired: true})
}

exports.isAuthorized = (minAuthLevel=99, requiredPermission, requiredRole) => {
    return (
    (req, res, next) => {
      // Flush out how roles work and then modify.
      // Right now it works just like permissions
      const {authLevel, permissions, roles} = req.user;
      const isPermitted = requiredPermission ? _.includes(permissions, requiredPermission) : true
      const isRoleAppropriate = requiredRole ? _.includes(roles, requiredRole) : true

      if ((authLevel <= minAuthLevel) || isPermitted || isRoleAppropriate) {
        next()
      } else {
        res.json({error: true, message: 'You do not have sufficient authorization to access that feature!'})
      }
    }
  )
}


///////////////////////////
// AUTH FUNCTION HELPERS //
///////////////////////////
const modifyDevicesList = (id, fingerprint, remove=false) => {
  User.findById(id)
  .then(user => {
    if (remove) {
      // console.log("remove was invoked");
      // console.log(user.allowedDevices);
      user.allowedDevices = user.allowedDevices.filter(device => (device != fingerprint))
      // console.log(user.allowedDevices);
    } else {
      user.allowedDevices.push(fingerprint);
    }
    user.save()
  })
  .catch(err => console.log(`Unable to find and modify user allowedDevices: ${err}`));
}

const createToken = (user) => {
  const secret = process.env.JWT_SECRET
  const timestamp = new Date().getTime()
  const tokenParams = {
    sub: user.id || user._id,
    iat: timestamp
  }
  return jwt.encode(tokenParams, secret)
}

;exports.process = process;

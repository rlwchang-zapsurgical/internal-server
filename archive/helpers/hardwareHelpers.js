const axios = require('axios');
// const SYSTEM_KEYS = process.env.SYSTEM_KEYS || require('../config/keys');
const SYSTEM_KEYS = undefined;

exports.getHardwareParams = (req, res, id, hardwareType) => {
  axios.get(SYSTEM_KEYS.SYSTEM_URI(id))
    .then(({data}) => res.json(data))
}

exports.getHardwareData = (req, res, id, hardwareType, dataType) => {
  axios.get(`http://192.168.111.60:8081/zsd/twincat/variables?namespace=${dataType}&json=true`)
    .then(({data}) => res.json(data))
    .catch(err => console.log(`Could not get data for ${dataType}: ${err}`))
}

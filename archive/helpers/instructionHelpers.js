const {Instruction} = require('../models');
const {successResponse, errorResponse} = require('./response')

exports.getInstructions = (req, res) => {
    const request = Instruction.find()
    return request
        .then((data) => successResponse(res, data))
        .catch((err) => errorResponse(res, err))
}

exports.showInstructions = (req, res) => {
    const request = Instruction.findById(req.params.id)
    return request
        .then((data) => successResponse(res, data))
        .catch((err) => errorResponse(res, err))
}

exports.editInstructions = (req, res) => {
    const request = Instruction.findByIdAndUpdate(req.params.id, req.body)
    return request
        .then((data) => successResponse(res, data))
        .catch((err) => errorResponse(res, err))
}

exports.createInstructions = (req, res) => {
    const request = Instruction.create(req.body)
    return request
        .then((data) => successResponse(res, data))
        .catch((err) => errorResponse(res, err))
}

exports.removeInstructions = (req, res) => {
    const request = Instruction.findByIdAndRemove(req.params.id)
    return request
        .then((data) => successResponse(res, data))
        .catch((err) => errorResponse(res, err))
}
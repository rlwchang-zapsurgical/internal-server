const _ = require('lodash');
const { Config } = require('../models')
const aws = require('./awsHelpers.js')
const response = require('./response.js')
const mime = require('mime-types')
const document = require('./document')



exports.getConfig = async (req, res) => {
    const configs = await Config.find().lean()

    for(let config of configs) {
      config.docs = await populateDocs(config.docs, config.id)
    }
    
    response.successResponse(res, configs)
}

exports.showConfig = async (req, res) => {
    let config
    const {id} = req.params

    try {
        console.log(
            'i get here just fine'
        )
        config = await Config.findById(id).lean()
    } catch (err) {
        console.log('I can get here too.')
        config = await Config.findOne({id: id}).lean()
    }

    config.docs = await populateDocs(config.docs, config.id)
    console.log('no problems here.')
    response.successResponse(res, config)
};

exports.addConfig = (req, res) => {
    Config.create(req.body)
        .then(config => res.json({
            status: 'success',
            message: 'Successfully added a new config to the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to add new config to DB.',
            error
        }))
}

exports.editConfig = (req, res) => {
    Config.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(config => res.json({
            status: 'success',
            message: 'You have successfuly edited a config entry from the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to edit the corresponding config',
            error
        }))
}

exports.removeConfig = (req, res) => {
    Config.findByIdAndRemove(req.params.id)
        .then(config => res.json({
            status: 'success',
            message: 'You have successfuly removed a config entry from the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to remove the corresponding config',
            error
        }))
}

// ====================
// CONFIG DOCUMENTS
// ====================
const bucket = 'zap-surgical-installers'

async function populateDocs(docs=[], id) {
    const result = []
    for(const doc of docs) {
        const url = await aws.getS3ObjectUrl(bucket, `ConfigDocuments/${id}/${doc.name}`)
        result.push({
            ...doc,
            url
        })
    }
    return result
}

exports.listConfigDocs = async (req, res) => {
    const {id} = req.params
    const {configId} = req.body

    let docsList = await aws.listS3Objects(bucket, `ConfigDocuments/${configId || id}`)

    // This is to handle cases where user might search using the actual _id from mongo
    if (_.isEmpty(docsList)) {
        const associatedConfig = await Config.findOne({id: id})
        
        if (associatedConfig) {
            const associatedId = associatedConfig.id
            docsList = await aws.listS3Objects(bucket, `ConfigDocuments/${configId || id}`)
        }
    }

    let result = []

    for(const doc of docsList) {
      const url = await aws.getS3ObjectUrl(bucket, doc['Key'])
      result.push({
          name: doc["Key"].split('/').slice(-1)[0],
          url
      })
    }

   return response.successResponse(res, result)
}


// This function only gets the signedUrl that can then be used to upload the file.
exports.getConfigDocsUploadLink = async (req, res) => {
    let {id} = req.params
    let {
        configId,
        files=[]
    } = req.body
    let config

    if (!Array.isArray(files)) {
        files = [files]
    }

    try {
        config = await Config.findById(id)
    } catch (err) {
        config = null
    }

    if (config) {
        configId = config.id
    }

    // console.log('id', id, configId)
    // console.log('files', files)
    const results = {}
    
    for (const file of files) {
        // console.log(mime.lookup(file.name))
        if (!_.includes([
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword'
        ], file.type)) {
            return response.errorResponse(res, {err: 'Invalid file type specified.'})
        }
    
        try {
            // console.log('file', file)
            const key = `ConfigDocuments/${configId || id}/${file.name}`
            const url = await aws.getS3ObjectUrl(
                bucket,
                key,
                {action: 'putObject'},
                {
                    ContentType: file.type,
                    ServerSideEncryption: "AES256",        
                }
            )
            console.log('url', url)
            results[file.name] = {
                type: file.type,
                url
            }
        } catch(err) {
            return response.errorResponse(res, {err}, "Failed while fetching one of the pre-signed URLs")
        } 
    }    
    response.successResponse(res, results)
}

// This function adds the doc to the database
exports.addConfigDocsToDatabase = async (req, res) => {
    const {id} = req.params
    const {configId, files=[]} = req.body
    let config

    try {
        config = await Config.findOne({
            $or: [{id}, {id: configId}]
        })
    } catch (err) {
        if (!config) {config = await Config.findById(id)}
    }

    // console.log('config', config.toObject())
    // console.log('files', files)
    filesWithBucketName = _.map(files, file => {
        return {
            ...file,
            bucket
        }
    })
    if (config) {
        config.docs = _.unionWith(config.toObject().docs || [], files, _.isEqual)
        await config.save()
    }

    // let updatedConfig = await Config.findOne({$or: [{id}, {id: configId}]})
    // if (!updatedConfig) {updatedConfig = await Config.findById(id)}

    response.successResponse(res, config)
}
// This function deletes the document from the database and the AWS bucket
// exports.removeConfigDocs = async (req, res) => {
//     // console.log('recieved a delete request')
//     const {id} = req.params
//     // const {configId, file} = req.body
//     const {configId} = req.query
//     const file = JSON.parse(req.query.file)
//     // console.log('received delete request for', id, file.name)
//     // console.log(req.query)
//     try {
//         const result = await aws.deleteS3Object({
//             bucket: bucket,
//             prefix: `ConfigDocuments/${configId || id}`,
//             name: file.name
//         })

//         if (result) {
//             let config
//             try {
//                 config = await Config.findOne({
//                     $or: [{id}, {id: configId}]
//                 })

//                 if (!config) {
//                     config = await Config.findOne({
//                         _id: id
//                     })
//                 }

//                 if (config) {
//                     config.docs = _.filter(
//                         config.toObject().docs,
//                         doc => doc.name != file.name
//                     )
//                     config.save()
//                 }
//             return response.successResponse(res, config, `Successfully deleted ${file.name}`)
//             } catch (err) {
//                 // console.log(err)
//                 throw (err)
//             }
//         }
//         // console.log('The delete request was sent')
//         // console.log('result', result)
//         return response.successResponse(res, result, `Successfully deleted ${file.name}`)
//     } catch (err) {
//         console.log(err)
//         return response.errorResponse(res, err)
//     }
// }

exports.removeConfigDocs = async(req, res) => {
    console.log('query', req.query)
    const {id, _id} = req.query
    const file = JSON.parse(req.query.file)
    console.log('id', _id)
    console.log('_id', _id)
    console.log('config found', await Config.find({
        $or: [{
            id
        }, {
            _id
        }]
    }))
    const result = await document.deleteDocs(file, {
        Model: 'config',
        query: {
            $or: [{
                id
            }, {
                _id
            }]
        },
        property: ['docs'],
        bucket: 'zap-surgical-installers'
    })

    return response.successResponse(res, result)
}


// Config helpers

// collection = [{
//     version: '1.3.4'
// }, {
//     version: '1.112.123'
// }, {
//     version: '1.3.1'
// }, {
//     version: '1.3.11'
// }, {
//     version: '2.4'
// }]
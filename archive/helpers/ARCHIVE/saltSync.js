const moment = require('moment');
const _ = require('lodash');
const minionHelpers = require('./minionHelpers');
const axios = require('axios');
const redis = require('../../packages/redis')
const response = require('../response')

const defaultOptionsForSaltCmd = {
  async: Boolean,
  async_poll: Number, //number in seconds to check
  async_timeout: Number, //number in seconds to stop listening for results. Note that the request will still.
  cacheExp: Number,//number of seconds to expire the cache after,
  clearCache: Boolean, //whether or not to clear the cache of the running cmd
  clearOtherKeys: String, //name of key to clear
}

const saltSuccessResponse = (res, data, names, message) => {
  if (Array.isArray(data)) {
    response.successResponse(res, minionHelpers.formatMinionMultipleData(data, names), message)
  } else {
    response.successResponse(res, minionHelpers.formatMinionData(data))
  }
}

exports.saltSuccessResponse = saltSuccessResponse

function getSaltParams(cmd, tgt='*', arg, options={}) {
  // Allow you to pass in both strings and objects with more details
  // By default, tgt_type if a glob. Possible types include 'grains' and 'compound'
  let target
  if (typeof tgt == 'string') {
    target = {tgt}
  } else {
    target = tgt
  }

  let client = options.client || 'local'
  if (options.async && !client.endsWith('async')) {
    client += '_async'
  }

  return {
    username: process.env.SALT_USERNAME,
    password: process.env.SALT_PASSWORD,
    eauth: process.env.SALT_EAUTH,
    client: client,
    tgt: target.tgt,
    tgt_type: target.tgt_type,
    arg,
    fun: cmd
  }
}

// These are the following links that we can send commands to.
// `${process.env.SALT_MASTER_URL}/` - Send all commands here. You need an auth cookie or token though.
// `${process.env.SALT_MASTER_URL}/login` - Login here to retrieve a cookie
// `${process.env.SALT_MASTER_URL}/run` - Send commands here if you want to include login credentials directly in body
// `${process.env.SALT_MASTER_URL}/jobs` - Send get jobs here to get hte status of a jbob
async function saltCmd(cmd, tgt, arg, options={}) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }
  const {
    cacheExp,
    clearCache,
    clearOtherKeys,
    async: isAsync,
    async_timeout
  } = options
  const params = getSaltParams(cmd, tgt, arg, options)
  // const params = getSaltParams(...arguments)
  const cacheKey = JSON.stringify(_.pick(params, ['client', 'tgt', 'arg', 'fun']))
  
  let result
  let jobId //These are for async jobs
  let startTime = Date.now() //These are for async jobs

  if (process.env.REDIS) {
    result = await checkCache() //Checks cache for existing data and clears additional data if specified in the options
    jobId = result && result.return[0].jid
    console.log('result1', result)
    if (result && result.jobId) {
      try {
        result = await checkJob()
      } catch (error) {
        console.log(error)
        throw new Error(error)
      }
    }
    console.log('result2', result)

    // If the cache does not have a result, send a direct saltCmd
    result = await axios.post(`${process.env.SALT_MASTER_URL}/run`, params)
    console.log('result3', result)
    jobId = result.data && result.data.return[0].jid
    await cacheResult(isAsync)
    
    if (result && jobId) {
      result = await checkJob()
    }
  }   
  
  console.log('result4', result)
  console.log('result.data', result.data)
    
  return await result

  async function checkCache() {
    // If the salt command should have a side effect and clear a different cache entry
    // clear it here
    if (clearOtherKeys) {
      if (Array.isArray(clearOtherKeys)) {
        _.forEach(clearOtherKeys, (key) => redis.del(key))
      } else {
        redis.del(clearOtherKeys)
      }
    }
    // If there is a prompt to clear the cache
    // Clear the cache and fetch new results
    // Otherwise search the cache
      // If the search had a system id
        // return the results for that id if it includes the control, operator, and database
        // if one of the three was missing, it was a botched query and needs to be redone, so don't cache
      // If the search had no system id (so a global search) return the result
        // This one needs better error handling to account for botched queries, but this is the best I can do for now
      // If there are no results, move on.
    if (clearCache) {
      redis.del(cacheKey)
    } else {
      const data = await redis.get(cacheKey)
      return data && JSON.parse(data)
    }
  }

  async function cacheResult(isAsyncCmd=false) {
    let expiration = isAsyncCmd ? async_timeout : cacheExp
    let cacheValue
    
    if (isAsyncCmd) {
      cacheValue = { startTime, jobId: result.jobId }
    } else {
      cacheValue = { data: result.data }
    }

    try {
        return await redis.set(cacheKey, JSON.stringify(cacheValue), 'EX', expiration)
      } catch (error) {
        console.log(error)
      }
  }

  async function checkJob() {
    const result = await axios.get(`${process.env.SALT_MASTER_URL}/jobs/${jobId}`)
    const jobData = result.data.info[0]
    // If succeess
    if (!_.isEmpty(jobData.Result) && jobData.Target == 'unknown-target') { // Checks to see if the job has finished. If true, it definitely finished. If false, it is pending or failed
      return result
    }

    // If failed
    if ((Date.now() - startTime) > async_timeout) { // Fail due to timeout. No other way to check for failure
      const timeoutError = new Error(`Failed to complete the job in the allocated time of ${async_timeout} seconds. The remote request may still be running, but the server is no longer listening for the results.`)
      timeoutError.name = "TimeoutError"
      throw timeoutError
    }

    // If pending
    return ({
      status: 'pending',
      startTime,
      jobId
    })
  }
}
exports.saltCmd = saltCmd


// If it is a long standing process, return the start time and status of pending
// Get the jid of the job
// Periodically check the job to see if it has completed
// If it has completed, store the result in the cache for the given job id
// Alternatively, check to see if there is a job id. If there is, redirect queries to that one
// async function handleAsyncCmd(params, options) {
//   const {async, async_poll=(60), async_timeout=(30 * 60)} = options
//   const startTime = Date.now()
//   let result = await axios.post(`${process.env.SALT_MASTER_URL}/run`, params)
//   if (!async) {
//     return request
//   }

//   const jobId = result.data.return[0].jid
//   let jobIsRunning = true
//   let polling = setInterval(() => {

//   }, async_poll * 1000)

//   if (!jobIsRunning) {
    
//   } else {
//     clearInterval(polling)
//     return request
//   }


//   const jobRequest = new Promise((resolve, reject) => {
//     if (Date.now() - startTime > async_timeout * 1000) {
//       reject(`Failed to complete the job in the allocated time of ${async_timeout} seconds. The remote request may still be running, but the server is no longer listening for the results.`)
//     } else {

//     }
//   })
// }

// async function handleAsyncCmd(params, options) {
//   const {async, async_poll=(60), async_timeout=(30 * 60)} = options
//   const startTime = new Date()
//   let result = await axios.post(`${process.env.SALT_MASTER_URL}/run`, params)
  
//   if (!async) {
//     return request
//   }

//   const jobId = result.data.return[0].jid
//   const cacheValue = {
//     startTime,
//     jobId
//   }

//   redis.set(cacheKey, JSON.stringify(cacheValue), 'EX', async_timeout)

//   const jobRequest = new Promise((resolve, reject) => {
//     if (Date.now() - startTime > async_timeout * 1000) {
//       reject()
//     } else {

//     }
//   })
// }
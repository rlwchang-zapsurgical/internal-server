const _ = require('lodash');
/////////////////////////////
// Helper minion functions //
/////////////////////////////
const getPcType = (name) => {
  if (name.toLowerCase().match(/broker|database/)) {
    return 'database'
  } else if (name.toLowerCase().match(/operator|console/)) {
    return 'operator'
  } else if (name.toLowerCase().match(/system|control/)) {
    return 'control'
  } else {
    return null
  }
}
//
// const processSaltDataSingle = data => {
//   data.return[0][systemName]
// }
const formatMinionData = ({data: {return: raw_data}}, config={methods: []}) => {
  // Methods is currently not implemented. See below for intended usage
  const data = raw_data[0]
  const systemMinions = _.pickBy(data, (value, key) => key.includes('sys'))

    let result = {}
    _.forEach(systemMinions, (value, key) => {
      const id = key.match(/\d+/)[0] || 'no-id'
      const type = getPcType(key)
      // const connectionStatus = value

      result[id] = result[id] || {}
      result[id][type] = value
    })

  return result
  // Methods is an array of ways to format the data.
  // E.g. methods=['pcType', 'systemId']
  // const {methods} = config;


  // const dataObj = data["return"][0];
  // const result = {};

  // for (let minion in dataObj) {
  //   const minionData = dataObj[minion]
  //   const nameArr = minion.split("_");
  //   const systemId = nameArr[1];
  //   const pcType = getPcType(nameArr[3]);

  //   if (_.includes(methods, 'pcType') && _.includes(methods, 'systemId')) {
  //     result[systemId] = result[systemId] || {};
  //     result[systemId][pcType] = minionData
  //   } else if (_.includes(methods, 'pcType')) {
  //     result[pcType] = minionData;
  //   } else if (_.includes(methods, 'systemId')) {
  //     result[systemId] = minionData;
  //   }
  // }
}

const formatMinionMultipleData = (dataArray, names) => {
  if (!names || names.length < 1) {console.warn('formatMinionMultipleData requires a name array equal to the length of the different kinds of data')}
  let result = {}

  _.forEach(dataArray, ({data: raw_data}, index) => {
    // // console.log('return',raw_data.return)
    const data = raw_data.return[0]
    const name = names[index]
    
    const systemMinions = _.pickBy(data, (value, key) => key.includes('sys'))
  
    _.forEach(systemMinions, (value, key) => {
      const id = key.match(/\d+/)[0] || 'no-id'
      const type = getPcType(key)
      // const connectionStatus = value
  
      result[id] = result[id] || {}
      result[id][type] = result[id][type] || {}
      result[id][type][name] = value
    })
    
  })
  return result
}

module.exports = {
  getPcType,
  formatMinionData,
  formatMinionMultipleData
}

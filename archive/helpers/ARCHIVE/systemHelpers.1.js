const DB = require('../models');
const moment = require('moment');
const axios = require('axios');
const _ = require('lodash');

const minionHelpers = require('./minionHelpers');
const response = require('./response')

// const saltMasterUrl = process.env.NODE_ENV == "production" ? "http://172.31.9.88:8080/run" : 'https://entsm1.zapsurgical.com/run';

// const testLogin = {
//   username: 'zapsurgical',
//   password: 'zap123',
//   eauth: 'auto'
// }

// const pingCmd = {
// 	client: "local",
// 	tgt: "*minion*",
// 	fun: "test.ping"
// }

// const processCmd = {
// 	"client": "local",
// 	"tgt": "*",
// 	"fun": "sys.list_modules"
// }

const saltSuccessResponse = (res, data) => response.successResponse(res, minionHelpers.formatMinionData(data))

function getSaltParams(cmd, tgt='*') {
  return {
    username: process.env.SALT_USERNAME,
    password: process.env.SALT_PASSWORD,
    eauth: process.env.SALT_EAUTH,
    client: "local",
    tgt,
    fun: cmd
  }
}
function saltCmd(cmd, tgt) {
  const request = axios.post(process.env.SALT_MASTER_URL, getSaltParams(cmd, tgt))

  return request
}

// exports.getSystems = (req, res) => (
//   DB.systems.find()
//   .then(res.json(systems))
//   .catch(console.log(err))
// )


// exports.loginSystems = (req, res) => {
  // const request = axios.post('https://entsm1.zapsurgical.com/login', testLogin);
  // console.log(request)
  // request.then(
  //   ({data}) => {
  //     res.cookie('session_id', data.return[0].token)
  //     res.json(data);
  //   }
  // )
  //   .catch(err => console.log(`An error occured: ${err}`))
// }

// exports.pingSystems = (req, res) => {
//   // Remove login step after testing
//  const request = axios.post('https://entsm1.zapsurgical.com', pingCmd)
//  request.then(({data}) => res.json(data))
//  .catch(err => console.log(`An error occured: ${err}`))
// }

exports.pingSystems = (req, res) => {
  // Remove login step after testing
//  const request = axios.post(saltMasterUrl , {...pingCmd, ...testLogin})
//  request.then(data => {

//     //  const systemObj = data.return[0];
//     //  const statusObj = {};

//     //  for (let system in systemObj) {
//     //    // if (!system.includes('minion')) {continue};
//     //    // console.log("another iteration");
//     //    const onlineStatus = systemObj[system]
//     //    const nameArr = system.split("_");
//     //    const systemId = nameArr[1];
//     //    const pcType = minionHelpers.getPcType(nameArr[3]);
//     //    statusObj[systemId] = statusObj[systemId] || {} ;
//     //    statusObj[systemId][pcType] = onlineStatus
//     //  }
//     //  res.json(statusObj)
//     const result = minionHelpers.formatMinionData(data)
//     console.log(result);
//     res.json(result)
//    }
//  )
//  .catch(err => console.log(`An error occured: ${err}`))

  const request = saltCmd('test.ping')
  request.then(data => saltSuccessResponse(res, data))
  .catch(err => response.errorResponse(res, err))
}

exports.getSystemInfo = (req, res) => {
  const {id} = req.params
  const tgt = id ? `*_${id}_*` : '*'
  const request = saltCmd('system.get_system_info', tgt)
  request
  .then(data => saltSuccessResponse(res, data))
  .catch(err => response.errorResponse(res, err))
}

// exports.systemInfo = (req, res) => {
//   const request = axios.post(saltMasterUrl , {...processCmd, ...testLogin})
//   request.then(({data}) => res.json(data))
//   .catch(err => console.log(`An error occured: ${err}`))
// }

exports.getSystems = (req, res) => {
  DB.System.find()
  .then(systems => res.json(systems))
  .catch(err => console.log(err))
}


exports.showSystem = (req, res) => {
  const {id} = req.params;
  DB.System.findOne({systemId: id})
  .then(system => res.json(system))
  .catch(err => console.log(err))
}

exports.createSystem = (req, res) => {
  const systemBody = req.body;
  console.log(systemBody);
  DB.System.create(systemBody)
  .then(log => res.json(log))
  .catch(err => console.log(err))
}

exports.editSystem = (req, res) => {
  const {id} = req.params;
  const systemBody = req.body;
  console.log(systemBody);
  DB.System.findOneAndUpdate(id, systemBody)
  .then(log => res.json(log))
  .catch(err => console.log(err))
}

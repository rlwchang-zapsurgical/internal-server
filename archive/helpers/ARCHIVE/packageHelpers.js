const axios = require('axios');

const minionHelpers = require('./minionHelpers');

const saltMasterUrl = process.env.NODE_ENV == "production" ? "http://172.31.9.88:8080/run" : 'https://entsm1.zapsurgical.com/run';
const saltAuth = {
  username: 'zapsurgical',
  password: 'zap123',
  eauth: 'auto'
}

const saltConfig = {
  client: 'local',
  ...saltAuth
}

exports.getPackages = (req, res) => {
  console.log('GET Packages activated');
  const { systemId } = req.params;
  const pcType = req.params.pcType ? req.params.pcType.toLowerCase() : null;
  // const { systemId, pcType } = req.params;
  const getParams = {
    ...saltConfig,
    tgt: pcType ? `minion_${systemId}_*${pcType}*` : `minion_${systemId}_*`,
    fun: 'pkg.list_pkgs',
  }

  const request = axios.post(saltMasterUrl, getParams)
  const formatConfig = {methods: ['pcType']};
  request
    .then(({data}) => {
      if (pcType) {
        res.json(minionHelpers.formatMinionData(data, formatConfig)[pcType])
      } else {
        res.json(minionHelpers.formatMinionData(data, formatConfig))
      }
    })
    .catch(err => {console.log(err); return res.json({err})})
    // .then(({data}) => {
    //   const minionObj = data["return"][0];
    //   // const minionName = minionObj.keys()[0]
    //   // console.log(minionObj)
    //   // res.json(minionObj)
    //   res.json(Object.values(minionObj)[0] || {})
    // })
    console.log(request);
}

exports.modifyPackage = (req, res) => {
  console.log('modifyPackage invoked');
  const { systemId, pcType } = req.params
  const { action, packageName } = req.body

  const postParams = {
    ...saltConfig,
    tgt: `minion_${systemId}_*${pcType.toLowerCase()}*`,
    fun: `pkg.${action}`, //install, remove
    arg: packageName
  }

  console.log(postParams)

  const request = axios.post(saltMasterUrl, postParams)
  request
    .then(({data}) => {
      const minionObj = data["return"][0];
      // res.json(minionObj)
      res.json(Object.values(minionObj)[0] || {})
    })
    .catch(err => res.json({err}))
}

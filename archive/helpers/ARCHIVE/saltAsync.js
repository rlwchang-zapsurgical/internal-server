// Unlike the salt.js file, this file is built with an async first approach.
const moment = require('moment');
const _ = require('lodash');
const minionHelpers = require('./minionHelpers');
const axios = require('axios');
const redis = require('../../packages/redis')
const response = require('../response')

const defaultOptionsForSaltCmd = {
  async: Boolean,
  async_poll: Number, //number in seconds to check
  asyncTimeOut: Number, //number in seconds to stop listening for results. Note that the request will still.
  cacheExp: Number,//number of seconds to expire the cache after,
  clearCache: Boolean, //whether or not to clear the cache of the running cmd
  clearOtherKeys: String, //name of key to clear
}

const saltSuccessResponse = (res, data, names, message) => {
  if (Array.isArray(data)) {
    response.successResponse(res, minionHelpers.formatMinionMultipleData(data, names), message)
  } else {
    response.successResponse(res, minionHelpers.formatMinionData(data))
  }
}

exports.saltSuccessResponse = saltSuccessResponse

function getSaltParams(cmd, tgt='*', arg, options={}) {
  // Allow you to pass in both strings and objects with more details
  // By default, tgt_type is a glob. Possible types include 'grains' and 'compound'
  let target = typeof tgt == 'string' ? {tgt} : tgt
  let client = options.client || 'local_async'

  const params = {
    username: process.env.SALT_USERNAME,
    password: process.env.SALT_PASSWORD,
    eauth: process.env.SALT_EAUTH,
    client: client,
    tgt: target.tgt,
    tgt_type: target.tgt_type,
    arg,
    fun: cmd
  }

  return params
}

async function authenticatedSaltRequest(url, data, options={}) {
  const token = await saltAuth()
  const method = options.method || (data ? 'post' : 'get')
  try {
    const result = (await axios.request({
      url,
      method,
      headers: {
        'X-Auth-Token': token
      },
      data
    })).data
    return result
  } catch(err) {
    console.log('Salt request failed:', err.message)
  }
}

// async function saltAuthMiddleware(req, res, next) {
//   const token = await saltAuth()
//   console.log(token)
//   if (token) {
//     req.headers['X-Auth-Token'] = token
//     console.log(req.headers)
//     next()
//   } else {
//     response.unauthorizedResponse(res)
//   }
// }

async function saltAuth(authOptions={}) {
  const cachedToken = await redis.get('salt-auth-token')
  // console.log('cachedtoken', cachedToken)
  if (cachedToken) return cachedToken

  const {username, password, eauth} = authOptions
  const result = (await axios.post(`${process.env.SALT_MASTER_URL}/login`, {
    username: process.env.SALT_USERNAME || username,
    password: process.env.SALT_PASSWORD || password,
    eauth: eauth || 'auto'
  })).data
  const perms = result.return[0].perms
  const token = result.return[0].token
  // console.log(token)
  // console.log(perms)
  if (_.isEmpty(perms)) {
    return null
  } else {
    redis.set('salt-auth-token', token, 'EX', 60 * 60 * 6) //This time is determined by what the cherryPy server currently allows. If you want to change this, change both of them.
    return token
  }
}

async function saltCmd(cmd, tgt, arg, options={}, res) {
  
  const jid = await executeSaltCmd(...arguments)
  // let result = await getSaltJobResults(jid, options, res)
  const result = await pollSaltJobResults(jid, options, res)
  // console.log('result from saltCmd', result)
  return result
}

function pollSaltJobResults(jid, options, res) {
  const {
    poll_result = 1,
    asyncTimeOut = 15 * 60
  } = options
  const poll_result_ms = poll_result * 1000
  return new Promise((resolve, reject) => {
    let polling = setInterval(() => {
      const checkJobRequest = getSaltJobResults(jid, options, res)
      checkJobRequest
        .then(jobResult => {
          if (jobResult.error) {
            clearInterval(polling)
            reject(err)
          } else if (jobResult.status == 'pending') {
            console.log('Job is still pending', jobResult)
          } else {
            console.log('Job finished:', jobResult)
            clearInterval(polling)
            resolve(jobResult)
          }
      })
        .catch((err) => {
          clearInterval(polling)
          reject(err)
        })
    }, poll_result_ms)
  })
}

// This function executes the salt command and returns the jobId(jid)
async function executeSaltCmd(cmd, tgt, arg, options={}) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }
  const params = getSaltParams(cmd, tgt, arg, options)
  const cacheKey = JSON.stringify(_.pick(params, ['client', 'tgt', 'arg', 'fun']))

  const {
    cacheExp = 0,
    clearCache,
    clearOtherKeys,
    sync,
    asyncTimeOut
  } = options
  
  
  let jid = await checkCache(cacheKey)
  if (!jid || jid == 'undefined' || jid == 'null') { // While developing, sometimes these values get stringified and sneak in.
    try {
      const jidRequest = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/run`, params)
      jid = jidRequest.return[0].jid
      if (cacheExp) {redis.set(cacheKey, jid, 'EX', cacheExp)}
    } catch(err) {
      console.log(err)
    }
  }
  // request.then((data) => console.log('data', data)).catch((err) => console.log(err))
  // const jid = result.data.return[0].jid
  // Think more about this part for what you should cache and for how long.
  return jid
}

async function getSaltJobResults(jid, options={}, res) {
  // let result = await checkCache(`salt-jid-${jid}`) 
  // if (result) {return result}
  const {asyncTimeOut = 60, cacheExp} = options
  const job = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/jobs/${jid}`)
  console.log('job', job)
  const info = job.info[0]
  const result = job.return[0]
  const startTime = new Date(info.StartTime + ' GMT+0').getTime()
  // console.log('start time: ', info.StartTime)
  // console.log('start time: ', startTime)
  // console.log('current time', Date.now())
  // console.log('remianing time', Math.floor((asyncTimeOut * 1000 - (Date.now() - startTime))/1000))
  // The job was complete
  if (!_.isEmpty(result) && info.Target == "unknown-target") {
    console.log('Job is complete')
    // redis.set(`salt-jid-${jid}`, result, 'EX', cacheExp)
    return res ? saltSuccessResponse(res, result) : result
  }
  // The job took too long and timed out
  if (Date.now() - startTime > asyncTimeOut * 1000) {
    console.log('it expired basically')
    const timeoutError = new Error('Action failed to complete in the designated time.')
    const error = {
      error: timeoutError,
      message: `Failed to complete in ${asyncTimeOut} seconds` 
    }
    
    return res ? 
      response.errorResponse(res, error.error, error.message) :
      error
  }
  
  console.log('job is still pending')
  // The job is still pending
  return res ? response.pendingResponse(res, info) : {status: 'pending', timeOut: startTime + asyncTimeOut * 1000}
}

async function checkCache(cacheKey, options={}) {
  let {
    clearCache = false,
    clearOtherKeys = [],
  } = options
  
  if (!Array.isArray(clearOtherKeys)) { clearOtherKeys = [clearOtherKeys] }
  _.forEach(clearOtherKeys, (key) => redis.del(key))
  
  if (clearCache) { redis.del(cacheKey) }

  const data = await redis.get(cacheKey)
  return data // This will be null, a jid, or the result object
}

module.exports = {
  saltCmd,
  executeSaltCmd,
  getSaltJobResults,
  saltAuth,
  authenticatedSaltRequest
}
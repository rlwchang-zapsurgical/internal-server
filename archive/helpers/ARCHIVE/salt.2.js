// Unlike the salt.js file, this file is built with an async first approach.
const moment = require('moment');
const _ = require('lodash');
const minionHelpers = require('./minionHelpers');
const axios = require('axios');
const redis = require('../../packages/redis')
const response = require('../response')

const defaultOptionsForSaltCmd = {
  async: Boolean,
  pollingInterval: Number, //number in seconds to check
  timeOut: Number, //number in seconds to stop listening for results. Note that the request will still.
  cacheExp: Number,//number of seconds to expire the cache after,
  clearCache: Boolean, //whether or not to clear the cache of the running cmd
  clearOtherKeys: String, //name of key to clear
}

const saltSuccessResponse = (res, data, names, message) => {
  // console.log('running salt success reponse')
  // console.log('data from successreponse', data)
  
  if (Array.isArray(data)) {
    response.successResponse(res, minionHelpers.formatMinionMultipleData(data.return[0], names), message)
  } else {
    response.successResponse(res, minionHelpers.formatMinionData(data.return[0]))
  }
}

function getSaltParams(cmd, tgt='*', arg, options={}) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }
  // Allow you to pass in both strings and objects with more details
  // By default, tgt_type is a glob. Possible types include 'grains' and 'compound'
  let target = typeof tgt == 'string' ? {tgt} : tgt
  let client = options.client || (options.isAsync ? 'local_async' : 'local')

  const params = {
    username: process.env.SALT_USERNAME,
    password: process.env.SALT_PASSWORD,
    eauth: process.env.SALT_EAUTH,
    client: client,
    tgt: target.tgt,
    tgt_type: target.tgt_type,
    arg,
    kwarg: {
      pillar: options.pillar,
      test: options.test
    },
    fun: cmd
  }

  return params
}

async function authenticatedSaltRequest(url, data, options={}) {
  const token = await saltAuth()
  const method = options.method || (data ? 'post' : 'get')
  try {
    const result = (await axios.request({
      url,
      method,
      headers: {
        'X-Auth-Token': token
      },
      data
    })).data
    return result
  } catch(err) {
    console.log('Salt request failed:', err.message)
  }
}

// async function saltAuthMiddleware(req, res, next) {
//   const token = await saltAuth()
//   console.log(token)
//   if (token) {
//     req.headers['X-Auth-Token'] = token
//     console.log(req.headers)
//     next()
//   } else {
//     response.unauthorizedResponse(res)
//   }
// }

async function saltAuth(authOptions={}) {
  const cachedToken = await redis.get('salt-auth-token')
  // console.log('cachedtoken', cachedToken)
  if (cachedToken) return cachedToken

  const {username, password, eauth} = authOptions
  const result = (await axios.post(`${process.env.SALT_MASTER_URL}/login`, {
    username: process.env.SALT_USERNAME || username,
    password: process.env.SALT_PASSWORD || password,
    eauth: eauth || 'auto'
  })).data
  const perms = result.return[0].perms
  const token = result.return[0].token
  // console.log(token)
  // console.log(perms)
  if (_.isEmpty(perms)) {
    return null
  } else {
    redis.set('salt-auth-token', token, 'EX', 60 * 60 * 6) //This time is determined by what the cherryPy server currently allows. If you want to change this, change both of them.
    return token
  }
}

async function saltCmd(cmd, tgt, arg, options={}, res) {
  if (!Array.isArray(arg) && typeof arg == 'object') {
    options = arg
    arg = undefined
  }

  const {
    cacheExp,
    clearCache
  } = options
  // console.log('cacheExp', cacheExp)
  const params = getSaltParams(cmd, tgt, arg, options)
  const cacheKey = JSON.stringify(_.pick(params, ['client', 'tgt', 'arg', 'fun']))
  let result = options.clearCache ? null : await checkCache(cacheKey)
  // console.log('result from saltcmd', result)
  if (!result) { 
    result = await executeSaltCmd(cmd, tgt, arg, options, params)
    if (options.cacheExp) { redis.set(cacheKey, JSON.stringify(result), 'EX', cacheExp) }
   }
  return result
}

// function pollSaltJobResults(jid, options, res) {
//   const {
//     poll_result = 1,
//     timeOut = 15 * 60
//   } = options
//   const poll_result_ms = poll_result * 1000
//   return new Promise((resolve, reject) => {
//     let polling = setInterval(() => {
//       const checkJobRequest = getSaltJobResults(jid, options, res)
//       checkJobRequest
//         .then(jobResult => {
//           if (jobResult.error) {
//             clearInterval(polling)
//             reject(err)
//           } else if (jobResult.status == 'pending') {
//             console.log('Job is still pending', jobResult)
//           } else {
//             console.log('Job finished:', jobResult)
//             clearInterval(polling)
//             resolve(jobResult)
//           }
//       })
//         .catch((err) => {
//           clearInterval(polling)
//           reject(err)
//         })
//     }, poll_result_ms)
//   })
// }
async function pollSaltJobResults(jid, handler, options={}) {
  handler = handler || ((jobResult, done) => {
    console.log('jobResult', jobResult)
    console.log('resolving...')
    done(jobResult)
  })
  
  const results = await pollingPromise(jid, handler, options)
  return results
}

function pollingPromise(jid, handler, options) {
  // handler is a user-defined function.
  // it is passed the job results, as well as a done function.
  // The user should call the done function when they want the job check to be considered done
  // This will cause the endPoll function to run with whatever result the user specified in done()
  // done() is an arbitrary name and can be called anything,
  // but it should be a function that takes in one param, the result
  // e.g.
  // function handler(result, done) {
  // do stuff here...
  // like processing result and storing it in someResult
  // then use done() with the result you want to pass back.
  // done(someResult)
  // }
  const {timeOut=(1000 * 60 * 10), timeInterval=(1500)} = options
  const stopTime = Date.now() + timeOut
  let pollInterval

  return new Promise((resolve, reject) => {
    pollInterval = setInterval(async () => {
      console.log('running the polling for jid', jid)
      const jobResult = await getSaltJobResults(jid)
      handler(jobResult, (error, result) => endPoll(error, result, resolve, reject))

      if (Date.now() > stopTime) {
        reject('Job failed to complete in time. If this is a longer job, please specify a different timeout duration')
        clearInterval(pollInterval)
      }
    }, timeInterval)
  })

  function endPoll(error, result, resolve, reject) {
    clearInterval(pollInterval)
    error ? reject(error) : resolve(result)
  }
}

// pollSaltJobResults('20180928165804354171')

// This function executes the salt command and returns the jobId(jid)
async function executeSaltCmd(cmd, tgt, arg, options={}, saltParams) {
  const params = saltParams || getSaltParams(cmd, tgt, arg, options)
  // console.log('params', params)

  if (options.isAsync) {
    // If they specify to run the command asynchronously, it will return a job id or jid.
    console.log('running asynchronouslys')
    // if (!jid || jid == 'undefined' || jid == 'null') { // While developing, sometimes these values get stringified and sneak in.
      try {
        const jidRequest = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/run`, params)
        const jid = jidRequest.return[0].jid
        return jid
      } catch(err) {
        console.log(err)
      }
    // }
  } else {
    // This is the synchronous version which is the default. It will return the actual results instead of a job id.
    try {
      let result = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/run`, params)
      return result
    } catch (err) {
      console.log('error from executeSaltCmd', err)
    }
  }

}

async function applyStates(states=[], tgt, options={}, saltParams) {
  const stateString = states.join(",")
  const result = await executeSaltCmd(
    'state.apply',
    tgt,
    stateString,
    options,
    saltParams
    )

  return result
}

async function getSaltJobResults(jid, options={}, res) {
  const {timeOut = 60, cacheExp} = options
  const job = await authenticatedSaltRequest(`${process.env.SALT_MASTER_URL}/jobs/${jid}`)
  return res ? response.successResponse(res,job) : job
}

async function checkCache(cacheKey, options={}) {
  let {
    clearCache = false,
    clearOtherKeys = [],
  } = options
  
  if (!Array.isArray(clearOtherKeys)) { clearOtherKeys = [clearOtherKeys] }
  _.forEach(clearOtherKeys, (key) => redis.del(key))
  
  if (clearCache) { redis.del(cacheKey) }
  // console.log('cacheKey', cacheKey)
  const data = await redis.get(cacheKey)
  // console.log('data from cache', data)
  // console.log('typeof data from cache', typeof data)
  try {
    const parsedData = JSON.parse(data)
    return parsedData
  } catch (err) {
    console.log(err)
    return data // This will be null, a jid, or the result object
  }
}

module.exports = {
  saltCmd,
  executeSaltCmd,
  applyStates,
  getSaltJobResults,
  saltAuth,
  authenticatedSaltRequest,
  saltSuccessResponse,
  pollSaltJobResults
}
const axios = require('axios');

// try {
//   const { URI, SALT } = require('../config/keys');
//   const saltMasterUrl = URI.saltMasterUrl
//   const saltAuth = SALT.auth
// }
// catch(e) {
//   console.log('Failed to fetch local keys: ' + e);
//   const { SALT_MASTER_URL, SALT_AUTH } = process.env;
//   const saltMasterUrl = SALT_MASTER_URL;
//   const saltAuth = SALT_AUTH
// }
const saltMasterUrl = process.env.NODE_ENV == "production" ? "http://172.31.9.88:8080/run" : 'https://entsm1.zapsurgical.com/run';

const saltAuth = {
  username: 'zapsurgical',
  password: 'zap123',
  eauth: 'auto'
}

const saltConfig = {
  client: 'local',
  ...saltAuth
}

exports.getServices = (req, res) => {
  const { systemId, pcType } = req.params;
  const getParams = {
    ...saltConfig,
    tgt: `minion_${systemId}_*${pcType.toLowerCase()}*`,
    fun: 'service.status',
    arg: ['*'],
  }

  const request = axios.post(saltMasterUrl, getParams)
  request
    .then(({data}) => {
      const minionObj = data["return"][0];
      // const minionName = minionObj.keys()[0]
      // console.log(minionObj)
      // res.json(minionObj)
      res.json(Object.values(minionObj)[0] || {})
    })
    .catch(err => res.json({err}))
}

exports.modifyService = (req, res) => {
  const { systemId, pcType } = req.params
  const { action, serviceName } = req.body

  const postParams = {
    ...saltConfig,
    tgt: `minion_${systemId}_*${pcType.toLowerCase()}*`,
    fun: `service.${action}`, //start, stop, restart
    arg: serviceName
  }

  const request = axios.post(saltMasterUrl, postParams)
  request
    .then(({data}) => {
      const minionObj = data["return"][0];
      // res.json(minionObj)
      res.json(Object.values(minionObj)[0] || {})
    })
    .catch(err => res.json({err}))
}

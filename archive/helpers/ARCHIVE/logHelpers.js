const DB = require('../models');
const moment = require('moment');
const fs = require('fs');
// const AWS = require('aws-sdk');
const _ = require('lodash');
const axios = require('axios');

const aws = require('./awsHelpers');

////////////////
// AWS CONFIG //
////////////////

// let AWS_KEYS = process.env;
//
// try {
//   AWS_KEYS = require('../config/keys').AWS_KEYS
//   // console.log(AWS_KEYS);
// }
// catch(e) {
//   console.log(e);
// }
//
// const {AWS_ACCESS_KEY, AWS_SECRET_KEY} = AWS_KEYS
//
// AWS.config.setPromisesDependency(null);
// const awsParams = {
//   accessKeyId: AWS_ACCESS_KEY,
//   secretAccessKey: AWS_SECRET_KEY,
//   apiVersion: 'latest',
//   };
// const s3Params = {
//   Bucket: "zap-surgical-system-logs",
//   // ACL: 'public-read'
// }
// const s3 = new AWS.S3(awsParams);
const bucket = 'zap-surgical-system-logs';


/////////////////
// SALT CONFIG //
/////////////////

const saltMasterUrl = process.env.NODE_ENV == "production" ? "http://172.31.9.88:8080/run" : 'https://entsm1.zapsurgical.com/run'

const testLogin = {
  username: 'zapsurgical',
  password: 'zap123',
  eauth: 'auto'
}

/////////////////////////
// Start log functions //
/////////////////////////
/////////////////////////
exports.getLogs = async function(req, res) {
  // const date = req.params.date || '';
  // const systemId = req.params.systemId;
  const {date, systemId, pcType} = req.params
  const typeMap = {
    broker: 'BrokerLogs',
    console: 'ConsoleLogs',
    system: 'SystemLogs'
  }
  // const getParams = {
  //   // ...s3Params,
  //   // MaxKeys: 50,
  //   Prefix: `${systemId}/${typeMap[pcType.toLowerCase()]}/`,
  // }
  const prefix = `${systemId}/${typeMap[pcType.toLowerCase()]}/`

  // s3.requestAllLogs(getParams).then(logs => res.json(logs.map(log => log["Key"]))).catch(err => console.log(`An error occured during the fetch: ${err}`))
  const results = await aws.listS3Objects(bucket,prefix)
  const logs = results.map(logObj => logObj["Key"]);
  res.json(logs);
}


// const requestAllLogs = (options, continuationToken, logs=[]) => {
//   const requestOptions = continuationToken ? {...options, ContinuationToken: continuationToken} : options
//
//   // console.log("request all logs invoked")
//   return s3.listObjectsV2(requestOptions).promise()
//   .then(data => {
//     if (data["IsTruncated"] && data["NextContinuationToken"]) {
//       return requestAllLogs(options, data["NextContinuationToken"], [...logs, ...data["Contents"]])
//     } else {
//       return [...logs, ...data["Contents"]]
//     }
//   })
//   .catch(err => console.log(`An error occured with fetching logs: ${err}`))
//
// }



exports.showLog = (req, res) => {
  const fullFilename = decodeURIComponent(req.params.filename);
  const showParams = {
    // ...s3Params,
    Bucket: bucket,
    Key: fullFilename
  }

  let logContents = ''
  const logReadStream = aws.s3.getObject(showParams).createReadStream();

  logReadStream.on('data', data => logContents += data.toString());
  logReadStream.on('end', () => res.json(logContents))

  // s3.getObject(showParams).promise()
  //   .then(data => res.json(data));

  // res.json(showParams)
}
//
// exports.createLog = (req, res) => {
//   const dateCreated = new Date();
//   const systemId = req.body.systemId || req.params.id;
//   const logName = `${systemId}_${moment(dateCreated).format('X')}.txt`
//   const logMessage = req.body.message;
//
//
//   const logBody = {
//     dateCreated,
//     systemId,
//     locationUri: `https://s3-us-west-1.amazonaws.com/zap-surgical-test/${logName}`
//   }
//
//   const createParams = {
//     ...s3Params,
//     Key: logName,
//     Body: logMessage,
//   }
//
//
//   function findSystemAndAddLog(data) {
//     DB.System.findOne({systemId})
//       .then(system => {
//         system.logs.push({...logBody, metaData: JSON.stringify(data)});
//         system.save()
//           .then(system => res.json(system))
//           .catch(err => console.log("MongoDB log write failed" + err))
//       })
//       .catch(err => console.log("MongoDB system-log write failed" + err))
//   }
//
//   s3.putObject(createParams).promise()
//     .then(data => findSystemAndAddLog(data))
//     .catch(err => res.json({err}))
//
// }
//
// console.log(moment().format("YYYY-MM-DD"));
// console.log(moment().subtract(10, 'days').format("YYYY-MM-DD"));

// Pushes logs for specified systems (all by default) to the syndic (broker) PC.
// Pushes with a givem date. If no date is given, pushes all logs by default.
// As a safety, fullPush needs to be true to push all logs.


// Push logs from {days} days ago. Default is 7. Uses native clock to decide what date it is.
exports.refreshLogs = (req, res) => {
  // console.log(req.body);
  const {systemId} = req.params || {};
  let {days=1} = req.body || {};
  let {options} = req.body || {}
  let remainingDays = days;

  if (options.fullPush) {
    days = 0;
    pushLogs(systemId, null, options)
  } else {
    for (remainingDays; remainingDays >= 0; remainingDays--) {
      const date = moment().subtract(remainingDays, 'days').format("YYYY-MM-DD");
      pushLogs(systemId, date, options)
    }
  }

  res.json({message: `Request has been sent to push logs ${days ? 'for the past ' + days +' day(s)' : 'with a full push'}. Please note that it may take several minutes to a few hours depending on the load of the request.`})
}


//////////////////////////
// HELPER LOG FUNCTIONS //
//////////////////////////
const pushLogs = (systemId='*', date, options={}, params = {}) => {
  // console.log('options', options);
  const {
    pcType='[database|control|operator]',
    fullPush=false,
  } = options

  const pushLogsParams = {
    ...testLogin,
    client: "local",
    // tgt: `minion_${systemId}_*_${pcType}*`,
    tgt: `sys_${systemId}_*${pcType}*`,
    fun: "cp.push_dir",
    arg: [`/ZapSurgical/Logs/${date || ''}`],
    ...params
  }
  // console.log('pushLogsParams:');
  // console.log(pushLogsParams);
  if (date && fullPush) {
    console.log("You specified both a date and a full log push. Please choose one or the other.");
    // return ({error: true, message: "You specified both a date and a full log push. Please choose one or the other."})
  } else if (date || fullPush) {
    // console.log(`Your push was successful with date=${date}, fullPush=${fullPush}`);
    const request = axios.post(saltMasterUrl, pushLogsParams);
    request
    .then(({data}) => console.log('Logs successfully pushed:', data))
    .catch(err => console.log(`Log push failed due to the following error: ${err}`))
  } else {
    console.log("You must specify a date or a fullPush to push logs.");
    // return ({error: true, message: "You must specify a date or a fullPush to push logs."})
  }
}

exports.pushLogs = pushLogs

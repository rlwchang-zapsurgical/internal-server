const _ = require('lodash');
const { Config } = require('../models')
const aws = require('./awsHelpers.js')
const response = require('./response.js')
const mime = require('mime-types')



exports.getConfig = (req, res) => {
    Config.find()
        .then(configs => res.json({
            status: 'success',
            message: 'Successfully retrieved a list of available config.',
            configs: configs
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to retrieve a list of available Zap config.',
            error
        }))
}

exports.showConfig = (req, res) => {
  Config.findById(req.params.id)
    .then(config =>
      res.json({
        status: "success",
        message: "Successfully retrieved config entry.",
        config: config
      })
    )
    .catch(error =>
      res.json({
        status: "error",
        message: "Failed to retrieve a list of available Zap config.",
        error
      })
    );
};

exports.addConfig = (req, res) => {
    Config.create(req.body)
        .then(config => res.json({
            status: 'success',
            message: 'Successfully added a new config to the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to add new config to DB.',
            error
        }))
}

exports.editConfig = (req, res) => {
    Config.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(config => res.json({
            status: 'success',
            message: 'You have successfuly edited a config entry from the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to edit the corresponding config',
            error
        }))
}

exports.removeConfig = (req, res) => {
    Config.findByIdAndRemove(req.params.id)
        .then(config => res.json({
            status: 'success',
            message: 'You have successfuly removed a config entry from the DB',
            config
        }))
        .catch(error => res.json({
            status: 'error',
            message: 'Failed to remove the corresponding config',
            error
        }))
}

// ====================
// CONFIG DOCUMENTS
// ====================
const bucket = 'zap-surgical-installers'

exports.listConfigDocs = async (req, res) => {
    const {id} = req.params
    const {configId} = req.body

    let docsList = await aws.listS3Objects(bucket, `ConfigDocuments/${configId || id}`)

    // This is to handle cases where user might search using the actual _id from mongo
    if (_.isEmpty(docsList)) {
        const associatedConfig = await Config.findOne({id: id})
        
        if (associatedConfig) {
            const associatedId = associatedConfig.id
            docsList = await aws.listS3Objects(bucket, `ConfigDocuments/${configId || id}`)
        }
    }

    let result = []

    for(const doc of docsList) {
      const url = await aws.getS3ObjectUrl(bucket, doc['Key'])
      result.push({
          name: doc["Key"].split('/').slice(-1)[0],
          url
      })
    }

   return response.successResponse(res, result)
}


// This function only gets the signedUrl that can then be used to upload the file.
exports.getConfigDocsUploadLink = async (req, res) => {
    const {id} = req.params
    let {
        configId,
        files=[]
    } = req.body

    if (!Array.isArray(files)) {
        files = [files]
    }

    // console.log('id', id, configId)
    // console.log('files', files)
    const results = {}
    
    for (const file of files) {
        // console.log(mime.lookup(file.name))
        if (!_.includes([
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword'
        ], file.type)) {
            return response.errorResponse(res, {err: 'Invalid file type specified.'})
        }
    
        try {
            // console.log('file', file)
            const key = `ConfigDocuments/${configId || id}/${file.name}`
            const url = await aws.getS3ObjectUrl(
                bucket,
                key,
                {action: 'putObject'},
                {
                    ContentType: file.type,
                    ServerSideEncryption: "AES256",        
                }
            )
            // console.log('url', url)
            results[file.name] = {
                type: file.type,
                url
            }
        } catch(err) {
            return response.errorResponse(res, {err}, "Failed while fetching one of the pre-signed URLs")
        } 
    }    
    response.successResponse(res, results)
}

// This function adds the doc to the database
exports.addConfigDocsToDatabase = async (req, res) => {
    const {id} = req.params
    const {configId, files=[]} = req.body
    const config = await Config.findOne({
        $or: [{id}, {id: configId}]
    })
    if (!config) {config = await Config.findById(id)}

    // console.log('config', config.toObject())
    // console.log('files', files)
    filesWithBucketName = _.map(files, file => {
        return {
            ...file,
            bucket
        }
    })

    if (config) {
        config.docs = _.differenceWith(config.docs || [], files, _.isEqual)
        // console.log(config.toObject())
        // config.id = undefined
        await config.save()

    }

    let updatedConfig = await Config.findOne({$or: [{id}, {id: configId}]})
    if (!updatedConfig) {updatedConfig = await Config.findById(id)}

    response.successResponse(res, updatedConfig)
}
// This function deletes the document from the database and the AWS bucket
exports.removeConfigDocs = async (req, res) => {

}


// Config helpers

// collection = [{
//     version: '1.3.4'
// }, {
//     version: '1.112.123'
// }, {
//     version: '1.3.1'
// }, {
//     version: '1.3.11'
// }, {
//     version: '2.4'
// }]
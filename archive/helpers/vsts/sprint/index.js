
const vsts = require('../../../packages/vsts')
const workItem = require('../workItem')

exports.getLatestSprintItems = getLatestSprintItems
async function getLatestSprintItems(project, team, options) {
  try {
    const sprint = await getLatestSprint(project, team, options)
    
    const workItems = await workItem.workItemsQuery({
      sprint: sprint.path
    })
    
    return workItems
  } catch (err) {
    console.log(err)
  }
}

async function getLatestSprint(project, team, options) {
  const {fromLast=1} = options
  const workApi = await vsts.getWorkApi()
  const result = await workApi.getTeamIterations({
    project,
    team
  })

  return result.slice(fromLast * -1)[0]
}
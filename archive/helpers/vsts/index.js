let helpers = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (file.toLowerCase() != 'index.js' &&
      file.toLowerCase() != 'archive') {
     helpers = {...helpers, ...require(`./${file.replace('.js', '')}`)}
  }
});

module.exports = helpers
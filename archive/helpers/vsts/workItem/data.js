exports.workItemFields = [
  'System.Id',
  'System.TeamProject',
  'System.WorkItemType',
  'System.State',
  'System.Title',
  'zapsurgicalScrum.Reasonforchange',
  'zapsurgicalScrum.ApplicableRelease',
  'zapsurgicalScrum.FixedoraddedinRelease',
  'System.Description', //This will need to be cleaned
  'Microsoft.VSTS.Common.AcceptanceCriteria', //This will need to be cleaned
  'Microsoft.VSTS.CMMI.ImpactOnArchitecture',
  'Custom.HazardID',
  'Custom.SRSDetails',
  'Custom.TestSteps',
  'Custom.RMSeverity',
  'Custom.HazardID',
  'Custom.uFMEAID',
  'Custom.dFMEAID',
  'Custom.RiskAssessment'
]

exports.workItemFieldsToSanitize = [
  'Microsoft.VSTS.Common.AcceptanceCriteria',
  'Microsoft.VSTS.CMMI.ImpactOnArchitecture',
  'System.Description',
  'Custom.HazardID',
  'Custom.uFMEAID',
  'Custom.dFMEAID',
  'Custom.SRSDetails',
  'Custom.TestSteps',
  'Custom.RMSeverity',
  'Custom.RiskAssessment'
]
const vsts = require('../../../packages/vsts')
const stripHtml = require('string-strip-html')
const _ = require('lodash')
const {workItemFields, workItemFieldsToSanitize} = require('./data')

exports.workItemsQuery = workItemsQuery
async function workItemsQuery(options={}) {
  options = {
    columns: workItemFields,
    ...options
  }

  
  if (!options.condition) {
    const conditions = []
    
    if (options.version) {conditions.push(`[zapsurgicalScrum.FixedoraddedinRelease]='${options.version}'`)}
    if (options.project) {conditions.push(`[System.TeamProject]='${options.project}'`)}
    if (options.sprint) {conditions.push(`[System.IterationPath]='${options.sprint}'`)}
    options.condition = `WHERE (${conditions.join(' AND ')})`
  }



  
  const wiqlResults = await wiqlQuery(options)
  const columns = options.columns
  const ids = _.map(wiqlResults.workItems, workItem => workItem.id)
  let result = await workItemsIndex(ids, columns)
  const sanitizeFields = workItemFieldsToSanitize
  result = _.forEach(result, workItem => {
    for (const field of sanitizeFields) {
      workItem.fields[field] = workItem.fields[field] && stripHtml(workItem.fields[field]).replace(';', '')
    }
  })
  return result
}

exports.wiqlQuery = wiqlQuery
async function wiqlQuery(options={}) {
  let formattedColumns = _.map(options.columns, column => `[${column}]`)
  try {
    const query = `
    SELECT ${formattedColumns.join(', ')}
      FROM Workitems
      ${ options.condition}
      `
    const wiql = {query}
    const workTrackingApi = await vsts.getWorkItemTrackingApi()
    const result = await workTrackingApi.queryByWiql(wiql, {})

    return result
  } catch(err) {
    console.log('query error', err)
  }
}

async function workItemsIndex(ids, fields) {
  const workTrackingApi = await vsts.getWorkItemTrackingApi()
  let result = []

  const idsChunked = _.chunk(ids, 20) // This is to safeguard against querying issues if we query a lot at a time.
  try {
    for (const chunk of idsChunked) {
      const workItems = await workTrackingApi.getWorkItems(ids, fields)
      result = _.concat(result, workItems)
    }
  // console.log('result', result)

    return result
  } catch(err) {
    console.log(err)
  }
}
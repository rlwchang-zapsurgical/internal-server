const {Release} = require('../models');
const aws = require('./awsHelpers');
const vsts = require('./vsts')
// const moment = require('moment');
const _ = require('lodash')
const axios = require('axios');
const response = require('./response')

const bucket = 'zap-surgical-installers';

const {ROOT_URL} = process.env

exports.getReleases = (req, res) => {
  async function processRelease(release={}) {
    const {projectName, build} = release;
    // Check if it is a singular compressed file
    // if (fileName.match(/\a*(.zip|.gz|.rar|.7z)/g))
    // \TPS-20180702.2-4910
    const fileName = `${projectName}-${build.buildNumber}-${build.buildId}.zip`;
    const prefix = `${projectName}`
    console.log(prefix);
    console.log(fileName)
    try {
      const downloadUrl = await aws.generateDownloadUrl(bucket, prefix, fileName);
      return ({...release.toObject(), downloadUrl})
    } catch(err) {
      console.log(err);
      return result = release
    }
  }

  async function returnProcessedReleases(releases=[]) {
    const releasePromises = releases.map(async function(release) {return await processRelease(release)});
    const processedReleases = await Promise.all(releasePromises);
    return res.json(processedReleases);
  }

  Release.find()
    .then(returnProcessedReleases)
    .catch(err => res.json({message: 'Failed to retrieve release records from database.', err: err}))
}

const addRelease = (req, res) => {
  Release.create(req.body)
    .then(release => res.json({message: 'Release record was successfully added to database.', release}))
    .catch(err => res.json({message: 'Failed to add a new release records from database.', err}))
}
exports.addRelease = addRelease;

exports.editRelease = (req, res) => {
  const {id} = req.body;
  Release.findByIdAndUpdate(id, req.body, {new: true})
    .then(release => res.json({message: 'Release record was successfully added to database.', release}))
    .catch(err => res.json({message: 'Failed to add a new release records from database.', err}))
}

exports.hookRelease = hookRelease;
function hookRelease (req, res, next) {
  const data = req.body;
  // console.log('Service Hook Data:', JSON.stringify(data))
  const releaseData = formatReleaseData(data);

  if (releaseData.releaseType == 'sprint') {
    const { build, projectName, version } = releaseData;
    console.log(`A Sprint Release was made for ${projectName}, build number ${build.buildNumber}`)
    console.log(releaseData)
    const fileName = `${projectName}-${build.buildNumber}-${build.buildId}.zip`;
    const prefix = `${projectName}`
    const fullFileName = prefix ? `${prefix}/${fileName}` : fileName;

    const softwareData = {
      name: projectName,
      version: version,
      s3ObjectName: fullFileName,
      s3BucketName: bucket,
      s3ReleaseDocument: `${projectName}-${build.buildNumber}-${build.buildId}-ReleaseNotes.txt`,
      build
    }

    // ====================
    // Handle different project architectures here
    // ====================
    // This section is useful for logging additional entries into the software DB.
    // 
    // TREATMENT
    let types = []
    switch(projectName) {
      case 'Treatment':
      // This line is here for now to capture the full suite of Treatment. This can be removed once we move to just installers.
        axios.post(`http://localhost:${process.env.PORT}/api/software`, softwareData)
        types = ['Pendant', 'TDCS', 'View']
        for (const type of types) {
          const fileName = `${projectName}-${type}-${build.buildNumber}-${build.buildId}.zip`
          const fullFileName = prefix ? `${prefix}/${fileName}` : fileName;
          const data = {
            ...softwareData,
            name: `${projectName}-${type}`,
            s3ObjectName: fullFileName
          }
          axios.post(`http://localhost:${process.env.PORT}/api/software`, data)
        }
        break;
      case 'TPS':
         // This line is here for now to capture the full suite of TPS. This can be removed once we move to just installers.
         axios.post(`http://localhost:${process.env.PORT}/api/software`, softwareData)
         types = ['App', 'Service']
         for (const type of types) {
           const fileName = `${projectName}-${type}-${build.buildNumber}-${build.buildId}.zip`
           const fullFileName = prefix ? `${prefix}/${fileName}` : fileName;
           const data = {
             ...softwareData,
             name: `${projectName}-${type}`,
             s3ObjectName: fullFileName
           }
           axios.post(`http://localhost:${process.env.PORT}/api/software`, data)
         }
         break;
      case 'SiteMgmt':
        const fileName = `Broker-${build.buildNumber}-${build.buildId}.zip`
        const fullFileName = prefix ? `${prefix}/${fileName}` : fileName;
        const data = {
          ...softwareData,
          name: `Broker`,
          s3ObjectName: fullFileName
        }
        axios.post(`http://localhost:${process.env.PORT}/api/software`, data)
        break;
      default:
        axios.post(`http://localhost:${process.env.PORT}/api/software`, softwareData)      

    }

    
    const configData = {
      author: build.author,
      software: {
        tps: '',
        tds: '',
        broker: ''
      }
    }
    
    switch(softwareData.name.toLowerCase()) {
      case 'tps':
        configData.software.tps = softwareData.version
        break;
      case 'treatment':
        configData.software.tds = softwareData.version
        break;
      case 'sitemgmt':
      case 'broker':
        configData.software.broker = softwareData.version
        break;
    }
    
    const getSoftwareRequest = axios.get(`${ROOT_URL}/software`)
    getSoftwareRequest
      .then(({data}) => {
        const {software} = data;
        for (let i = 0; i < software.length; i++) {
          const softwareItem = software[i];
          // console.log(i);
          if ((softwareItem.name.toLowerCase() == 'treatment') && !configData.software.tds) {
            configData.software.tds = softwareItem.version
          }
          if ((softwareItem.name.toLowerCase() == 'tps') && !configData.software.tps) {
            configData.software.tps = softwareItem.version
          }
          if ((softwareItem.name.toLowerCase() == 'broker' || softwareItem.name.toLowerCase() == 'sitemgmt') && !configData.software.broker) {
            configData.software.broker = softwareItem.version
          }
        }
      const addConfigRequest = axios.post(`${ROOT_URL}/configs`, configData)
        console.log(addConfigRequest)
    })
      .catch(error => console.log(error))
  }

  req.body = releaseData;
  console.log('Release Data:')
  console.log(releaseData)
  next();
}

exports.getReleaseDocuments = getReleaseDocuments
async function getReleaseDocuments(req, res) {
  const {version, project} = req.query
  
    console.log('project', project)
    console.log('version', version)
  const workItems = await vsts.workItemsQuery({
    project,
    version
  })
  
  const result = {
    bugs: _.filter(workItems, (workItem) => _.get(workItem, ['fields', 'System.WorkItemType']) == 'Bug' ),
    features: _.filter(workItems, (workItem) => _.get(workItem, ['fields', 'System.WorkItemType']) == 'Product Backlog Item'),
    trace: _.filter(workItems, (workItem) => _.get(workItem, ['fields', 'Custom.TestSteps']) && _.get(workItem, ['fields', 'Custom.SRSDetails'])),
    issues: findIssues(workItems),
    project,
    version
  }

  response.successResponse(res, result)
}

function findIssues(data=[]) {
  const results = []

  const allowedBugStates = ['Done', 'Approved', 'Committed', 'Removed']
  const allowedBugs = allowedBugStates.join(' ')

  
  for(let feature of data) {
      if (!feature.fields['Custom.TestSteps']) {
        results.push({
        id: feature.id,
        type: 'test',
        description: 'No test steps found.'
        })
      }
      
      if (
        feature.fields["System.WorkItemType"] == "Bug" &&
        _.includes(allowedBugStates, feature.fields['System.State']) &&
        (
          parseSeverity(feature.fields["Custom.RMSeverity"]) > 2 ||
          parseSeverity(feature.fields["Microsoft.VSTS.Common.Severity"] > 2)
        )
        )
      {
        results.push({
          id: feature.id,
          type: 'bug',
          description: `There is a bug of with a severity greater 2 that is not in one of the allowed states: ${allowedBugs}`
        })    
      }
  }
  
  return results  
}
//////////////////////////////
// Release Helper Functions //
//////////////////////////////



function branchParser(branchName) {
  const nameRegex = new RegExp('[a-zA-Z._-]+', 'gi');

  if (branchName && versionParser(branchName)) {
    return versionParser(branchName)
  } else if (branchName && branchName.match(nameRegex)) {
    return (branchName.match(nameRegex).slice(-1)[0])
  } else {
    return branchName
  }
}

function versionParser(branchName) {
  const versionRegex = new RegExp('([0-9].){2,3}([0-9])+', 'gi');
  console.log(branchName);
  if (branchName && branchName.match(versionRegex)) {
    return (branchName.match(versionRegex)[0])
  } else {
    return undefined;
  }
}

function typeParser(branchName) {
  const type = ['engineering'];
  if (branchName && branchName.match(/release/gi)) type.push('qa');

  return type
}

function releaseTypeParser(branchName) {
  if (branchName && branchName.match(/release/gi)) return 'sprint';
}

function secondsToBuild(startTime, endTime) {
  return new Date(endTime) - new Date(endTime);
}

function formatReleaseData(data) {
  // Extract out useful data from the webhook
  const {
    resource: {
      id: buildId,
      url: buildJsonUrl,
      status,
      buildNumber,
      startTime: buildStart,
      finishTime: buildEnd,
      project: { name: projectName = `Unnamed Project - ${data.resource.definition.name} | ${data.resource.buildNumber}` } = {},
      definition: { name: definition = `No definition name` } = {},
      // log: {url: logUrl},
      requestedBy: { displayName: authorBy } = {},
      requestedFor: { displayName: authorFor } = {},
      sourceBranch,
      sourceVersion: commitHash
    },
    resourceVersion,
    createdDate
  } = data;

  const buildUrl = buildJsonUrl.replace('_apis/build/Builds/', '_build/index?buildId=')
  const buildDuration = secondsToBuild(buildStart, buildEnd);
  const fullBranchName = sourceBranch;
  const branchName = fullBranchName ? branchParser(fullBranchName) : 'Branch and Version not defined';

  const buildData = {
    buildId,
    buildStart,
    buildEnd,
    buildDuration,
    buildNumber,
    buildUrl,
    buildJsonUrl,
    branchName,
    fullBranchName,
    commitHash,
    definition,
    author: authorFor || authorBy
  }

  const releaseData = {
    // name: ,
    version: versionParser(branchName),
    // description: ,
    type: typeParser(fullBranchName),
    projectName,
    // downloadUrl: `https://zapsurgical-installer.s3.aws.amazon.com/`,
    // instructionsUrl: ,
    releaseType: releaseTypeParser(fullBranchName),
    notes: [],
    dateUploaded: new Date(buildEnd),
    build: buildData
  }

  return releaseData;
}




// Sample Data
// { subscriptionId: '09b44797-2423-47a0-a2ed-5d5b84156a7f',
//   notificationId: 67,
//   id: 'da1976a7-8cba-4b0c-b00c-2b6a08849fb3',
//   eventType: 'build.complete',
//   publisherId: 'tfs',
//   message: 
//    { text: 'Build 20180610.6 succeeded',
//      html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464">20180610.6</a> succeeded',
//      markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464) succeeded' },
//   detailedMessage: 
//    { text: 'Build 20180610.6 succeeded',
//      html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464">20180610.6</a> succeeded',
//      markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4464) succeeded' },
//   resource: 
//    { _links: 
//       { self: [Object],
//         web: [Object],
//         sourceVersionDisplayUri: [Object],
//         timeline: [Object] },
//      properties: {},
//      tags: [],
//      validationResults: [],
//      plans: [ [Object] ],
//      triggerInfo: {},
//      id: 4464,
//      buildNumber: '20180610.6',
//      status: 'completed',
//      result: 'succeeded',
//      queueTime: '2018-06-11T02:04:02.33863Z',
//      startTime: '2018-06-11T02:04:39.7147951Z',
//      finishTime: '2018-06-11T02:05:29.4135955Z',
//      url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Builds/4464',
//      definition: 
//       { drafts: [],
//         id: 42,
//         name: 'Treatment-.NET Desktop-PendantApp',
//         url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Definitions/42?revision=7',
//         uri: 'vstfs:///Build/Definition/42',
//         path: '\\',
//         type: 'build',
//         queueStatus: 'enabled',
//         revision: 7,
//         project: [Object] },
//      buildNumberRevision: 6,
//      project: 
//       { id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         name: 'Treatment',
//         description: 'Treatment delivery system',
//         url: 'https://zapsurgical.visualstudio.com/_apis/projects/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         state: 'wellFormed',
//         revision: 63110880,
//         visibility: 'private' },
//      uri: 'vstfs:///Build/Build/4464',
//      sourceBranch: 'refs/heads/develop',
//      sourceVersion: '2e03961ee29ab6df5c5489664f2ae8498c190d4b',
//      queue: 
//       { id: 1,
//         name: 'Default',
//         url: 'https://zapsurgical.visualstudio.com/_apis/build/Queues/1',
//         pool: [Object] },
//      priority: 'normal',
//      reason: 'individualCI',
//      requestedFor: 
//       { displayName: 'Ben Chen',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/8fad799a-5598-63f6-9cde-fff4cc53806c',
//         _links: [Object],
//         id: '8fad799a-5598-63f6-9cde-fff4cc53806c',
//         uniqueName: 'Ben@zapsurgical.com',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=8fad799a-5598-63f6-9cde-fff4cc53806c',
//         descriptor: 'aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj' },
//      requestedBy: 
//       { displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//         _links: [Object],
//         id: '00000002-0000-8888-8000-000000000000',
//         uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//         descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA' },
//      lastChangedDate: '2018-06-11T02:05:29.777Z',
//      lastChangedBy: 
//       { displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//         _links: [Object],
//         id: '00000002-0000-8888-8000-000000000000',
//         uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//         descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA' },
//      orchestrationPlan: { planId: '479fbd3f-360c-4037-8a10-d1a53abacecb' },
//      logs: 
//       { id: 0,
//         type: 'Container',
//         url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/builds/4464/logs' },
//      repository: 
//       { id: '1b1ccb62-d3cf-4683-a499-55c317ab08c6',
//         type: 'TfsGit',
//         name: 'Treatment',
//         url: 'https://zapsurgical.visualstudio.com/Treatment/_git/Treatment',
//         clean: null,
//         checkoutSubmodules: false },
//      keepForever: false,
//      retainedByRelease: false,
//      triggeredByBuild: null },
//   resourceVersion: '2.0',
//   resourceContainers: 
//    { collection: 
//       { id: 'de1f328d-b12a-4368-afec-1208abbcce65',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' },
//      account: 
//       { id: '62e5a3ed-0139-4177-a9d0-6af10375baee',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' },
//      project: 
//       { id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' } },
//   createdDate: '2018-06-11T02:05:31.2479373Z' }
// Treatment
// Treatment-.NET Desktop-PendantApp
// refs/heads/develop
// develop
// { subscriptionId: '09b44797-2423-47a0-a2ed-5d5b84156a7f',
//   notificationId: 68,
//   id: 'd0098f78-0795-42dc-8461-54527dfce490',
//   eventType: 'build.complete',
//   publisherId: 'tfs',
//   message: 
//    { text: 'Build 20180610.6 succeeded',
//      html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465">20180610.6</a> succeeded',
//      markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465) succeeded' },
//   detailedMessage: 
//    { text: 'Build 20180610.6 succeeded',
//      html: 'Build <a href="https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&amp;builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465">20180610.6</a> succeeded',
//      markdown: 'Build [20180610.6](https://zapsurgical.visualstudio.com/web/build.aspx?pcguid=de1f328d-b12a-4368-afec-1208abbcce65&builduri=vstfs%3a%2f%2f%2fBuild%2fBuild%2f4465) succeeded' },
//   resource: 
//    { _links: 
//       { self: [Object],
//         web: [Object],
//         sourceVersionDisplayUri: [Object],
//         timeline: [Object] },
//      properties: {},
//      tags: [],
//      validationResults: [],
//      plans: [ [Object] ],
//      triggerInfo: {},
//      id: 4465,
//      buildNumber: '20180610.6',
//      status: 'completed',
//      result: 'succeeded',
//      queueTime: '2018-06-11T02:04:02.4948914Z',
//      startTime: '2018-06-11T02:04:53.5534731Z',
//      finishTime: '2018-06-11T02:05:30.1834767Z',
//      url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Builds/4465',
//      definition: 
//       { drafts: [],
//         id: 56,
//         name: 'Treatment-.NET Desktop-PendantApp-RichardsPlayground',
//         url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/Definitions/56?revision=1',
//         uri: 'vstfs:///Build/Definition/56',
//         path: '\\',
//         type: 'build',
//         queueStatus: 'enabled',
//         revision: 1,
//         project: [Object] },
//      buildNumberRevision: 6,
//      project: 
//       { id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         name: 'Treatment',
//         description: 'Treatment delivery system',
//         url: 'https://zapsurgical.visualstudio.com/_apis/projects/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         state: 'wellFormed',
//         revision: 63110880,
//         visibility: 'private' },
//      uri: 'vstfs:///Build/Build/4465',
//      sourceBranch: 'refs/heads/develop',
//      sourceVersion: '2e03961ee29ab6df5c5489664f2ae8498c190d4b',
//      queue: 
//       { id: 1,
//         name: 'Default',
//         url: 'https://zapsurgical.visualstudio.com/_apis/build/Queues/1',
//         pool: [Object] },
//      priority: 'normal',
//      reason: 'individualCI',
//      requestedFor: 
//       { displayName: 'Ben Chen',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/8fad799a-5598-63f6-9cde-fff4cc53806c',
//         _links: [Object],
//         id: '8fad799a-5598-63f6-9cde-fff4cc53806c',
//         uniqueName: 'Ben@zapsurgical.com',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=8fad799a-5598-63f6-9cde-fff4cc53806c',
//         descriptor: 'aad.OGZhZDc5OWEtNTU5OC03M2Y2LTljZGUtZmZmNGNjNTM4MDZj' },
//      requestedBy: 
//       { displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//         _links: [Object],
//         id: '00000002-0000-8888-8000-000000000000',
//         uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//         descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA' },
//      lastChangedDate: '2018-06-11T02:05:30.543Z',
//      lastChangedBy: 
//       { displayName: 'Microsoft.VisualStudio.Services.TFS',
//         url: 'https://app.vssps.visualstudio.com/A62e5a3ed-0139-4177-a9d0-6af10375baee/_apis/Identities/00000002-0000-8888-8000-000000000000',
//         _links: [Object],
//         id: '00000002-0000-8888-8000-000000000000',
//         uniqueName: '00000002-0000-8888-8000-000000000000@2c895908-04e0-4952-89fd-54b0046d6288',
//         imageUrl: 'https://zapsurgical.visualstudio.com/_api/_common/identityImage?id=00000002-0000-8888-8000-000000000000',
//         descriptor: 's2s.MDAwMDAwMDItMDAwMC04ODg4LTgwMDAtMDAwMDAwMDAwMDAwQDJjODk1OTA4LTA0ZTAtNDk1Mi04OWZkLTU0YjAwNDZkNjI4OA' },
//      orchestrationPlan: { planId: '569d44c1-3af4-4335-ac13-1364a224199a' },
//      logs: 
//       { id: 0,
//         type: 'Container',
//         url: 'https://zapsurgical.visualstudio.com/74fb2031-8c0c-4b51-89b3-e2a3f21d47b5/_apis/build/builds/4465/logs' },
//      repository: 
//       { id: '1b1ccb62-d3cf-4683-a499-55c317ab08c6',
//         type: 'TfsGit',
//         name: 'Treatment',
//         url: 'https://zapsurgical.visualstudio.com/Treatment/_git/Treatment',
//         clean: null,
//         checkoutSubmodules: false },
//      keepForever: false,
//      retainedByRelease: false,
//      triggeredByBuild: null },
//   resourceVersion: '2.0',
//   resourceContainers: 
//    { collection: 
//       { id: 'de1f328d-b12a-4368-afec-1208abbcce65',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' },
//      account: 
//       { id: '62e5a3ed-0139-4177-a9d0-6af10375baee',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' },
//      project: 
//       { id: '74fb2031-8c0c-4b51-89b3-e2a3f21d47b5',
//         baseUrl: 'https://zapsurgical.visualstudio.com/' } },
//   createdDate: '2018-06-11T02:05:31.372934Z' }
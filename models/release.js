const mongoose = require('mongoose');
const uuid = require('uuid/v4')

const Schema = new mongoose.Schema({
    name: {
      type: String,
      // required: true,
      // unique: true,
      // index: true
    },
    status: {type: String, default: 'candidate'}, // 
    stage: String,
    software: {
      'tps': {type: mongoose.Schema.Types.ObjectId, ref: "Build"},
      'tds': {type: mongoose.Schema.Types.ObjectId, ref: "Build"},
      'broker': {type: mongoose.Schema.Types.ObjectId, ref: "Build"}
    },
    approval: {
      'eng': {
        requestor: String,
        requested: Boolean,
        requestDate: Date,
        editor: String,
        edited: Boolean,
        editDate: Date,
        approver: String,
        approved: Boolean,
        approveDate: {type: Date, default: Date.now},
        lastModifier: String,
        lastModified: Date,
      },
      'qa': {
        requestor: String,
        requested: Boolean,
        requestDate: Date,
        approver: String,
        approved: Boolean,
        approveDate: Date,
        lastModifier: String,
        lastModified: Date,
      }
    },
    eco: String,
    crn: String,
    documents: Array,
    endDate: Date,
    notes: String
    // software: {type: mongoose.Schema.Types.ObjectId, ref: "Software"}, 
    // stages: [{type: mongoose.Schema.Types.ObjectId, ref: "Stage"}]
}, {minimize: false, toJSON: {virtuals: true} })

module.exports = Schema
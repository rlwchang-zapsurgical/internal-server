const mongoose = require('mongoose');
const AutoIncrement = require("mongoose-sequence")(mongoose);

// This configuration focuses on integration rather than individual software integrity
// These configurations can be approved or disapproved by QA
const Schema = mongoose.Schema({
    // There is a hidden 'id' field created automatically thanks to the plugin
    // This id field is the so-called Zap-X id number for a given configuration.
    // software: [{
    //     type: Schema.Types.ObjectId,
    //     ref: 'Software'
    // }],
    software: {type: Object, default: {}},
    // software will be an object of the form:
    // {
    //     TPS: '1.9.9',
    //     TDS: '1.8.7',
    //     Broker: '1.54.1'
    // }
    qaApproved: Boolean,
    s3ObjectName: String,
    s3BucketName: String,
    author: String,
    dateCreated: {type: Date, deafult: Date.now},
    dateModified: Date,
    editor: String,
    approver: String,
    dateApproved: Date,
    notes: String,
    docs: [{type: mongoose.Schema.Types.ObjectId, ref: 'Document'}],
    documents: Array
}, {minimize: false})

Schema.plugin(AutoIncrement, { inc_field: 'id' })


module.exports = Schema
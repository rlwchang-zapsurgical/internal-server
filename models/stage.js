const mongoose = require('mongoose');
const uuid = require('uuid/v4')

var Schema = new mongoose.Schema({
    category: String,
    isComplete: Boolean,
    release: {type: mongoose.Schema.Types.ObjectId, ref: 'Release'},
    software: {type: mongoose.Schema.Types.ObjectId, ref: 'Software'},
    documents: [{type: mongoose.Schema.Types.ObjectId, ref: 'Document'}],
    data: mongoose.Schema.Types.Mixed
}, { _id: false, minimize: false, toJSON: {virtuals: true} })

module.exports = Schema
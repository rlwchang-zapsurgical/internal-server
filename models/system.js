const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  // status: "active",
  systemId: String,
  date: {type: Date, default: new Date()},
  name: String,
  owner: {
    name: String,
    city: String,
    state: String,
    country: String,
    lat: Number, //Latitude
    lon: Number, //Longitude
  },
  name: String,
  city: String,
  state: String,
  country: String,
  lat: Number, //Latitude
  lon: Number, //Longitude
  // treatments: [{type: mongoose.Schema.Types.ObjectId, ref: 'License'}],
  treatments: Array,
  masterDiskControl:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'MasterDisk'
  },
  masterDiskOperator:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MasterDisk'
  },
  masterDiskDatabase:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'MasterDisk'
  },
  notes: [String]
}, {minimize: false})

module.exports = Schema

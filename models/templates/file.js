const uuid = require('uuid/v4')

module.exports = {
  uuid: {type: String, default: uuid}, // This is more for client side use
  name: String,
  version: String,
  s3ObjectName: String,
  s3BucketName: String,
  notes: String,
  requirements: Array,
  ext: String,
  mime: String,
}
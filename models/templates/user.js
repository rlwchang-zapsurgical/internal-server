const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  number: String,
}, {_id: false});


module.exports = userSchema;

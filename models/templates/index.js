const _ = require('lodash')
const mongoose = require('mongoose')
const omissionList = [
  'index.js',
  'archive',
  'schema'
]

let templates = {}

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (!_.includes(omissionList, file)) {
    const fileName = file.replace('.js', '')
     templates[fileName] = require(`./${fileName}`)
  }
});

module.exports = templates
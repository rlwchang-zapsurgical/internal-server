const mongoose = require('mongoose');
// TODO: Change the managers and members to reference the mongoose model of Users

const Schema = mongoose.Schema({
  name: String,
  managers: Array,
  // managers: {type: mongoose.Schema.Types.ObjectId, ref: 'User', default: []},
  members: Array,
  // members: {type: mongoose.Schema.Types.ObjectId, ref: 'User', default: []}
}, {minimize: false})

module.exports = Schema
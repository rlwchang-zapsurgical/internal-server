const templates = require('./templates')
const mongoose = require('mongoose')

const Schema = new mongoose.Schema({
  name: String,
  type: String,
  code: String,
  eco: String,
  revision: String,
  template: {type: mongoose.Schema.Types.ObjectId, ref: 'Document'},
  isTemplate: Boolean,
  data: {type: mongoose.Schema.Types.Mixed, default: {}},
  dateUploaded: {type: Date, default: Date.now},
  dateChanged: {type: Date, default: Date.now},
  file: templates.file
}, {minimize: false})

module.exports = Schema
const mongoose = require('mongoose');
const _ = require('lodash')
const templates = require('./templates')


const Schema = new mongoose.Schema({
  name: String,
  version: String,
  type: {type: String, required: true}, //this will be 'operator', 'control', or 'database'
  // file: {type: fileSchema, default: {}},
  file: templates.file,
  software: Array,
  documents: [{type: mongoose.Schema.Types.ObjectId, ref: 'Document', default: []}],
}, {
  minimize: false,
  toJSON: {virtuals: true, minimize: false},
  toObject: {virtuals: true, minimize: false}
});

Schema.virtual('softwareObjects').get(function() {
  const result = {}
  _.forEach(this.software, (softwareItem) => {
   result[softwareItem.name] = softwareItem
  });

  return result
})


module.exports = Schema;

const mongoose = require('mongoose')
const templates = require('./templates')

const Schema = new mongoose.Schema({
  name: {type: String, required: true},
  qaApproved: {type: Boolean, default: false},
  notes: {type: [String], default: []},
  s3ReleaseDocument: {type: String},
  file: templates.file,
  build: Object, // This will eventually be phased out to use actual references
  buildRef: {type: mongoose.Schema.Types.ObjectId, ref: 'Build'},
  masterDisk: {type: mongoose.Schema.Types.ObjectId, ref: 'MasterDisk'},
  version: String,
  dateUploaded: Date
}, {minimize: false})

module.exports = Schema
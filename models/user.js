const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const uuid = require('uuid/v4')


const Schema = new mongoose.Schema({
  image: String,//base64 encoded data url
  firstName: String,
  lastName: String,
  username: String,
  password: String,
  email: String,
  number: String,
  allowedDevices: [String],
  mfaSecret: {type: String, default: uuid},
  jwtSecret: {type: String, default: uuid},
  // authLevel: {type :Number, default: 99},
  roles: [String], //eng, prod, qa, admin, or boss
  permissions: [String],
  groups: [{type: mongoose.Schema.Types.ObjectId, ref: 'UserGroup'}],
  hashingAlg: {type: String, default: 'SHA-256'}
});

Schema.plugin(passportLocalMongoose);

module.exports = Schema

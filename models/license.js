const mongoose = require('mongoose');
const uuid = require('uuid/v4')

var Schema = new mongoose.Schema({
    category: String,
    code: String,
    dateIssued: {type: Date, default: Date.now},
    uuid: {type: String, default: uuid}, // A _id will automatically be assigned as well, which can be used in conjunction
    used: {type: Boolean, default: false},
    expired: {type: Boolean, default: false}
}, { _id: false, minimize: false, toJSON: {virtuals: true} })


Schema.virtual('remaining').get(
  function() {
    let remaining = 0
    this.codes.forEach(code => {
      if (!code.used && !code.expired) {remaining++}
    })
    return remaining
  }
)

module.exports = Schema
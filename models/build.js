const mongoose = require('mongoose');
const templates = require('./templates')

const Schema = new mongoose.Schema({
  name: {type: String},
  version: {type: String},
  description: String,
  type: String,
  instructionsUrl: String,
  notes: [String],
  dateUploaded: Date,
  build: Object,
  // {
  //   buildId: String,
  //   buildStart: Date,
  //   buildEnd Date,
  //   buildDuration: Number,
  //   buildNumber: String,
  //   branchName: String,
  //   fullBranchName: String,
  //   commitHash: String,
  //   definition: String,
  //   author: String
  // },
  file: templates.file,
  masterDisks: {
    operator: {type: mongoose.Schema.Types.ObjectId, ref: "MasterDisk"},
    control: {type: mongoose.Schema.Types.ObjectId, ref: "MasterDisk"},
    database: {type: mongoose.Schema.Types.ObjectId, ref: "MasterDisk"}
  },
  notes: String,
  projectName: String,
  qaApproved: Boolean,
  qaApprovalRequested: {type: Boolean, default: false},
  rejected: Boolean,
  testingStatus: String,
  releaseNotes: templates.file
});


module.exports = Schema;

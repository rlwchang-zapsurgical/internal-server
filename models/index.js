const _ = require('lodash')
const mongoose = require('mongoose')
const omissionList = [
  'index.js',
  'archive',
  'templates'
]

require("fs").readdirSync(__dirname).forEach(function (file) {
  if (!_.includes(omissionList, file)) {
      // const modelName = _.startCase(file.replace('Model.js', ''))
      let modelName = file.replace(/(Model)?\.js/, '')
      const pascalCaseName = _.startCase(modelName).replace(/\W+/gi, '')
      const model = mongoose.model(pascalCaseName, require(`./${file.replace('.js', '')}`))
      exports[modelName] = model
      exports[pascalCaseName] = model
  }
});

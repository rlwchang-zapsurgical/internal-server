## Introduction
This is the server for the Internal Enterprise Solution (IES).

IES is a suite of web applications that are used to monitor our medical devices, send patches, distribute software, and take care of other logistics related to the development and release of Zap software and devices.

This server takes care of most of the backend communications and functionality.

*Note:* This server is **NOT** in charge of interfacing with the system directly. All direct system activities, including fetching data and remote installations is handled by a separate REST server, running CherryPy and the SaltStack framework.

## Getting Started
This section is for helping new developers get setup and familiar with the server.


### New to Node.js?
If you are new to Node.js, hereafter referred to simply as Node, start off by installing the latest version of the software. Node can be downloaded from https://nodejs.org/en/.

Node is a program that allows Javascript, which is designed for the browser, to operate in a native desktop environment. Node is built on top of Google's V8 engine, which serves as a transpiler. When Javascript is fed to the engine, the code is converted to C++ code, which indirectly gives Javascript access to things like the filesystem. This allows Javascript to be used as a server-side language, since it now behaves like any other programming language.

When you download Node, you get access to both the Node program, and a very powerful package manager caller *npm*. *npm* is used for downloading packages, managing dependencies. To see if you have *npm* installed, go to the command line and type the following:

```npm --version```

If this command gives you a valid version number, then you are good to go. If it gives you an error, make sure you have installed the latest version of Node. In previous versions, *npm* was not included. If you need to, you can also download *npm* separately. If you are sure that you have downloaded *npm*, check your environment variables to make sure your PATH variable points to it.

By default, when you copy this repository, the dependencies are not incldued, so you will first need to install them. You can do this by running ```npm install```. Assuming you have internet connection, this will build the dependency tree for you and pull in all of the relevant packages.

After you have installed all the dependencies, you can start your server by running ```npm run dev```. This runs the *dev* script found in your ```package.json``` folder. You will likely notice that it spits out an error and complains.

The reason for the complaints are missing environment variables. This repository keeps most of the configuration in ```.env``` files. To get these setup, skip down to the **Developer Setup** section and find the **Environment Variables**.

Once that is all setup, you should be able to start the server using ```npm run dev```. This uses a package called *nodemon* to start your server. As you develop, *nodemon* watches all linked Javascript files. When one of them changes, it will automatically restart your server for you to show the new changes. If you do not want it to always restart, start the server using the following command: ```node -r dotenv/config index.js dotenv_config_path=.env```. You will have to manually kill the server with ```Ctrl + C``` each time and restart it when you make changes.

To learn more about Node and this server, read the sections below.

### Experienced Node developers
Do as you normally would with a Node repository. After installing with *yarn* or *npm* you can get started with ```npm run dev``` or ```yarn dev```. When you run it, you will notice that it will have an error and complain at you. The reason for this is because most of the settings are kept in a ```.env``` file. Create two ```.env``` files in the root directory. See the **Developer Setup**'s **Environment Variables** section for more details.

## Developer Setup

### Download Node.js
To get started, make sure you have a modern version of Node.js, hereafter referred to as Node, downloaded. You can download it at https://nodejs.org/en/. Download the latest version if in doubt, because you need to have *npm* or some kind of Node package manager installed. The latest versions come up with *npm* bundled, so you can follow along right out of the box.

### Git Clone
This repository is hosted on Bitbucket. Make sure you have *Git* installed (https://git-scm.com/downloads). Afterwards, navigate to your desired directory on a terminal and run the following command:
```
git clone https://bitbucket.org/rlwchang-zapsurgical/internal-server.git
```

You may be prompted to enter your authentication credentials. Make sure you are authorized to clone the repository. As of 10-10-18, the following individuals are admins and can grant you access:
- Richard Chang
- Qing Feng
- Riju Pahwa
- Ben Chen

### Installing Dependencies
By default, the dependencies are not installed. Go to the root directory of your installation and run the following command to pull in the dependencies:
```
npm run install
```

### Environment Variables
The server actually uses a lot of environment variables to store tis configurations. In order to run the server, you need to create a ```.env``` and ```.env.staging``` file in the root directory. After creating them, your directory should look like this:

```
├── config
├── .elasticbeanstalk
├── .env // Notice this file
├── .env.staging // And this one!
├── .git
├── .gitignore
├── helpers
├── index.js
├── middleware
├── models
├── node_modules
├── package.json
├── package-lock.json
├── packages
├── README.md
├── routes
├── services
└── yarn.lock
```

In the ```.env``` file, place the configuration for your local development environment. Yours should look similar to the following as of 10-10-2018:

__.env__

```
NODE_ENV=development

ROOT_URL='http://localhost:3000/api'

JWT_SECRET=zapsurgicalsecret

AWS_ACCESS_KEY=[YOUR OWN AWS ACCESS KEY GOES HERE!]
AWS_SECRET_KEY=[YOUR OWN AWS SECRETE KEY GOES HERE!]

SALT_MASTER_URL='https://entsm1.zapsurgical.com'

SALT_USERNAME=zapsurgical
SALT_PASSWORD=zap123
SALT_EAUTH=auto

REDIS=true
# REDIS_URL='redis://ec2-54-193-37-231.us-west-1.compute.amazonaws.com'
# REDIS_URL='redis://zap-enterprise-dev-001.cgkzin.0001.usw1.cache.amazonaws.com:6379'
TWILIO_ACCESS_KEY=AC891f33d07776299a6e326a672a76d5e7
TWILIO_SECRET_KEY=503b58bf23c825b3a14e55f2e4072fb5
TWILIO_PHONE_NUMBER='+14157921388'
```

Likewise, your ```.env.staging``` file should look similar, with a few different values. This file is used when you run ```npm run staging``` or ```yarn staging```

__.env.staging__
```
NODE_ENV=production

ROOT_URL='http://enterprise.zapsurgical.com/api'

JWT_SECRET=zapsurgicalsecret

AWS_ACCESS_KEY=[YOUR OWN AWS ACCESS KEY GOES HERE!]
AWS_SECRET_KEY=[YOUR OWN AWS SECRETE KEY GOES HERE!]

SALT_MASTER_URL='https://entsm1.zapsurgical.com'

SALT_USERNAME=zapsurgical
SALT_PASSWORD=zap123
SALT_EAUTH=auto

REDIS=true
# REDIS_URL='redis://ec2-54-193-37-231.us-west-1.compute.amazonaws.com'
# REDIS_URL='redis://zap-enterprise-dev-001.cgkzin.0001.usw1.cache.amazonaws.com:6379'

TWILIO_ACCESS_KEY=AC891f33d07776299a6e326a672a76d5e7
TWILIO_SECRET_KEY=503b58bf23c825b3a14e55f2e4072fb5
TWILIO_PHONE_NUMBER='+14157921388'
```

### Starting the Server
In order to start the server with automatic restarts, run ```npm run dev```. This will start the server and restart it whenever a linked Javascript file changes.

Run ```node -r dotenv/config index.js dotenv_config_path=.env``` if you do not want this behavior.

Congratulations, you are ready to develop!

## Navigating the Filesystem
The filesystem consists of a lot of files and folders. The most important ones will be explained below to help orient new developers.

The filesystem with all the relevant files and folders is shown below.

```
├── .env
├── .env.staging
├── helpers
├── index.js
├── middleware
├── models
├── node_modules
├── package.json
├── packages
├── README.md
├── routes
└── services
```

The files and folders are explained below:

### Key Files and Folders

```package.json``` - This is where all of your configurations are kept, including your *npm* scripts and your dependencies. Without this file, it would be a nightmare for new developers to get setup.

```index.js``` - This is the entry point into the server. All of the other relevant Javascript files are directly or indirectly pulled in by this file.

```node_modules``` - This directory is where all of your dependencies live after you run ```npm run install```. Beyond browsing the code, you should almost never touch this folder. This is not pushed with each commit, so any changes made here won't show up for the developers.

### Core Modules

```routes``` - This is where all the route files live. These files determine what helper functions are matched to a given request, sent to a given url or path.

```middleware``` - This is where middleware functions live. Middleware functions are usually reusable functions that sit right between the part where the route is matched and a helper or handler function is called. It intercepts and inspects the request first, either allowing it to pass to the handler, modifying the request, or outright rejecting it.

```helpers``` - This is where the controllers or handlers live. After a request is captured and matched with a certain route, it is sent to a helper function to be handled.

```models``` - This is where the database models live. Check here to see what the general format of a given database entry looks like.

```packages``` - This is where 3rd party package setups live.

```services``` - This is not currently in use and may be for a future feature.


### Configurations

```package.json``` - This is mentioned above.

```.env``` - This is where your local development environment settings live. This does **NOT** get pushed.

```.env.staging``` - This is where your local development environment settings live. This is called staging because it has a few flags set to make it mimic what it would be like in a production environment. This does **NOT** get pushed.

```README.md``` - This is where the current README that you are reading lives. If you need to change something in this README, do so in this file.
// This script writes the environment configuration for Elastic Beanstalk deployment
const dotenv = require('dotenv')
const fs = require('fs')
const path = require('path')


console.log('Creating config file for environment variables...')
const dotenvPath = path.resolve(__dirname, '../.env.prod')
const targetPath = path.resolve(__dirname, '../.ebextensions/env.config')
if (fs.existsSync(dotenvPath)) {
  const environmentVariables = dotenv.parse(fs.readFileSync(dotenvPath))
  // console.log(environmentVariables)
  let fileData = [
    // 'AWSConfigurationTemplateVersion: 1.1.0.0',
    // 'OptionSettings:',
    'option_settings:',
    '  aws:elasticbeanstalk:application:environment:',    
  ]

  
  for (const key in environmentVariables) {
    const variable = environmentVariables[key]
    fileData.push(`    ${key}: "${variable}"`)
  }
  fileData = fileData.join('\n')

  fs.writeFileSync(targetPath, fileData)

} else {
  console.warn(`Unabled to load environment variables. File ${dotenvPath} does not exist.`)
}


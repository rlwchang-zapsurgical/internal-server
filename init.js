const _ = require('lodash')
const fs = require('fs')
const path = require('path')

// ====================
// LOGGING
// ====================
const StackTrace = require('stacktrace-js')
const colors = require('colors')

console.log(_.repeat('=', 40))
console.log(`SERVER STARTED ON: ${(new Date()).toLocaleString()}`)
console.log(_.repeat('=', 40))

const logging = {
  log: {
    func: global.console.log,
    color: 'green',
    label: 'LOG'
  },
  warn: {
    func: global.console.warn,
    color: 'yellow',
    label: 'WARN'
  },
  error: {
    func: global.console.error,
    color: 'red',
    label: 'ERROR'
  }
}

for (const level in logging) {
  const log = logging[level]
  const {func, color, label} = log
  global.console[level] = (...params) => {
    const trace = StackTrace.getSync()[1]
    const filePathArr = trace.fileName.split('\\')
    const filePath = path.join(...filePathArr.slice(-3, filePathArr.length))
    // log(trace)
    return func(
      label.padEnd(5)[color],
      '||',
      (new Date()).toLocaleString(),
      '||',
      `(${filePath}:${trace.lineNumber})`,
      ':',
      ...params
    )
  }
}

// ====================
// ENVIRONMENT VARIABLES
// ====================
// Disabled for now since it would require restructuring the .env files and cause breaking changes
// if (process.env.LOAD_ENV) {
//   const dotenv = require('dotenv')
//   const commonEnv = dotenv.config().parsed
//   const devEnv = dotenv.parse(fs.readFileSync('.env.dev'))
//   const stagingEnv = dotenv.parse(fs.readFileSync('.env.staging'))
//   const keys = dotenv.parse(fs.readFileSync('.keys'))
  
//   let env
  
  
//   switch(process.env.NODE_ENV.toLowerCase()) {
//     case 'development':
//       env = {...devEnv}
//       break
//     case 'production':
//       env = {...stagingEnv}
//       break
  
//   }
  
  
//   for(let key in env) {
//     if (_.has(process.env, key) || _.has(commonEnv, key)) {
//       console.warn(`Overriding ${key}: ${process.env[key] || commonEnv[key]} => ${env[key]}`)
//     }
//   }
  
//   for(let key in keys) {
//     if (_.has(process.env, key) || _.has(commonEnv, key)) {
//       console.error(`This variable name is already in use and cannot be used as a secret key. Please rename the key: ${key}`)
//     }
//   }
  
  
//   process.env = processEnv({
//     ...process.env,
//     ...commonEnv,
//     ...env,
//     ...keys
//   })
  
//   function processEnv(env) {
//     const result = {}
  
//     for (key in env) {
//         const value = env[key]
//         if (value.toLowerCase() == 'true') result[key] = true
//         else if (value.toLowerCase() == 'false') result[key] = false
//         else result[key] = value
//     }
  
//     return result
//   }
// }
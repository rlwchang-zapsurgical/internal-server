const aws = require('./awsHelpers')
const models = require('../models')
const mime = require('mime')
const _ = require('lodash')
const response = require('./response')
const path = require('path')

const fileFormat = {
  mime: 'String | list of mime types',
  ext: 'String | list of extensions',
  name: 'name of the files',
}

const optionsFormat = {
  bucket: 'Bucket to upload documents to',
  action: 'Action to take. It should be putObject by default',
  prefix: 'Any prefix to attach to the front of the file name',
  mimes: 'Array of mime types that are accepted', //not fully implementd yet
  exts: 'Array of extensions that are accepted.', //not fully implementd yet
  // These have to do with the DB parts
  Model: '',
  query: '',
  property: '',
  overwrite: '',
  replace: '' 
}





// ====================
// ADD TO DATABASE
// ====================

exports.addUploadToDB = addUploadToDB
async function addUploadToDB(file={}, options={}) {
  return await addUploadsToDB([file], options)
}

exports.addUploadsToDB = addUploadsToDB
async function addUploadsToDB(files=[], options={}) {
  console.log('registering to DB')
  if (!Array.isArray(files)) {files = [files]}
  let {
    Model,
    query,
    property,
    bucket,
    prefix,
    overwrite=true,
    replace=false
  } = options

  checkArgs([Model, query], [bucket, prefix, property])

  if (typeof Model == 'string') {Model = models[_.startCase(Model)]}
  if (typeof query == 'string') {query = {_id: query}}
  const modelInstance = await Model.findOne(query)

  if ((!property || _.isEmpty(property)) && files.length == 1) {
    console.log('just one file and is main')
    let file = files[0]
    if(typeof file == 'string') {file = {name: file}}
    let fileMime = file.mime || mime.getType(file.name)
    let ext = file.ext || mime.getExtension(fileMime)
    file = {
      ...file,
      mime: fileMime,
      ext,
      bucket,
      prefix,
      s3ObjectName: prefix ? `${_.trim(prefix, '/')}/${_.trim(file.name, '/')}` : file.name,
      s3BucketName: bucket,
    }

    modelInstance.file = file
    await modelInstance.save()


    return modelInstance.toObject()

  } else {
    console.log('not the main file')
      if (typeof property == 'string') {property = [property]}
    
      let pointer = modelInstance
      let pointerCopy = modelInstance.toObject()
      let finalProp = property.slice(-1)[0]
    
      for (let i = 0; i < property.length; i++) {
        const prop = property[i]
        
        if(i == property.length - 1) {
          pointer[prop] = pointer[prop] || []
          pointerCopy[prop] = pointerCopy[prop] || []
        } else {
          pointer = pointer[prop]
          pointerCopy = pointerCopy[prop]
        }
      }
    
      let filesFormatted = files.map(file => {
        if(typeof file == 'string') {file = {name: file}}
        let fileMime = file.mime || mime.getType(file.name)
        let ext = file.ext || mime.getExtension(fileMime)
        return {
          ...file,
          mime: fileMime,
          ext,
          bucket,
          prefix,
          s3ObjectName: prefix ? `${_.trim(prefix, '/')}/${_.trim(file.name, '/')}` : file.name,
          s3BucketName: bucket,
        }
      })
    
      filesFormatted = filesFormatted.filter((file) => isValidFileType(file, options))
      // console.log('formatting files', filesFormatted)
      if (replace) {
        pointer[finalProp] = filesFormatted
      } else if (overwrite) {
        pointer[finalProp] = _.unionWith(pointerCopy[finalProp] || [], filesFormatted, _.isEqual)
      } else {
        pointer[finalProp] = pointer[finalProp].concat(filesFormatted)
      }
      // console.log('pointer', pointer)
      await pointer.save()
      return pointer.toObject()
    }
  }


// ====================
// DELETE FROM DATABASE
// ====================


// ====================
// HELPERS
// ====================



// ====================
// Express Methods
// ====================
exports.getUploadLinksRes = getUploadLinksRes
async function getUploadLinksRes(req, res) {
  const {file, files, options} = getParams(req, res)
  let result

  try {
    if (file) {result = await getUploadLink(file, options)}
    else {result = await getUploadLinks(files, options)}
    return response.successResponse(res, result)
  } catch(err) {
    return response.errorResponse(res, err)
  }
}

exports.addUploadsToDBRes = addUploadsToDBRes
async function addUploadsToDBRes(req, res) {
  const {file, files, options} = getParams(req, res)
  let result

  try {
    if (file) {result = await addUploadToDB(file, options)}
    else {result = await addUploadsToDB(files, options)}
    return response.successResponse(res, result)
  } catch(err) {
    return response.errorResponse(res, err)
  }
}

exports.deleteUploadsRes = deleteUploadsRes
async function deleteUploadsRes(req, res) {
  const {file, files, options} = getParams(req, res)
  let result
  console.log('options from delete', options)
  console.log('files', files)
  console.log('file', file)
  try {
    if (file) {result = await deleteUpload(file, options)}
    else {result = await deleteUploads(files, options)}
    console.log('results from delete', result)
    return response.successResponse(res, result)
  } catch(err) {
    return response.errorResponse(res, err)
  }
}

function getParams(req, res) {
  let {id, modelName, property} = req.params
  let {file, files, options={}, additionalParams={}} = req.body

  options.Model = options.Model || modelName
  options.property = options.property || property
  options.query = options.query || {_id: id}

  // console.log('model', options.Model)
  // console.log('files', file || files)
  // console.log('options', options)

  getOptions(options, {...additionalParams, id})

  return {
    file,
    files,
    options
  }
}

function getOptions(options, additionalParams) {
  let {Model, property} = options
  const modelName = Model.name || Model
  const uploadParams = {
    config: {
      bucket: 'zap-surgical-installers',
    },
    masterdisk: {
      bucket: 'zap-surgical-masterdisks',
      prefix: {
        documents: `Documents/${_.startCase(additionalParams.pcType)}-${additionalParams.version}-${additionalParams.id}`,
        default: `${_.startCase(additionalParams.pcType)}-${additionalParams.version}-${additionalParams.id}`
      },
      exts: {
        documents: ['docx', 'doc'],
        default: ['iso']
      }
    }
  }

  if (!property || property == 'file' || property == 'files') {property = 'default'}
  console.log('property', property)
  options.bucket = _.get(uploadParams, [modelName.toLowerCase(), 'bucket'], null)
  options.prefix = _.get(uploadParams, [modelName.toLowerCase(), 'prefix', property], null)
  console.log('.prefix', options.prefix)
  // options.exts = _.get(uploadParams, [modelName.toLowerCase(), 'exts', property], null)
}


const _ = require('lodash')
const axios = require('axios');
const aws = require('./awsHelpers')
const { Software } = require('../models')
const bucket = 'zap-surgical-installers';
const versions = require('./versions')
const document = require('./document')
const response = require('./response')

// const SYSTEM_KEYS = require('../config/keys');
// const SYSTEM_KEYS = undefined;

// exports.getSoftwareParams = (req, res, id, softwareType) => {
//   axios.get(SYSTEM_KEYS.SYSTEM_URI(id))
//     .then(({data}) => res.json(data))
//     .catch(console.log('Failed to retrieve software parameters'))

// }

// exports.getSoftwareData = (req, res, id, softwareType, dataType) => {
//   axios.get(`http://192.168.111.60:8081/zsd/${softwareType}/variables?namespace=${dataType}&json=true`)
//     .then(({data}) => res.json(data))
//     .catch(err => console.log(`Could not get data for ${dataType}: ${err}`))
// }

// {
  //   buildId: String,
  //   buildStart: Date,
  //   buildEnd Date,
  //   buildDuration: Number,
  //   buildNumber: String,
  //   branchName: String,
  //   fullBranchName: String,
  //   commitHash: String,
  //   definition: String,
  //   author: String
  // },

exports.getSoftware = (req, res) => {
  Software.find()
  .then((software) => {
    addDownloadUrl(software)
      .then(processedSoftware => {
        // console.log(processedSoftware)
        res.json({
          status: 'success',
          message: 'Successfully retrieved a list of available software.',
          software: versions.sortByVersion(processedSoftware, asc=false, (item) => item.version)
        })
      })
      .catch(console.log)    
  })
  .catch(error => {
    console.log('error', error)
    res.json({
    status: 'error',
    message: 'Failed to retrieve a list of available Zap software.',
    error
    })
  })
}

exports.showSoftware = (req, res) => {
  Software.findById(req.params.id)
  .populate({path: 'instructions', model: 'Instruction'})
    .then(software => res.json({
      status: 'success',
      message: 'Successfully retrieved a software entry.',
      software
    }))
    .catch(error => res.json({
      status: 'error',
      message: 'Failed to retrieve Zap software.',
      error
    }))
}

exports.addSoftware = (req, res) => {
  Software.create(req.body)
    .then(software => res.json({
      status: 'success',
      message: 'Successfully added a new software to the DB',
      software
    }))
    .catch(error => res.json({
      status: 'error',
      message: 'Failed to add new software to DB.',
      error
    }))
}

exports.editSoftware = (req, res) => {
  Software.findByIdAndUpdate(req.params.id, req.body, {new: true})
  .populate({path: 'instructions', model: 'Instruction'})
  .then(software => res.json({
    status: 'success',
    message: 'You have successfuly edited a software entry from the DB',
    software
  }))
  .catch(error => res.json({
    status: 'error',
    message: 'Failed to edit the corresponding software',
    error
  }))
}

exports.removeSoftware = (req, res) => {
  Software.findByIdAndRemove(req.params.id)
  .then(software => res.json({
    status: 'success',
    message: 'You have successfuly removed a software entry from the DB',
    software
  }))
  .catch(error => res.json({
    status: 'error',
    message: 'Failed to remove the corresponding software',
    error
  }))
}


// ====================
// SOFTWARE INSTALL, PATCHES, ETC
// ====================

async function addDownloadUrl(software=[]) {
  const processedSoftware = []

  for (const softwareItem of software) {
    const processedItem = { ...softwareItem.toObject() }
    const downloadUrl = await aws.getS3ObjectUrl(softwareItem.s3BucketName, softwareItem.s3ObjectName)
    processedItem.downloadUrl = downloadUrl

    for (const doc of processedItem.documents) {
      doc.downloadUrl = await aws.getS3ObjectUrl(doc.bucket, `${doc.prefix}/${doc.name}`)
    }

    processedSoftware.push(processedItem)
  }

  return processedSoftware
}

async function getSoftwareDocumentsUploadLinks(req, res) {
  const {software, files=[], options: additionalOptions={}} = req.body
  if (!additionalOptions.prefix) {console.warn('The prefix naming has not been fixed yet. Please fix before using without the additional options parameter.')}
  // console.log('options', additionalOptions)
  const options = {
    bucket: 'zap-surgical-installers',
    exts: ['doc', 'docx'],
    ...additionalOptions,
    prefix: additionalOptions.prefix ? `SoftwareDocuments/${additionalOptions.prefix}` : `SoftwareDocuments/${software.name}-${software.version}`,
  }
  const result = await document.getDocsUploadLinks(files, options)
  // console.log('result', result)
  response.successResponse(res, result)
}
exports.getSoftwareDocumentsUploadLinks = getSoftwareDocumentsUploadLinks

async function addSoftwareDocuments(req, res) {
  let {software={}, file, files, options: additionalOptions={}} = req.body
  if (file && !files) {files = [file]}
  if (!Array.isArray(files)) { files = [files] }
  const {_id, id} = req.params
  const options = {
    bucket: 'zap-surgical-installers',
    prefix: additionalOptions.prefix ? `SoftwareDocuments/${additionalOptions.prefix}` : `SoftwareDocuments/${software.name}-${software.version}`,
    exts: ['doc', 'docx'],
    Model: 'software',
    query: {
      _id: _id || id
    },
    property: additionalOptions.property || ['documents']
  }

  const result = await document.addDocsToDB(files, options)

  response.successResponse(res, result)
}
exports.addSoftwareDocuments = addSoftwareDocuments